errHandle = file('/home/admin/Logs/LogsKH005Mail.txt',open='w',encoding='UTF-8')
sink(errHandle,type='message',append = T)
sink(errHandle,type='output',append = T)
source('/home/admin/CODE/KH005XDigest/HistoricalAnalysis2G3GKH005.R')
source('/home/admin/CODE/Misc/memManage.R')
source('/home/admin/CODE/MasterMail/timestamp.R')
source('/home/admin/CODE/Send_mail/sendmail.R')
source('/home/admin/CODE/KH005XDigest/aggregateInfo.R')
initDigest = function(df,no)
{
  {
	if(as.numeric(no) == 1)
	{
	  no2 = 'ComAp'
		noac=""
	}
	else if(as.numeric(no)==2)
	{
	  no2 = 'Load'
		noac=""
	}
	else if(as.numeric(no)==3)
	{
		no2 = "Solar"
		noac=""
	}
	}
	#ratspec = round(as.numeric(df[,2])/2624,2)
  body = "\n\n_________________________________________\n\n"
  body = paste(body,as.character(df[,1]),no2)
  body = paste(body,"\n\n_________________________________________\n\n")
  body = paste(body,"Eac",noac,"-1 [kWh]: ",as.character(df[,2]),sep="")
  
	{
	if(as.numeric(no)==2 || as.numeric(no)==3)
	{
		body = paste(body,"\n\nEac",noac,"-2 [kWh]: ",as.character(df[,3]),sep="")
		{
		if(as.numeric(no)==3)
		{
			body = paste(body,"\n\nYield-1 [kWh/kWp]:",as.character(df[,6]))
			body = paste(body,"\n\nYield-2 [kWh/kWp]:",as.character(df[,13]))
			body = paste(body,"\n\nTotal daily irradiation [kWh/m2]:",df[,10])
			body = paste(body,"\n\nPR [%]:",as.character(df[,11]))
			body = paste(body,"\n\nTotal daily irradiation (pyranometer -- KH-714S) [kWh/m2]:",df[,14])
			body = paste(body,"\n\nPR pyranometer [%]:",as.character(df[,15]))
      body = paste(body,"\n\nGrid Availability [%]:",as.character(df[,17]))	
      body = paste(body,"\n\nPlant Availability [%]:",as.character(df[,18]))
			if(as.numeric(df[,11]) > 80)
			{
				body = paste(body," -- WARNING SYSTEM COULD BE RUNNING AT FULL THROTTLE")
			}
			body = paste(body,"\n\nMean Tmod [C]:",as.character(df[,12]))
		}
		else
		{
			body = paste(body,"\n\nArtificial load [kWh]:",(as.numeric(df[,3])+TOTSOLAR))
			body = paste(body,"\n\nPercentage loads powered by solar [%]:",as.character(df[,10]))
		}
		}
  	body = paste(body,"\n\nRatio [%]:",as.character(df[,7]))
		acpts = round(as.numeric(df[,4]) * 14.4)
  	body = paste(body,"\n\nPoints recorded: ",acpts," (",as.character(df[,4]),"%)",sep="")
		body = paste(body,"\n\nDowntime (%):",as.character(df[,5]))
  	body = paste(body,"\n\nLast recorded timestamp:",as.character(df[,8]))
  	body = paste(body,"\n\nLast recorded energy meter reading [kWh]:",as.character(df[,9]))
	}
	else if(as.numeric(no)==1)
	{
		#body = paste(body,"\n\nYield [kWh/kWp]:",as.character(df[,5]))
		acpts = round(as.numeric(df[,3])*14.4)
		body = paste(body,"\n\nArtificial load [kWh]:",(as.numeric(df[,2])+TOTSOLAR))
		body = paste(body,"\n\nPercentage loads powered by solar [%]:",as.character(df[,6]))
  	body = paste(body,"\n\nPoints recorded: ",acpts," (",as.character(df[,3]),"%)",sep="")
		body = paste(body,"\n\nDowntime (%):",as.character(df[,4]))
		#body = paste(body,"\n\nDowntime (%):",as.character(df[,4]))
	}
	}
  return(body)
}

printtsfaults = function(TS,num,body)
{
  no = num
  {
	if(as.numeric(no) == 1)
	{
	  no2 = 'ComAp'
		noac=""
	}
	else if(as.numeric(no)==2)
	{
	  no2 = 'Loads'
		noac=""
	}
	else if(as.numeric(no)==3)
	{
		no2 = "Solar"
		noac=""
	}
	}
	num = no2
	if(length(TS) > 1)
	{
		if(as.numeric(no)==3 || as.numeric(no)==4)
		{
		body = paste(body,"\n\n_________________________________________\n\n")
		body = paste(body,paste("Timestamps for",num,"where Pac < 1 between 8am -5pm\n"))
		for(lm in  1 : length(TS))
		{
			body = paste(body,TS[lm],sep="\n")
		}
		}
	}
	return(body)
}

sendMail = function(df1,df2,df3,pth1,pth2,pth3,pth5)
{
  filetosendpath = c(pth1,pth2,pth3,pth5)
	filenams = c()
  for(l in 1 : length(filetosendpath))
  {
    temp = unlist(strsplit(filetosendpath[l],"/"))
    filenams[l] = temp[length(temp)]
		if(l == 1)
			currday = temp[length(temp)]
  }
	print('Filenames Processed')
	body=""
	body = paste(body,"Site Name: Sheico Phnom Penh",sep="")
	body = paste(body,"\n\nLocation: Phnom Penh, Cambodia")
	body = paste(body,"\n\nO&M Code: KH-005")
	body = paste(body,"\n\nSystem Size: 858 kWp")
	body = paste(body,"\n\nNumber of Energy Meters: 1")
	body = paste(body,"\n\nModule Brand / Model / Nos: Trina Solar / PD14-325Wp / 2640")
	body = paste(body,"\n\nInverter Brand / Model / Nos: SMA / STP60-10 / 12")
	body = paste(body,"\n\nSite COD:",DOB)
	body = paste(body,"\n\nSystem age [days]:",as.character((as.numeric(DAYSALIVE))))
	body = paste(body,"\n\nSystem age [years]:",as.character(round((as.numeric(DAYSALIVE))/365,2)))
  
	body = paste(body,initDigest(df3,3))  #its correct, dont change
	body = paste(body,initDigest(df2,2))  #its correct, dont change
	body = paste(body,initDigest(df1,1),sep="\n")  #its correct, dont change
  body = printtsfaults(TIMESTAMPSALARM,1,body)
  body = printtsfaults(TIMESTAMPSALARM2,2,body)
  body = printtsfaults(TIMESTAMPSALARM3,3,body)
	print('2G data processed')
	body = paste(body,"\n\n_________________________________________\n\n")
  body = paste(body,"Station Data")
  body = paste(body,"\n\n_________________________________________\n\n")
  body = paste(body,"Station DOB:",as.character(DOB))
  body = paste(body,"\n\n# Days alive:",as.character(DAYSALIVE))
  yrsalive = format(round((DAYSALIVE/365),2),nsmall=2)
  body = paste(body,"\n\n# Years alive:",yrsalive)
	print('3G data processed')
	body = gsub("\n ","\n",body)
	graph_command1=paste("Rscript /home/admin/CODE/EmailGraphs/PR_Graph_Azure.R","KH-005",64.2,0.008,"2017-03-14",substr(currday,14,23),sep=" ")
  	system(graph_command1)
  	graph_path1=paste('/home/admin/Graphs/Graph_Output/KH-005/[KH-005] Graph ',substr(currday,14,23),' - PR Evolution.pdf',sep="")
  	graph_extract_path1=paste('/home/admin/Graphs/Graph_Extract/KH-005/[KH-005] Graph ',substr(currday,14,23),' - PR Evolution.txt',sep="")
  	filetosendpath=c(graph_path1,graph_extract_path1,filetosendpath)
	while(1)
	{
  mailSuccess = try(send.mail(from = sender,
            to = recipients,
            subject = paste("Station [KH-005X] Digest",substr(currday,14,23)),
            body = body,
            smtp = list(host.name = "smtp.office365.com", port = 587, user.name = uname, passwd = pwd, tls = TRUE),
            authenticate = TRUE,
            send = TRUE,
            attach.files = filetosendpath,
            debug = F),silent=T)
	if(class(mailSuccess)=='try-error')
	{
		Sys.sleep(180)
		next
	}
	break
	}
	recordTimeMaster("KH-005X","Mail",substr(currday,14,23))
}

sender = c('operations@cleantechsolar.com')
uname = 'shravan.karthik@cleantechsolar.com'
recipients = getRecipients("KH-005X","m")

pwd = 'CTS&*(789'
todisp = 1
while(1)
{
	recipients = getRecipients("KH-005X","m")
  recordTimeMaster("KH-005X","Bot")
	sendmail = 0
  prevx = x
  prevy = y
  prevt = t
  years = dir(path)
  for(x in prevx : length(years))
  {
    pathyear = paste(path,years[x],sep="/")
    writepath2Gyr = paste(writepath2G,years[x],sep="/")
    writepath3Gyr = paste(writepath3G,years[x],sep="/")
    checkdir(writepath2Gyr)
    checkdir(writepath3Gyr)
    months = dir(pathyear)
    startmnth = 1
    if(prevx == x)
    {
      startmnth = prevy
    }
    for(y in startmnth : length(months))
    {
      pathmonths = paste(pathyear,months[y],sep="/")
      writepath2Gmon = paste(writepath2Gyr,months[y],sep="/")
      writepath3Gfinal = paste(writepath3Gyr,"/[KH-005X] ",months[y],".txt",sep="")
      checkdir(writepath2Gmon)
      stations = dir(pathmonths)
			#if(length(stations) > 2)
			#stations = c(stations[2],stations[3],stations[1])
      pathdays = paste(pathmonths,stations[1],sep="/")
      pathdays2 = paste(pathmonths,stations[2],sep="/")
      pathdays3 = paste(pathmonths,stations[3],sep="/")
      writepath2Gdays = paste(writepath2Gmon,stations[1],sep="/")
      writepath2Gdays2 = paste(writepath2Gmon,stations[2],sep="/")
      writepath2Gdays3 = paste(writepath2Gmon,stations[3],sep="/")
      checkdir(writepath2Gdays)
      checkdir(writepath2Gdays2)
      checkdir(writepath2Gdays3)
      days = dir(pathdays)
      days2 = dir(pathdays2)
      days3 = dir(pathdays3)
      startdays = 1
      if(y == prevy)
      {
        startdays = prevt
      }
      if(startdays == 1)
      {
        temp = unlist(strsplit(days[1]," "))
				print(substr(temp[2],1,10))
        temp = as.Date(substr(temp[2],1,10),"%Y-%m-%d")
      }
      for(t in startdays : length(days))
      {
         condn1  = (t != length(days))
         condn2 = ((t == length(days)) && ((y != length(months)) || (x != length(years))))
         if(!(condn1 || condn2))
         {
				 	 if(todisp)
					 {
					 	print('No new files')
						todisp = 0
					 }
         }
				 if(grepl("Copy",c(days[t],days2[t],days3[t])))
				 {
				 	daycop = c(days[t],days2[t],days3[t])
					daycop = daycop[grepl("Copy",daycop)]
					for(inner in 1 : length(daycop))
					{
				 	print('Copy file found so removing it')
					command = paste('rm "',pathdays,'/',daycop[t],'"',sep="")
					print(command)
					system(command)
					print('Command complete.. sleeping')
					}
				 	Sys.sleep(3600)
					next
				 }

				 if(is.na(days[t]) && is.na(days2[t]) && is.na(days3[t]))
				 {
					 	print('Files all NA restarting after hour')
           Sys.sleep(3600)
           next
				 }
				 todisp = 1
         sendmail = 1
				 if(!(condn1 || condn2))
				 {
				 	sendmail = 0
				 }
				 else
				 {
         	DAYSALIVE = DAYSALIVE + 1
				 }
				 print(paste('Processing',days[t]))
				 print(paste('Processing',days2[t]))
				 print(paste('Processing',days3[t]))
         writepath2Gfinal = paste(writepath2Gdays,"/",days[t],sep="")
         writepath2Gfinal2 = paste(writepath2Gdays2,"/",days2[t],sep="")
         writepath2Gfinal3 = paste(writepath2Gdays3,"/",days3[t],sep="")
         readpath = paste(pathdays,days[t],sep="/")
         readpath2 = paste(pathdays2,days2[t],sep="/")
         readpath3 = paste(pathdays3,days3[t],sep="/")
				 TOTSOLAR <<- 0
				 METERCALLED <<- 3 # its correct dont change
         df3 = secondGenData(readpath3,writepath2Gfinal3)
				 METERCALLED <<- 1  #its correct dont change
         df1 = secondGenData(readpath,writepath2Gfinal)
				 METERCALLED <<- 2 # its correct dont change
         df2 = secondGenData(readpath2,writepath2Gfinal2)
				thirdGenData(writepath2Gfinal,writepath2Gfinal2,writepath2Gfinal3,writepath3Gfinal)
  if(sendmail ==0)
  {
	  Sys.sleep(3600)
    next
  }
	print('Sending Mail')
  sendMail(df1,df2,df3,writepath2Gfinal,writepath2Gfinal2,
	writepath2Gfinal3,writepath3Gfinal)
	print('Mail Sent')
      }
    }
  }
	gc()
}
sink()
