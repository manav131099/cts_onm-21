rm(list = ls())

errHandle = file('/home/admin/Logs/LogsKH001.txt',open='w',encoding='UTF-8')
sink(errHandle,type='message',append = T)
sink(errHandle,type='output',append = T)

source('/home/admin/CODE/KH001Digest/HistoricalAnalysis2G3GKH010.R')
source('/home/admin/CODE/Send_mail/sendmail.R')
require('mailR')
source('/home/admin/CODE/MasterMail/timestamp.R')
source('/home/admin/CODE/KH001Digest/aggregateInfo.R')
print('History done')
InstCap = .63
timetomins = function(x)
{
  hr = unlist(strsplit(x,":"))
  min = as.numeric(hr[2])
  hr = as.numeric(hr[1])
  return((hr * 60 + min + 1))
}
rf = function(x)
{
  return(format(round(x,2),nsmall=2))
}
rf1 = function(x)
{
  return(format(round(x,1),nsmall=1))
}
rf3 = function(x)
{
  return(format(round(x,3),nsmall=3))
}
checkdir = function(x)
{
  if(!file.exists(x))
  {
    dir.create(x)
  }
}
initDigest = function(df,extra)
{
	rat = round(as.numeric(df[,8])/(InstCap * as.numeric(extra[3])),1)
	rat2 = round((abs(as.numeric(extra[2]) - as.numeric(extra[4])))/(InstCap * as.numeric(extra[3])),1)
	rat3 =  round((abs(as.numeric(extra[2]) - as.numeric(extra[4])))/(InstCap*100),2)
	body=""
	body = paste(body,"\n\n_________________________________________\n")
  body = paste(body,as.character(df[,1]))
  body = paste(body,"\n_________________________________________\n\n")
  body = paste(body,"Pts recorded: ",round(as.numeric(df[,2])*14.4,0)," (",as.character(df[,2]),"%)",sep="")
	body = paste(body,"\n\nDowntime [%]:",as.character(df[,3]))
	body = paste(body,"\n\nDaily Irradiation Silicon Sensor [kWh/m2]: ",format(round(as.numeric(extra[3]),2),nsmall=2))
	body = paste(body,"\n\nDaily specific yield [kWh/kWp]: ",rat3)
  body = paste(body,"\n\nEac",1," load consumed [kWh]: ",as.character(df[,4]),sep="")
  body = paste(body,"\n\nEac",1," load production [kWh]: ",as.character(df[,5]),sep="")
  body = paste(body,"\n\nEnergy delivered [kWh]: ",as.character(df[,6]),sep="")
  body = paste(body,"\n\nEnergy received [kWh]: ",as.character(df[,7]),sep="")
	body = paste(body,"\n\nEac2 system generated power [kWh]: ",as.character(df[,8]),sep="")
	body = paste(body,"\n\nPercentage solar power pumped to grid [%]: ",as.character(df[,9]),sep="")
	body = paste(body,"\n\nSolar power consumed by load [kWh]: ",as.character(df[,10]),sep="")
	body = paste(body,"\n\nTotal power consumed by loads [kWh]: ",as.character(df[,12]),sep="")
	body = paste(body,"\n\nPerfomance Ratio Eac sum Method (%): ",rat)
	body = paste(body,"\n\nPerfomance Ratio Difference Method (%): ",rat2)
	body = paste(body,"\n\nLast recorded timestamp solar meter:",extra[1])
  body = paste(body,"\n\nLast recorded reading solar meter [kWh]:",extra[2])
	body = paste(body,"\n\nLast recorded timestamp export meter:",extra[1])
  body = paste(body,"\n\nLast recorded reading export meter [kWh]:",extra[5])
  return(body)
}
printtsfaults = function(TS,body)
{
	if(length(TS) > 1)
	{
		body = paste(body,"\n_________________________________________\n")
		body = paste(body,paste("\nTimestamps","where Eac2 < 5 or is NA between 9am - 5pm\n"))
		for(lm in  1 : length(TS))
		{
			body = paste(body,TS[lm],sep="\n")
		}
		body = paste(body,"\n_________________________________________\n")
	}
	return(body)
}
sendMail = function(filetosendpath,extra)
{
  body = NULL
  filenams = c()
  for(l in 1 : length(filetosendpath))
  {
    temp = unlist(strsplit(filetosendpath[l],"/"))
    filenams[l] = temp[length(temp)]
		if(l == 1)
			currday = temp[length(temp)]
  }
	df1 = read.table(filetosendpath[1],header = T,sep="\t")
	print('Filenames Processed')
  body = ""
	body = paste(body,"Site Name: PPSEZ - System 1",sep="")
	body = paste(body,"\n\nLocation: Phnom Penh, Cambodia")
	body = paste(body,"\n\nO&M Code: KH-001")
	body = paste(body,"\n\nSystem Size: 63 kWp")
	body = paste(body,"\n\nNumber of Energy Meters: 2")
	body = paste(body,"\n\nModule Brand / Model / Nos: Canadian Solar / CS6X-315P / 200")
	body = paste(body,"\n\nInverter Brand / Model / Nos: SMA / STP 25000TL / 2")
	body = paste(body,"\n\nSite COD:",as.character(DOB))
	body = paste(body,"\n\nSystem age [days]:",as.character((as.numeric(DAYSALIVE))))
	body = paste(body,"\n\nSystem age [years]:",as.character(round((as.numeric(DAYSALIVE))/365,2)))
	body = paste(body,initDigest(df1,extra[1:5]))
  body = printtsfaults(TIMESTAMPSALARM,body)
	if(length(filetosendpath) > 1)
	{
		df1 = read.table(filetosendpath[2],header = T,sep="\t")
		body = paste(body,initDigest(df1,extra[6:10]))
	}
  body = paste(body,"\n\n_________________________________________\n\n")
  body = paste(body,"Station History")
  body = paste(body,"\n\n_________________________________________\n\n")
  body = paste(body," Station DOB:",as.character(DOB))
  body = paste(body,"\n # Days alive:",as.character(DAYSALIVE))
  yrsalive = format(round((DAYSALIVE/365),2),nsmall=2)
  body = paste(body,"\n # Years alive:",yrsalive)
  body = paste(body,"\n # Eac2 total production this month [kWh]:",MONTHEACPROD)
  body = paste(body,"\n\n # Power supplied to grid this month [kWh]:",MONTHEACGRIDIN)
  body = paste(body,"\n\n # Solar power consumed by loads this month [kWh]:",MONTHEACLOADCONS)
  body = paste(body,"\n\n # Total power consumed by loads this month [kWh]:",MONTHLOADTOTS)
  proje1 = format(round((MONTHEACPROD * DAYSTHISMONTHAC / DAYSTHISMONTH),1),nsmall=1)
  proje2 = format(round((MONTHEACGRIDIN * DAYSTHISMONTHAC / DAYSTHISMONTH),1),nsmall=1)
  proje3 = format(round((MONTHEACLOADCONS * DAYSTHISMONTHAC / DAYSTHISMONTH),1),nsmall=1)
  proje4 = format(round((MONTHLOADTOTS * DAYSTHISMONTHAC / DAYSTHISMONTH),1),nsmall=1)

  body = paste(body,"\n\n # Eac2 projected total production for the month [kWh]:",proje1)
  body = paste(body,"\n\n # Projected power supplied to grid for the month [kWh]:",proje2)
  body = paste(body,"\n\n # Projected solar power consumption by the loads for the month [kWh]:",proje3)
  body = paste(body,"\n\n # Projected power consumption by the loads for the month [kWh]:",proje4)
  body = paste(body,"\n\n # Eac2 total production last 30 days [kWh]:",sum(LAST30DAYSEACPROD))
  body = paste(body,"\n\n # Power supplied to grid last 30 days [kWh]:",sum(LAST30DAYSEACGRIDIN))
  body = paste(body,"\n\n # Solar power consumed by loads last 30 days [kWh]:",sum(LAST30DAYSEACLOADCONS))
  body = paste(body,"\n\n # Total power consumed by loads last 30 days [kWh]:",sum(LAST30DAYSLOADTOTS))
  body = paste(body,"\n\n # Eac2 mean production last 30 days [kWh]:",round(mean(LAST30DAYSEACPROD),1))
  body = paste(body,"\n\n # Mean power supplied to grid last 30 days [kWh]:",round(mean(LAST30DAYSEACGRIDIN),1))
  body = paste(body,"\n\n # Mean Solar power consumed by loads last 30 days [kWh]:",round(mean(LAST30DAYSEACLOADCONS),1))
  body = paste(body,"\n\n # Mean power consumed by loads last 30 days [kWh]:",round(mean(LAST30DAYSLOADTOTS),1))

  Sys.sleep(20)
	body = gsub("\n ","\n",body)
  
  graph_command = paste("Rscript /home/admin/CODE/EmailGraphs/PR_Graph_Azure.R", "KH-001S", 76.2, 0.008, "2016-06-05", substr(currday,11,20), sep=" ")
  system(graph_command, wait=TRUE)	
  graph_path = paste("/home/admin/Graphs/Graph_Output/KH-001/[KH-001] Graph ", substr(currday,11,20), " - PR Evolution.pdf",sep="")
  graph_extract_path = paste("/home/admin/Graphs/Graph_Extract/KH-001/[KH-001] Graph ", substr(currday,11,20), " - PR Evolution.txt",sep="")
  filetosendpath = c(graph_path, graph_extract_path, filetosendpath) 
 
	send.mail(from = sender,
            to = recipients,
            subject = paste("Station [KH-001S] Digest",substr(currday,11,20)),
            body = body,
            smtp = list(host.name = "smtp.office365.com", port = 587, user.name = uname, passwd = pwd, tls = TRUE),
            authenticate = TRUE,
            send = TRUE,
            attach.files = filetosendpath,
            debug = F)
recordTimeMaster("KH-001S","Mail",substr(currday,11,20))
}
sender = c('operations@cleantechsolar.com')
uname = 'shravan.karthik@cleantechsolar.com'

recipients = getRecipients("KH-001S","m")
#recipients = c('shravan1994@gmail.com')

pwd = 'CTS&*(789'
todisp = 1
path = "/home/admin/Dropbox/Cleantechsolar/1min/[714]"
pathwrite = "/home/admin/Dropbox/Second Gen/[KH-001S]"
pathwrite3G = pathwrite
checkdir(pathwrite)
date = format(Sys.time(),tz = "Singapore")
years = dir(path)
prevpathyear = paste(path,years[length(years)],sep="/")
months = dir(prevpathyear)
prevpathmonth = paste(prevpathyear,months[length(months)],sep="/")
currday = prevday = dir(prevpathmonth)[length(dir(prevpathmonth))]
prevwriteyears = paste(pathwrite,years[length(years)],sep="/")
prevwritemonths = paste(prevwriteyears,months[length(months)],sep="/")
prevwriteyears3G = paste(pathwrite3G,years[length(years)],sep="/")
filenam = paste(substr(as.character(years[x]),3,4),substr(as.character(months[y]),6,7),sep="")
prevwritemonths3G = paste(prevwritemonths,"/[KH-001S] ",filenam,".txt",sep="")

stnnickName = "714"
stnnickName2 = "KH-001S"
lastdatemail = lastMailDate(paste('/home/admin/Start/MasterMail/',stnnickName2,'_Mail.txt',sep=""))
if(!is.null(lastdatemail))
{
	yr = substr(lastdatemail,1,4)
	monyr = substr(lastdatemail,1,7)
	prevpathyear = paste(path,yr,sep="/")
	prevpathmonth = paste(prevpathyear,monyr,sep="/")
	currday = prevday = paste("[",stnnickName,"] ",lastdatemail,".txt",sep="")
	prevwriteyears = paste(pathwrite,yr,sep="/")
	prevwritemonths = paste(prevwriteyears,monyr,sep="/")
	prevwriteyears3G = paste(pathwrite3G,yr,sep="/")
	filenam = paste(substr(as.character(years[x]),3,4),substr(as.character(months[y]),6,7),sep="")
	prevwritemonths3G = paste(prevwritemonths,"/[",stnnickName2,"] ",filenam,".txt",sep="")
}

check = 0
pwd = 'CTS&*(789'

wt =1
while(1)
{
recipients = getRecipients("KH-001S","m")
recordTimeMaster("KH-001S","Bot")
years = dir(path)
writeyears = paste(pathwrite,years[length(years)],sep="/")
writeyears3G = paste(pathwrite3G,years[length(years)],sep="/")
checkdir(writeyears)
checkdir(writeyears3G)
pathyear = paste(path,years[length(years)],sep="/")
months = dir(pathyear)
writemonths = paste(writeyears,months[length(months)],sep="/")
checkdir(writemonths)
pathmonth = paste(pathyear,months[length(months)],sep="/")
filenam = paste(substr(as.character(years[length(years)]),3,4),substr(as.character(months[length(months)]),6,7),sep="")
writemonths3G = paste(writemonths,"/[KH-001S] ",filenam,".txt",sep="")
tm = timetomins(substr(format(Sys.time(),tz = "Singapore"),12,16))
currday = dir(pathmonth)[length(dir(pathmonth))]
print(paste('Currday is',currday))
print(paste('Prevday is',prevday))

if(length(currday) < 1)
{
print('Empty.. sleeping for an hour')
Sys.sleep(3600)
next
}

if(prevday == currday)
{
   if(wt == 1)
{
 print("")
 print("")
 print("__________________________________________________________")
 print(substr(currday,7,16))
 print("__________________________________________________________")
 print("")
 print("")
 print('Waiting')
wt = 0
}
{
  if(tm > 1319 || tm < 30)
  {
      Sys.sleep(120)
  }        
  else
  {
    Sys.sleep(3600)
  }
}
next
}
print('New Data found.. sleeping to ensure sync complete')
Sys.sleep(20)
print('Sleep Done')
if(prevpathmonth != pathmonth)
{
  MONTHEACPROD = 0
  MONTHEACGRIDIN = 0
  MONTHEACLOADCONS = 0
  MONTHLOADTOTS = 0
  DAYSTHISMONTH = 0
  strng = unlist(strsplit(currday," "))
	strng = as.Date(substr(strng[2],1,10),"%Y-%m-%d")
	DAYSTHISMONTHAC = monthDays(strng)
}
lastdata = read.table(paste(pathmonth,currday,sep="/"),header = T,sep="\t")
print(colnames(lastdata))
dat = as.numeric(lastdata[complete.cases(lastdata[,3]),3])
colno = NA
if(length(dat) > 1){
colno = round(sum(dat)/60000,2)}
print(colno)
lastdata = c(as.character(lastdata[nrow(lastdata),1]),as.character(lastdata[nrow(lastdata),86]),as.character(colno),as.character(lastdata[1,86]),as.character(lastdata[nrow(lastdata),40]))
currdayw = gsub("714","KH-001S",currday)

datawrite = secondGenData(paste(pathmonth,currday,sep="/"),paste(writemonths,currdayw,sep="/"))
datasum = thirdGenData(paste(writemonths,currdayw,sep="/"),writemonths3G)
MONTHEACPROD = MONTHEACPROD + datasum[1]
MONTHEACGRIDIN = MONTHEACGRIDIN + datasum[2]
MONTHEACLOADCONS = MONTHEACLOADCONS + datasum[3]
MONTHLOADTOTS = MONTHLOADTOTS + datasum[4]
DAYSTHISMONTH = DAYSTHISMONTH + 1 
GLOBEACPROD = GLOBEACPROD + datasum[1]
GLOBEACGRIDIN = GLOBEACGRIDIN + datasum[2]
GLOBEACLOADCONS = GLOBEACLOADCONS + datasum[3]
GLOBLOADTOTS = GLOBLOADTOTS+datasum[4]

DAYSALIVE = DAYSALIVE + 1
LAST30DAYSEACPROD[[idxvec]] = datasum[1]
LAST30DAYSEACGRIDIN[[idxvec]] = datasum[2]
LAST30DAYSEACLOADCONS[[idxvec]] = datasum[3]
LAST30DAYSLOADTOTS[[idxvec]] = datasum[4]


print('Digest Written');
filetosendpath = c(paste(writemonths,currdayw,sep="/"))
filename = c(currdayw)
filedesc = c("Daily Digest")
prevdayw = gsub("714","KH-001S",prevday)
dataprev = read.table(paste(prevwritemonths,prevdayw,sep="/"),header = T,sep = "\t")
dataprev2 = read.table(paste(prevpathmonth,prevday,sep="/"),header = T,sep = "\t")
colno2 = round(sum(dataprev2[complete.cases(dataprev2[,3]),3])/60000,2)
datawrite = secondGenData(paste(prevpathmonth,prevday,sep="/"),paste(prevwritemonths,prevdayw,sep="/"))
print('Summary read')
if(as.numeric(datawrite[,2]) != as.numeric(dataprev[,2]))
{
print('Difference in old file noted')
tots = thirdGenData(paste(prevwritemonths,prevdayw,sep="/"),prevwritemonths3G)
previdx = (idxvec - 1) %% 31
lastdata[6] = as.character(dataprev2[nrow(dataprev2),1])
lastdata[7] = as.character(dataprev2[nrow(dataprev2),86])
lastdata[8] = colno2
lastdata[9] = as.character(dataprev2[1,86])
lastdata[9] = as.character(dataprev2[1,40])

if(previdx == 0) {previdx = 1}

if(prevpathmonth == pathmonth)
{
	MONTHEACPROD = MONTHEACPROD + tots[1] - as.numeric(dataprev[1,8])
	MONTHEACGRIDIN = MONTHEACGRIDIN + tots[2] - as.numeric(dataprev[1,7])
	MONTHEACLOADCONS = MONTHEACLOADCONS + tots[3] - as.numeric(dataprev[1,10])
	MONTHLOADTOTS = MONTHLOADTOTS + tots[4] - as.numeric(dataprev[1,12])
}
	GLOBEACPROD = GLOBEACPROD + tots[1] - as.numeric(dataprev[1,8])
	GLOBEACGRIDIN = GLOBEACGRIDIN + tots[2] - as.numeric(dataprev[1,7])
	GLOBEACLOADCONS = GLOBEACLOADCONS + tots[3] - as.numeric(dataprev[1,10])
	GLOBLOADTOTS = GLOBLOADTOTS + tots[4] - as.numeric(dataprev[1,12])
	
	LAST30DAYSEACPROD[[previdx]] = tots[1]
	LAST30DAYSEACGRIDIN[[previdx]] = tots[2]
	LAST30DAYSEACLOADCONS[[previdx]] = tots[3]
	LAST30DAYSLOADTOTS[[previdx]] = tots[4]
  filetosendpath[2] = paste(prevwritemonths,prevdayw,sep="/")
  filename[2] = prevdayw
  filedesc[2] = c("Updated Digest")
print('Updated old files.. both digest and summary file')
}
idxvec = (idxvec + 1 ) %% 31
if(idxvec == 0) {idxvec = 1}

sendMail(filetosendpath,lastdata)
print('Mail Sent');
prevday = currday
prevpathyear = pathyear
prevpathmonth = pathmonth
prevwriteyears = writeyears
prevwritemonths = writemonths
prevwriteyears3G = writeyears3G
prevwritemonths3G = writemonths3G
wt = 1
gc()
}
sink()
