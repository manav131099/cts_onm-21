rm(list=ls())
require('compiler')
enableJIT(3)
errHandle = file('/home/admin/Logs/LogsIN022Mail.txt',open='w',encoding='UTF-8')
sink(errHandle,type='message',append = T)
sink(errHandle,type='output',append = T)

source('/home/admin/CODE/IN022Digest/summaryFunctions.R')
source('/home/admin/CODE/Send_mail/sendmail.R')
source('/home/admin/CODE/common/math.R')
RESETHISTORICAL=0
daysAlive = 0
reorderStnPaths = c(30,17,18,19,20,1,9,10,11,12,13,14,15,16,2,3,4,5,6,7,8,21,22,
23,24,25,26,27,28,29)

#source('/home/admin/CODE/IN022Digest/Historical.R')
source('/home/admin/CODE/MasterMail/timestamp.R')
require('mailR')
source('/home/admin/CODE/IN022Digest/aggregateInfo.R')

METERNICKNAMES = c("WMS","OG-1","OG-2","OG-3","OG-4","INV-1","INV-2","INV-3",
"INV-4","INV-5","INV-6","INV-7","INV-8","INV-9","INV-10","INV-11","INV-12",
"INV-13","INV-14","INV-15","INV-16","PH-2 INV-1","PH-2 INV-2","PH-2 INV-3",
"PH-2 INV-4","PH-2 INV-5","PH-2 INV-6","PH-2 INV-7","PH-2 INV-8","PH-2 INV-9")

METERACNAMES = c("WMS","OG1","OG2","OG3","OG4","Inverter_1","Inverter_2",
"Inverter_3","Inverter_4","Inverter_5","Inverter_6","Inverter_7","Inverter_8",
"Inverter_9","Inverter_10","Inverter_11","Inverter_12","Inverter_13",
"Inverter_14","Inverter_15","Inverter_16","PH2_Inverter-1","PH2_Inverter-2",
"PH2_Inverter-3","PH2_Inverter-4","PH2_Inverter-5","PH2_Inverter-6",
"PH2_Inverter-7","PH2_Inverter-8","PH2_Inverter-9")

checkdir = function(x)
{
	if(!file.exists(x))
	{
		dir.create(x)
	}
}

DOB = "01-07-2017"
sender = c('operations@cleantechsolar.com')
uname = 'shravan.karthik@cleantechsolar.com'
recipients = getRecipients("IN-022C","m")
pwd = 'CTS&*(789'
referenceDays = NA
extractDaysOnly = function(days)
{
	seq = seq(from=1,to=length(days)*6,by=6)
	days = unlist(strsplit(days,"-"))
	days = paste(days[seq+3],days[seq+4],days[seq+5],sep="-")
	return(days)
}
analyseDays = function(days,ref)
{
	if(length(days) == length(ref))
		return(days)
	daysret = unlist(rep(NA,length(ref)))
	if(length(days) == 0)
		return(daysret)
	days2 = extractDaysOnly(days)
	idxmtch = match(days2,ref)
	idxmtch2 = idxmtch[complete.cases(idxmtch)]
	if(length(idxmtch) != length(idxmtch2))
	{
		print(".................Missmatch..................")
		print(days2)
		print("#########")
		print(ref)
		print("............................................")
		return(days)
	}
	daysret[idxmtch] = as.character(days)
	if(!(is.na(daysret[length(daysret)])))
		return(daysret)
	else
		return(days)
}
performBackWalkCheck = function(day)
{
	path = "/home/admin/Dropbox/FlexiMC_Data/Gen1_Data/[IN-022C]"
	retval = 0
	yr = substr(day,1,4)
	yrmon = substr(day,1,7)
	pathprobe = paste(path,yr,yrmon,sep="/")
	stns = dir(pathprobe)
	print(stns)
	idxday = c()
	for(t in 1 : length(stns))
	{
		pathdays = paste(pathprobe,stns[t],sep="/")
		print(pathdays)
		days = dir(pathdays)
		dayMtch = days[grepl(day,days)]
		if(length(dayMtch))
		{
			idxday[t] = match(dayMtch,days)
		}
		print(paste("Match for",day,"is",idxday[t]))
	}
	idxmtch = match(NA,idxday[t])
	if(length(unique(idxday))==1 || length(idxmtch) < 5)
	{
		print('All days present ')
		retval = 1
	}
	return(retval)
}
sendMail= function(pathall)
{
  filenams = c()
  body = ""
	body = paste(body,"Site Name: Bajaj Auto, Chakan",sep="")
	body = paste(body,"\n\nLocation: Pune, India")
	body = paste(body,"\n\nO&M Code: IN-022")
	body = paste(body,"\n\nSystem Size:",sum(INSTCAPM))
	body = paste(body,"\n\nNumber of Energy Meters: 4")
	body = paste(body,"\n\nModule Brand / Model / Nos: JA Solar / 320Wp / 6000")
	body = paste(body,"\n\nInverter Brand / Model / Nos: SMA / STP60 / 25")
	body = paste(body,"\n\nSite COD: 2017-03-15")
	body = paste(body,"\n\nSystem age [days]:",as.character((as.numeric(daysAlive)+125)))
	body = paste(body,"\n\nSystem age [years]:",as.character(round((as.numeric(daysAlive)+125)/365,2)))
	bodyac = body
	body = ""
	TOTALGENCALC = 0
	MYLD = c()
	
	acname = METERNICKNAMES
	noInverters = NOINVS
	InvReadings = unlist(rep(NA,noInverters))
	InvAvail = unlist(rep(NA,noInverters))
	globMtYld = c()
	globMtVal = c()
	idxglobMtYld = 1
	GTI=NA
	for(t in 1 : length(pathall))
  {
	type = 0
	meteridx = t
	if(t <= (1+NOMETERS))
		type = 1
	if( t > (1 + NOMETERS))
	{
		splitpath = unlist(strsplit(pathall[t],"Inverter_"))
		offset = 0
		if(grepl("Ph2_",pathall[t]))
			offset = 16
		if(length(splitpath)>1)
		{
			meteridx = unlist(strsplit(splitpath[2],"/"))
			meteridx = as.numeric(meteridx[1]) + offset 
		}
	}
	path = pathall[t]
  dataread = read.table(path,header = T,sep="\t")
	currday = as.character(dataread[1,1])
	offsetnam = 0
	if(t > (1+NOMETERS))
	  offsetnam = 1+NOMETERS
	filenams[t] = paste(currday,"-",METERNICKNAMES[meteridx + offsetnam],".txt",sep="")
	print("---------------------")
	print(path)
	print(filenams[t])
	print("---------------------")
	if(type == 1)
	{
	body = paste(body,"\n\n________________________________________________\n\n")
	body = paste(body,currday,acname[t])
	body = paste(body,"\n\n________________________________________________\n\n")
	body = paste(body,"DA [%]:",as.character(dataread[1,2]),"\n\n")
	{
	if(t >= 2)
	{
	body = paste(body,"EAC method-1 (Pac) [kWh]:",as.character(dataread[1,3]),"\n\n")
	body = paste(body,"EAC method-2 (Eac) [kWh]:",as.character(dataread[1,4]),"\n\n")
	TOTALGENCALC = TOTALGENCALC + as.numeric(dataread[1,4])
	body = paste(body,"Yield-1 [kWh/kWp]:",as.character(dataread[1,5]),"\n\n")
	body = paste(body,"Yield-2 [kWh/kWp]:",as.character(dataread[1,6]),"\n\n")
	globMtYld[idxglobMtYld] = as.numeric(dataread[1,6])
	globMtVal[idxglobMtYld] = as.numeric(dataread[1,4])
	idxglobMtYld = idxglobMtYld + 1
	body = paste(body,"PR-1 [%]:",as.character(dataread[1,7]),"\n\n")
	body = paste(body,"PR-2 [%]:",as.character(dataread[1,8]),"\n\n")
	body = paste(body,"Last recorded value [kWh]:",as.character(dataread[1,9]),"\n\n")
    body = paste(body,"Last recorded time:",as.character(dataread[1,10]),"\n\n")
    body = paste(body,"Grid Availability [%]:",as.character(dataread[1,11]),"\n\n")
    body = paste(body,"Plant Availability [%]:",as.character(dataread[1,12]))
	}
	else if( t == 1)
	{
		body = paste(body,"GHI [kWh/m^2]:",as.character(dataread[1,3]),"\n\n")
		GTI = as.numeric(dataread[1,3])
		body = paste(body,"Tamb [C]:",as.character(dataread[1,4]),"\n\n")
		body = paste(body,"Tmod [C]:",as.character(dataread[1,5]),"\n\n")
		body = paste(body,"Tamb solar hours [C]:",as.character(dataread[1,6]),"\n\n")
		body = paste(body,"Tmod solar hours[C]:",as.character(dataread[1,7]))
	}
	}
	next
  }
	InvReadings[meteridx] = as.numeric(dataread[1,7])
	InvAvail[meteridx]= as.numeric(dataread[1,10])
	MYLD[(meteridx-2)] = as.numeric(dataread[1,7])
	}
	body = paste(body,"\n\n________________________________________________\n\n")
	body = paste(body,currday,"Inverters")
	body = paste(body,"\n\n________________________________________________")
	MYLDCPY = MYLD
	if(length(MYLD))
		MYLD = MYLD[complete.cases(MYLD)]
	addwarn = 0
	sddev=meanyld=covar=NA
	if(length(MYLD))
	{
		addwarn = 1
		sddev = round(sdp(MYLD),2)
		meanyld = mean(MYLD)
		covar = round((sdp(MYLD)*100/meanyld),1)
	}
	MYLD = MYLDCPY
	for(t in 1 : noInverters)
	{
		body = paste(body,"\n\n Yield ",acname[(t+1+NOMETERS)],": ",as.character(InvReadings[t]),sep="")
		if( addwarn && is.finite(covar) && covar > 5 &&
		(!is.finite(InvReadings[t]) || (abs(InvReadings[t] - meanyld) > 2*sddev)))
		{
			body = paste(body,">>> Inverter outside range")
		}
	}
	body = paste(body,"\n\nStdev/COV Yields: ",sddev," / ",covar,"%",sep="")
	for(t in 1 : noInverters)
  	{
    		body = paste(body,"\n\n Inverter Availability ",acname[(t+1+NOMETERS)]," [%]: ",as.character(InvAvail[t]),sep="")
    }
	bodyac = paste(bodyac,"\n\nSystem Full Generation [kWh]:",sum(globMtVal))
	bodyac = paste(bodyac,"\n\nSystem Full Yield [kWh]:",round(sum(globMtVal)/sum(INSTCAPM),2))
	bodyac = paste(bodyac,"\n\nSystem Full PR [%]:",
	round(sum(globMtVal)*100/sum(INSTCAPM)/GTI,1),"\n\n")
	for(t in 1 : NOMETERS)
	{
		bodyac = paste(bodyac,"Yield",acname[(t+1)],"[kWh/kWp]:",as.character(globMtYld[t]),"\n\n")
	}
	sdm =round(sdp(globMtYld),3)
	covarm = round(sdm*100/mean(globMtYld),1)
	bodyac = paste(bodyac,"Stdev/COV Yields:",sdm,"/",covarm,"[%]")
	body = paste(bodyac,body,sep="")
	body = gsub("\n ","\n",body)
	firecond = lastMailDate(paste('/home/admin/Start/MasterMail/IN-022C_Mail.txt',sep=""))
  if(currday == firecond)
		return()
  send.mail(from = sender,
            to = recipients,
            subject = paste("Station [IN-022C] Digest",currday),
            body = body,
            smtp = list(host.name = "smtp.office365.com", port = 587, user.name = uname, passwd = pwd, tls = TRUE),
            authenticate = TRUE,
            send = TRUE,
            attach.files = pathall,
            file.names = filenams, # optional paramete
            debug = F)
recordTimeMaster("IN-022C","Mail",currday)
}


path = "/home/admin/Dropbox/FlexiMC_Data/Gen1_Data/[IN-022C]"
path2G = '/home/admin/Dropbox/FlexiMC_Data/Second_Gen/[IN-022C]'
path3G = '/home/admin/Dropbox/FlexiMC_Data/Third_Gen/[IN-022C]'
path4G = '/home/admin/Dropbox/FlexiMC_Data/Fourth_Gen/[IN-022C]'

if(RESETHISTORICAL)
{
	command = paste("rm -rf",path2G)
	system(command)
	command = paste("rm -rf",path3G)
	system(command)
	command = paste("rm -rf",path4G)
	system(command)
}

checkdir(path2G)
checkdir(path3G)

years = dir(path)
stnnickName2 = "IN-022C"
lastdatemail = lastMailDate(paste('/home/admin/Start/MasterMail/',stnnickName2,'_Mail.txt',sep=""))
stopDate = paste("[",stnnickName2,"]-P2I9-",lastdatemail,".txt",sep="")
print(paste('StopDate is',stopDate))
ENDCALL=0
for(x in 1 : length(years))
{
	path2Gyr = paste(path2G,years[x],sep = "/")
	pathyr = paste(path,years[x],sep="/")
	checkdir(path2Gyr)
	months = dir(pathyr)
	for(y in 1 : length(months))
	{
		path2Gmon = paste(path2Gyr,months[y],sep = "/")
		pathmon = paste(pathyr,months[y],sep="/")
		checkdir(path2Gmon)
		stns = dir(pathmon)
		stns = stns[reorderStnPaths]
		dunmun = 0
		for(t in 1 : length(stns))
		{
			type = 1
			if(grepl("OG",stns[t]))
				type = 0
			if(stns[t] == "WMS")
				type = 2
			pathmon1 = paste(pathmon,stns[t],sep="/")
		  days = dir(pathmon1)
		  path2Gmon1 = paste(path2Gmon,stns[t],sep = "/")
			if(!file.exists(path2Gmon1))
			{	
				dir.create(path2Gmon1)
			}

		if(length(days)>0)
		{
		for(z in 1 : length(days))
		{
			if(ENDCALL == 1)
				break
			if((z==length(days)) && (y == length(months)) && (x ==length(years)))
				next
			print(days[z])
			pathfinal = paste(pathmon1,days[z],sep = "/")
			path2Gfinal = paste(path2Gmon1,days[z],sep="/")
			if(RESETHISTORICAL)
			{
				secondGenData(pathfinal,path2Gfinal,type)
			}
			if(!dunmun){
			daysAlive = daysAlive+1
			}
			if(days[z] == stopDate)
			{
				ENDCALL = 1
				print('Hit end call')
				next
			}
		}
		}
		dunmun = 1
		if(ENDCALL == 1)
			break
	}
	if(ENDCALL == 1)
	break
}
if(ENDCALL == 1)
break
}

print('Backlog done')

prevx = x
prevy = y
prevz = z
repeats = 0
SENDMAILTRIGGER = 0
newlen=1
while(1)
{
	recipients = getRecipients("IN-022C","m")
	recordTimeMaster("IN-022C","Bot")
	years = dir(path)
	noyrs = length(years)
	path2Gfinalall = vector('list')
	for(x in prevx : noyrs)
	{
		pathyr = paste(path,years[x],sep="/")
		path2Gyr = paste(path2G,years[x],sep="/")
		checkdir(path2Gyr)
		mons = dir(pathyr)
		nomons = length(mons)
		startmn = prevy
		endmn = nomons
		if(startmn>endmn)
		{
			startmn = 1
			prevx = x-1
			prevz = 1
		}
		for(y in startmn:endmn)
		{
			pathmon = paste(pathyr,mons[y],sep="/")
			path2Gmon = paste(path2Gyr,mons[y],sep="/")
			checkdir(path2Gmon)
			stns = dir(pathmon)
			if(length(stns) < 30) ## Need to make change according to no.of  substations
			{
				print('Station sync issue.. sleeping for an hour')
				Sys.sleep(3600) # Sync issue, meter data comes after MFM and WMS
				stns = dir(pathmon)
			}
			stns = stns[reorderStnPaths]
			for(t in 1 : length(stns))
			{
				newlen = (y-startmn+1) + (x-prevx)
				print(paste('Reset newlen to',newlen))
				type = 1
				if(grepl("OG",stns[t]))
					type = 0
				if(stns[t] == "WMS")
					type = 2
			  pathmon1 = paste(pathmon,stns[t],sep="/")
			  days = dir(pathmon1)
			  path2Gmon1 = paste(path2Gmon,stns[t],sep = "/")
			chkcopydays = days[grepl('Copy',days)]
			if(!file.exists(path2Gmon1))
			{
			dir.create(path2Gmon1)
			}
			if(length(chkcopydays) > 0)
			{
				print('Copy file found they are')
				print(chkcopydays)
				idxflse = match(chkcopydays,days)
				print(paste('idx matches are'),idxflse)
				for(innerin in 1 : length(idxflse))
				{
					command = paste("rm '",pathmon,"/",days[idxflse[innerin]],"'",sep="")
					print(paste('Calling command',command))
					system(command)
				}
				days = days[-idxflse]
			}
			days = days[complete.cases(days)]
			if(t==1)
				referenceDays <<- extractDaysOnly(days)
			else
				days = analyseDays(days,referenceDays)
			nodays = length(days)
			if(y > startmn)
			{
				z = prevz = 1
			}
			if(nodays > 0)
			{
			startz = prevz
			if(startz > nodays)
				startz = nodays
			for(z in startz : nodays)
			{
				if(t == 1)
				{
					path2Gfinalall[[newlen]] = vector('list')
				}
				if(is.na(days[z]))
					next
				pathdays = paste(pathmon1,days[z],sep = "/")
				path2Gfinal = paste(path2Gmon1,days[z],sep="/")
				secondGenData(pathdays,path2Gfinal,type)
				if((z == nodays) && (y == endmn) && (x == noyrs))
				{
					if(!repeats)
					{
						print('No new data')
						repeats = 1
					}
					next
				}
				if(is.na(days[z]))
				{
					print('Day was NA, do next in loop')
					next
				}
			  SENDMAILTRIGGER <<- 1
				if(t == 1)
				{
					splitstr =unlist(strsplit(days[z],"-"))
					daysSend = paste(splitstr[4],splitstr[5],splitstr[6],sep="-")
					print(paste("Sending",daysSend))
					if(performBackWalkCheck(daysSend) == 0)
					{
						print("Sync issue, sleeping for an 1 hour")
						Sys.sleep(3600) # Sync issue -- meter data comes a little later than
					   	             # MFM and WMS
					}
				}
				repeats = 0
				print(paste('New data, calculating digests',days[z]))
				path2Gfinalall[[newlen]][t] = paste(path2Gmon1,days[z],sep="/")
				newlen = newlen + 1
				print(paste('Incremented newlen by 1 to',newlen))
			}
			}
			}
		}
	}
	if(SENDMAILTRIGGER)
	{
	print('Sending mail')
	print(paste('newlen is ',newlen))
	for(lenPaths in 1 : (newlen - 1))
	{
		if(lenPaths < 1)
			next
		print(unlist(path2Gfinalall[[lenPaths]]))
		pathsend = unlist(path2Gfinalall[[lenPaths]])
		daysAlive = daysAlive+1
		sendMail(pathsend)
	}
	SENDMAILTRIGGER <<- 0
	newlen = 1
	print(paste('Reset newlen to',newlen))

	}

	prevx = x
	prevy = y
	prevz = z
	Sys.sleep(3600)
}
print(paste('Exited for some reason x y z values are'),x,y,z)
sink()
