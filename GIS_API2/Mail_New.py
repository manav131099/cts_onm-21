import os
import glob
import pandas as pd
import numpy as np
from numpy import *
import statistics
import math
import smtplib
import email.message
from datetime import date
from datetime import timedelta 
tod = date.today()
yesterday = tod - timedelta(days = 1) 
import time


os.unlink("//home/anusha/GIS/NEW_WAY/newmail.txt")

from datetime import date
today = str(date.today())

today = str(today)
year = today[0:4]
content = []
count_pr = 0
count_pr_b1 = 0
count_pr_b2 = 0
count_energy = 5 #CHANGE LATER
month = today[0:7]
sorted_site = []
sorted_site_GTI = []
print(today)


site = [  'CHANGI', 'CHENNAI', 'CHIPLUN', 'HOSUR', 'HYDERABAD', 'JHAJJHAR', 'KARAIKAL', 'LALRU', 'NEW_DELHI', 'ORAGADAM', 'PAKA', 'PUNE', 'TAK', 'TUAS', 'WOODLANDS', 'NAN', 'KAN', 'CEBU', 'PENANG', 'SUKHA', 'GUDANG', 'MANILA', 'ENSTEK','KORAT','HALDIA','TANGKAK','MPIP','SUPHAN','RAYONG','PHRAE','LOPBURI','UTTARADIT','ROJANA','HO CHI MINH','THAYANG','PHITLOK','CHUMPHON','TAIPING','TANJUNG_PELEPAS','SAMUT_SAKHON','SAMUT_SKRM','CHAKSU','SAMPHRAN','PHNOM_PENH','NEGERI_PERAK','KAMPNGSAEN','BANGPAIN','CHACHOENGSAO','CMIC','BURHANPUR','MAESOT','CHIANGKM','CHIANGKNG','CHONBURI','NAVI_MUMBAI','NELLORE','AHMEDABAD','LILORA','PRACHUAP', 'AURANGABAD', 'BAWAL','BESCOM', 'BHILWARA', 'COIMBATORE', 'KURANGANWALI', 'MALUMBRA', 'NASIK', 'SATNA', 'TIRUNELVELI', 'KHAMGAON', 'AHMEDNAGAR','BHOPAL','DEWAS']

GTI_site =  ['AURANGABAD', 'BAWAL','BESCOM', 'BHILWARA', 'COIMBATORE', 'KURANGANWALI', 'MALUMBRA', 'NASIK', 'SATNA', 'TIRUNELVELI', 'KHAMGAON', 'AHMEDNAGAR','BHOPAL','DEWAS']




sorted_site = sorted(site)
sorted_site_GTI = sorted(GTI_site)

print(sorted_site)

GHI = []
GTI = []
ma = []
yearsum = []
exp_val = []
ma_GTI = []
yearsum_GTI = []

for a in sorted_site:
  path = '/home/admin/Dropbox/GIS_API2/'+a+'/'+a+'_AGGREGATE.txt'
  df = pd.read_csv(path,sep='\t',header=0,skipinitialspace=True)
  today_val = sum(df['GHI'].tail(1))
  GHI.append(today_val)
  ma30d = round(sum(df['GHI'].tail(30))/30,2)
  ma.append(ma30d)
  n = len(df)
  if(n<365):
    s365 = round(sum(df['GHI']),2)
    yearsum.append(s365)
  else:
    s365 = round(sum(df['GHI'].tail(365)),2)
    yearsum.append(s365)
  if(a in sorted_site_GTI):
    today_val_gti = sum(df['GTI'].tail(1))
    GTI.append(today_val_gti)
    ma30d_GTI = round(sum(df['GTI'].tail(30))/30,2)
    ma_GTI.append(ma30d_GTI)
    n = len(df)
    if(n<365):
      s365 = round(sum(df['GTI']),2)
      yearsum_GTI.append(s365)
    else:
      s365 = round(sum(df['GTI'].tail(365)),2)
      yearsum_GTI.append(s365)
    
writepath = '/home/anusha/GIS/NEW_WAY/newmail.txt'
target = open(writepath, 'a+')
   
target.write("Last date received for 71 stations and daily irradiation for the given day:")
target.write('\n')

print(len(sorted_site))
print(len(GHI))
print(len(ma))
print(len(yearsum))
i = 0
for a,b,d,e in zip(sorted_site, GHI, ma, yearsum):
      if(a not in sorted_site_GTI):
        target.write('\n')
        target.write(str(a)+ ': ')
        target.write(str(today)+ ' ,')
        target.write(str(b)+ ' ,')
        #if(a in sorted_site_GTI):
        #  target.write(str(c)+ ',')
        target.write('30-d MA: '+str(d)+ ' ,')
        target.write('365-d sum: '+str(e)+ ' ,')
        target.write('\n')
      if(a in sorted_site_GTI):
          target.write('\n')
          target.write(str(a)+ ': ')
          target.write(str(today)+ ' ,')
          target.write(str(b)+ ' ,')
          target.write(str(GTI[i])+ ',')
          target.write('30-d MA: '+str(d)+ ' ,')
          target.write('365-d sum: '+str(e)+ ' ,')
          target.write('\n')
          i = i+1
  
indices = []
for a,b,d,e in zip(sorted_site, GHI, ma, yearsum):
  if(a in sorted_site_GTI):
    idx = sorted_site.index(a)
    indices.append(idx)



indices  = [x+1 for x in indices]

for i,j,k,l,m in zip(indices,sorted_site_GTI,GTI, ma_GTI, yearsum_GTI):
  sorted_site.insert(i,j)
  GHI.insert(i,k)
  ma.insert(i,l)
  yearsum.insert(i,m)

df = pd.DataFrame(list(zip(sorted_site, GHI, ma, yearsum)), 
               columns =['SITE', 'LastDayValue','Last30MA','Last365DayTot']) 


df1 = pd.read_csv('/home/anusha/GIS/NEW_WAY/Test.txt',sep='\t',header=0,skipinitialspace=True)

result = pd.concat([df, df1], axis=1, sort=False)

print(result)


