import datetime as DT
import requests
import os
from lxml import etree
import pandas as pd
import csv
from datetime import timedelta 
import shutil
import pytz
tz = pytz.timezone('Asia/Calcutta')

#GHI CODE

today=(DT.datetime.now(tz).date()+DT.timedelta(days=-1))
api_key = 'HesHutsorc8mnk95sc'
url = 'https://solargis.info/ws/rest/datadelivery/request?key=%s' % api_key
headers = {'Content-Type': 'application/xml'}
xmlfile = open('/home/admin/CODE/GIS_API2/API_req.xml', 'r')
body = xmlfile.read()
site = ['AURANGABAD', 'BAWAL','BESCOM', 'BHILWARA', 'COIMBATORE', 'KURANGANWALI', 'MALUMBRA', 'NASIK', 'SATNA', 'TIRUNELVELI', 'KHAMGAON', 'AHMEDNAGAR','BHOPAL','ARUNODAYA','DEWAS']
lat = [ 19.837299,28.114228,14.80685, 25.321367, 11.103033, 29.786753, 17.9300, 20.270197, 24.571884, 8.885125,20.729196,19.160566, 23.301148,14.865003,22.938423]
lng = [ 75.237695,76.614849, 76.124664, 74.586903, 76.9851, 75.087425,  76.0080, 73.839717, 80.995512, 77.677131, 76.525509,74.700773, 77.519769,76.910903,76.034573]
tilt = [15, 28, 13, 20, 12, 17, 15, 16, 14, 8, 15, 6, 20,13,20]
azimuth = [180, 180, 180, 180, 180, 180, 180, 180, 180, 180, 180, 180, 180, 180,180]
first_day = today.replace(day=1) 
week_ago = today - DT.timedelta(days=7)
yesterday = today - DT.timedelta(days=1)
#today = yesterday

day = today.day
print("Print Day")
print(day)
month = today.month-1
print(day)
if(day == 2):
  print("Yayyy")
  last_day = today - timedelta(days = 2) 
  first_day_last = last_day.replace(day=1) 
  delta = last_day - first_day_last
  delta = delta.days
  delta = delta+3
 
  
  start = [first_day_last, first_day]
  end = [last_day, today]
  print(start)
  print(end)
  
  for (a, b, c, d, e) in zip(site, lat, lng, tilt, azimuth): 
    response = requests.post(url, data=body.format(Start_Date= start[0], End_Date = end[0], latitude= b , longitude = c, tilt = d, azimuth = e, para = "GHI GTI"), headers=headers)   
    data = []
    ghi_rounded = []
    gti_rounded = []
    ts_rounded = []
    root = etree.fromstring(bytes(response.text, encoding='utf-8'))
    for element in root.iter("*"):
      data.append(element.items())
    tuple_list = [item for t in data for item in t] 
    first_tuple_elements = []
      
    for a_tuple in tuple_list:
      first_tuple_elements.append(a_tuple[1])
      #print(first_tuple_elements)
    ts = first_tuple_elements[1::2]
    for i in ts:
      x = slice(0, 10)
      ts_rounded.append(i[x])
      
    ghi = first_tuple_elements[2::2]
    ghi.pop(0)
    for i in ghi:
      i = i.split()
      
      i[0] = float(i[0])
      i[1] = float(i[1])
      i[0] = round(i[0],2)
      i[1] = round(i[1],2)
      ghi_rounded.append(i[0])
      gti_rounded.append(i[1])
    list_of_tuples = list(zip(ts_rounded, ghi_rounded)) 
    df = pd.DataFrame(list_of_tuples) 
    df = df.iloc[1:]
    ts_rounded.pop(0)
    path = "/home/admin/Dropbox/GIS_API3/"+a+"/"+a+"_AGGREGATE.txt"
      
    path_new = "/home/admin/Dropbox/GIS_API3/"+a+"/"+a+"_AGGREGATENEW.txt"
    lines = open(path).readlines()	
    open(path_new, 'w').writelines(lines[:-delta])
    file = open("/home/admin/Dropbox/GIS_API3/"+a+"/"+a+"_AGGREGATENEW.txt", "a")
    #print(file)
    #print("Written")
    for index in range(len(ts_rounded)):
      file.write(str(ts_rounded[index]) + "\t" + str(ghi_rounded[index]) +  "\t" + str(gti_rounded[index]) + "\n")
    file.close() 
    with open(path_new, 'rb') as f2, open(path, 'wb') as f1:
      shutil.copyfileobj(f2, f1)
      
  for (a, b, c, d, e) in zip(site, lat, lng, tilt, azimuth): 
      response = requests.post(url, data=body.format(Start_Date= start[1], End_Date = end[1], latitude= b , longitude = c, tilt = d, azimuth = e, para = "GHI GTI"), headers=headers) 
      data = []
      ghi_rounded = []
      gti_rounded = []
      ts_rounded = []
      root = etree.fromstring(bytes(response.text, encoding='utf-8'))
      for element in root.iter("*"):
          data.append(element.items())
      tuple_list = [item for t in data for item in t] 
      first_tuple_elements = []
      
      for a_tuple in tuple_list:
          first_tuple_elements.append(a_tuple[1])
      #print(first_tuple_elements)
      ts = first_tuple_elements[1::2]
      for i in ts:
        x = slice(0, 10)
        ts_rounded.append(i[x])
      
      ghi = first_tuple_elements[2::2]
      ghi.pop(0)
      for i in ghi:
        i = i.split()
      
        i[0] = float(i[0])
        i[1] = float(i[1])
        i[0] = round(i[0],2)
        i[1] = round(i[1],2)
        ghi_rounded.append(i[0])
        gti_rounded.append(i[1])
      list_of_tuples = list(zip(ts_rounded, ghi_rounded)) 
      df = pd.DataFrame(list_of_tuples) 
      df = df.iloc[1:]
      ts_rounded.pop(0)
      file = open("/home/admin/Dropbox/GIS_API3/"+a+"/"+a+"_AGGREGATE.txt", "a")
      for index in range(len(ts_rounded)):
          file.write(str(ts_rounded[index]) + "\t" + str(ghi_rounded[index]) +  "\t" + str(gti_rounded[index]) + "\n")
      file.close() 

          
     
      
else: 
  for (a, b, c, d, e) in zip(site, lat, lng, tilt, azimuth): 
      response = requests.post(url, data=body.format(Start_Date= today, End_Date = today, latitude= b , longitude = c, tilt = d, azimuth = e, para = "GHI GTI"), headers=headers)
      
            
      data = []
      ghi_rounded = []
      gti_rounded = []
      ts_rounded = []
      root = etree.fromstring(bytes(response.text, encoding='utf-8'))
      for element in root.iter("*"):
          data.append(element.items())
      tuple_list = [item for t in data for item in t] 
      first_tuple_elements = []
      
      for a_tuple in tuple_list:
          first_tuple_elements.append(a_tuple[1])
      #print(first_tuple_elements)
      ts = first_tuple_elements[1::2]
      for i in ts:
        x = slice(0, 10)
        ts_rounded.append(i[x])
      
      ghi = first_tuple_elements[2::2]
      ghi.pop(0)
      for i in ghi:
        i = i.split()
      
        i[0] = float(i[0])
        i[1] = float(i[1])
        i[0] = round(i[0],2)
        i[1] = round(i[1],2)
        ghi_rounded.append(i[0])
        gti_rounded.append(i[1])
      list_of_tuples = list(zip(ts_rounded, ghi_rounded)) 
      df = pd.DataFrame(list_of_tuples) 
      df = df.iloc[1:]
      ts_rounded.pop(0)
      file = open("/home/admin/Dropbox/GIS_API3/"+a+"/"+a+"_AGGREGATE.txt", "a")
      for index in range(len(ts_rounded)):
          file.write(str(ts_rounded[index]) + "\t" + str(ghi_rounded[index]) +  "\t" + str(gti_rounded[index]) + "\n")
      file.close() 


