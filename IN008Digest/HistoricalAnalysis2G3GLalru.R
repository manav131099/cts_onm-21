system('rm -R "/home/admin/Dropbox/Second Gen/[IN-008X]"')
system('rm -R "/home/admin/Dropbox/Third Gen/[IN-008X]"')

require('mailR')
require('Hmisc')

source('/home/admin/CODE/IN008Digest/LalruMailDigestFunctions.R')
source('/home/admin/CODE/Misc/calcBackLog.R')

path = '/home/admin/Dropbox/Gen 1 Data/[IN-008X]'
writepath2G = '/home/admin/Dropbox/Second Gen/[IN-008X]'
checkdir(writepath2G)

writepath3G = '/home/admin/Dropbox/Third Gen/[IN-008X]'
checkdir(writepath3G)
DAYSALIVE = 0
LAST30DAYSEAC1 = vector('numeric',30)
LAST30DAYSEAC2 = vector('numeric',30)
MONTHTOTEAC1 = 0
MONTHTOTEAC2 = 0
DAYSTHISMONTH = 0
DAYSTHISMONTHAC = 0
EAC1GLOB = 0
EAC2GLOB = 0
idxvec = 1
years = dir(path)
stnnickName2 = "IN-008X"
lastdatemail = lastMailDate(paste('/home/admin/Start/MasterMail/',stnnickName2,'_Mail.txt',sep=""))
stopDate = paste("[",stnnickName2,"-M1] ",lastdatemail,".txt",sep="")
ENDCALL=0
for(x in 1 : length(years))
{
  pathyrs = paste(path,years[x],sep="/")
  writepath2Gyr = paste(writepath2G,years[x],sep="/")
  checkdir(writepath2Gyr)
  writepath3Gyr = paste(writepath3G,years[x],sep="/")
  checkdir(writepath3Gyr)
  months = dir(pathyrs)
  for(y in 1 : length(months))
  {
    MONTHTOTEAC1 = 0
    MONTHTOTEAC2 = 0
    DAYSTHISMONTH = 0
    pathmonths = paste(pathyrs,months[y],sep="/")
    writepath2Gmon = paste(writepath2Gyr,months[y],sep="/")
    checkdir(writepath2Gmon)
    writepath3Gfinal = paste(writepath3Gyr,"/[IN-008X] ",months[y],".txt",sep="")
    stations = dir(pathmonths)
    pathdays = paste(pathmonths,stations[1],sep="/")
    pathdays2 = paste(pathmonths,stations[2],sep="/")
    writepath2Gdays = paste(writepath2Gmon,stations[1],sep="/")
    writepath2Gdays2 = paste(writepath2Gmon,stations[2],sep="/")
    checkdir(writepath2Gdays)
    checkdir(writepath2Gdays2)
    days = dir(pathdays)
    temp = unlist(strsplit(days[1]," "))
    temp = as.Date(substr(temp[2],1,10),"%Y-%m-%d")
    DAYSTHISMONTHAC = monthDays(temp)
    days2 = dir(pathdays2)
      for(t in 1 : length(days))
      {
        if(ENDCALL==1)
					break
				{
          if(x == 1 && y == 1 && t == 1)
          {
            DOB = unlist(strsplit(days[t]," "))
            DOB = substr(DOB[2],1,10)
            DOB = as.Date(DOB,"%Y-%m-%d")
          }
          else if(x == length(years) && y == length(months) && t == length(days))
          {
            next
          }
        }
        DAYSALIVE = DAYSALIVE + 1
        writepath2Gfinal = paste(writepath2Gdays,"/",days[t],sep="")
        writepath2Gfinal2 = paste(writepath2Gdays2,"/",days2[t],sep="")
        readpath = paste(pathdays,days[t],sep="/")
        readpath2 = paste(pathdays2,days2[t],sep="/")
				METERCALLED <<- 1
        df1 = secondGenData(readpath,writepath2Gfinal)
				METERCALLED <<- 2
        df2 = secondGenData(readpath2,writepath2Gfinal2)
        tots = thirdGenData(writepath2Gfinal,writepath2Gfinal2,writepath3Gfinal)
        LAST30DAYSEAC1[[idxvec]] = tots[1]
        LAST30DAYSEAC2[[idxvec]] = tots[2]
        MONTHTOTEAC1 = MONTHTOTEAC1 + tots[1]
        MONTHTOTEAC2 = MONTHTOTEAC2 + tots[2]
        DAYSTHISMONTH = DAYSTHISMONTH + 1
        EAC1GLOB = EAC1GLOB + tots[1]
        EAC2GLOB = EAC2GLOB + tots[2]
        idxvec = (idxvec + 1) %% 30
        if(idxvec == 0)
        {
          idxvec = 1
        }
				if(days[t] == stopDate)
				{
					ENDCALL = 1
				}
      }
				if(ENDCALL==1)
				break
    }
		if(ENDCALL==1)
		break
}
print('Historical Analysis Done')
