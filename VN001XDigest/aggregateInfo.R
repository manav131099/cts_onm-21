source('/home/admin/CODE/common/aggregate.R')

registerMeterList("VN-001X",c("Main","I1","I2","I3","I4","I5","I6","InSameFile"))
aggNameTemplate = getNameTemplate()
aggColTemplate = getColumnTemplate()

aggColTemplate[1] = 1 #Column no for date
aggColTemplate[2] = 2 #Column no for DA
aggColTemplate[3] = NA #column for LastRead
aggColTemplate[4] = NA #column for LastTime
aggColTemplate[5] = 10 #column for Eac-1
aggColTemplate[6] = NA #column for Eac-2
aggColTemplate[7] = 13 #column for Yld-1
aggColTemplate[8] = NA #column for Yld-2
aggColTemplate[9] = 15 #column for PR-1
aggColTemplate[10] = NA #column for PR-2
aggColTemplate[11] = 14 #column for Irr
aggColTemplate[12] = "Self" # IrrSrc Value
aggColTemplate[13] = NA #column for Tamb
aggColTemplate[14] = 10 #column for Tmod
aggColTemplate[15] = NA #column for Hamb

registerColumnList("VN-001X","Main",aggNameTemplate,aggColTemplate)

aggNameTemplate = getNameTemplate()
aggColTemplate = getColumnTemplate()

aggColTemplate[1] = 1 #Column no for date
aggColTemplate[2] = 2 #Column no for DA
aggColTemplate[3] = NA #column for LastRead
aggColTemplate[4] = NA #column for LastTime
aggColTemplate[5] = 4 #column for Eac-1
aggColTemplate[6] = NA #column for Eac-2
aggColTemplate[7] = 16 #column for Yld-1
aggColTemplate[8] = NA #column for Yld-2
aggColTemplate[9] = NA #column for PR-1
aggColTemplate[10] = NA #column for PR-2
aggColTemplate[11] = 14 #column for Irr
aggColTemplate[12] = "Self" # IrrSrc Value
aggColTemplate[13] = NA #column for Tamb
aggColTemplate[14] = 10 #column for Tmod
aggColTemplate[15] = NA #column for Hamb

registerColumnList("VN-001X","I1",aggNameTemplate,aggColTemplate)

aggNameTemplate = getNameTemplate()
aggColTemplate = getColumnTemplate()

aggColTemplate[1] = 1 #Column no for date
aggColTemplate[2] = 2 #Column no for DA
aggColTemplate[3] = NA #column for LastRead
aggColTemplate[4] = NA #column for LastTime
aggColTemplate[5] = 5 #column for Eac-1
aggColTemplate[6] = NA #column for Eac-2
aggColTemplate[7] = 17 #column for Yld-1
aggColTemplate[8] = NA #column for Yld-2
aggColTemplate[9] = NA #column for PR-1
aggColTemplate[10] = NA #column for PR-2
aggColTemplate[11] = 14 #column for Irr
aggColTemplate[12] = "Self" # IrrSrc Value
aggColTemplate[13] = NA #column for Tamb
aggColTemplate[14] = 10 #column for Tmod
aggColTemplate[15] = NA #column for Hamb

registerColumnList("VN-001X","I2",aggNameTemplate,aggColTemplate)

aggNameTemplate = getNameTemplate()
aggColTemplate = getColumnTemplate()

aggColTemplate[1] = 1 #Column no for date
aggColTemplate[2] = 2 #Column no for DA
aggColTemplate[3] = NA #column for LastRead
aggColTemplate[4] = NA #column for LastTime
aggColTemplate[5] = 6 #column for Eac-1
aggColTemplate[6] = NA #column for Eac-2
aggColTemplate[7] = 18 #column for Yld-1
aggColTemplate[8] = NA #column for Yld-2
aggColTemplate[9] = NA #column for PR-1
aggColTemplate[10] = NA #column for PR-2
aggColTemplate[11] = 14 #column for Irr
aggColTemplate[12] = "Self" # IrrSrc Value
aggColTemplate[13] = NA #column for Tamb
aggColTemplate[14] = 10 #column for Tmod
aggColTemplate[15] = NA #column for Hamb

registerColumnList("VN-001X","I3",aggNameTemplate,aggColTemplate)

aggNameTemplate = getNameTemplate()
aggColTemplate = getColumnTemplate()

aggColTemplate[1] = 1 #Column no for date
aggColTemplate[2] = 2 #Column no for DA
aggColTemplate[3] = NA #column for LastRead
aggColTemplate[4] = NA #column for LastTime
aggColTemplate[5] = 7 #column for Eac-1
aggColTemplate[6] = NA #column for Eac-2
aggColTemplate[7] = 19 #column for Yld-1
aggColTemplate[8] = NA #column for Yld-2
aggColTemplate[9] = NA #column for PR-1
aggColTemplate[10] = NA #column for PR-2
aggColTemplate[11] = 14 #column for Irr
aggColTemplate[12] = "Self" # IrrSrc Value
aggColTemplate[13] = NA #column for Tamb
aggColTemplate[14] = 10 #column for Tmod
aggColTemplate[15] = NA #column for Hamb

registerColumnList("VN-001X","I4",aggNameTemplate,aggColTemplate)

aggNameTemplate = getNameTemplate()
aggColTemplate = getColumnTemplate()

aggColTemplate[1] = 1 #Column no for date
aggColTemplate[2] = 2 #Column no for DA
aggColTemplate[3] = NA #column for LastRead
aggColTemplate[4] = NA #column for LastTime
aggColTemplate[5] = 8 #column for Eac-1
aggColTemplate[6] = NA #column for Eac-2
aggColTemplate[7] = 20 #column for Yld-1
aggColTemplate[8] = NA #column for Yld-2
aggColTemplate[9] = NA #column for PR-1
aggColTemplate[10] = NA #column for PR-2
aggColTemplate[11] = 14 #column for Irr
aggColTemplate[12] = "Self" # IrrSrc Value
aggColTemplate[13] = NA #column for Tamb
aggColTemplate[14] = 10 #column for Tmod
aggColTemplate[15] = NA #column for Hamb

registerColumnList("VN-001X","I5",aggNameTemplate,aggColTemplate)

aggNameTemplate = getNameTemplate()
aggColTemplate = getColumnTemplate()

aggColTemplate[1] = 1 #Column no for date
aggColTemplate[2] = 2 #Column no for DA
aggColTemplate[3] = NA #column for LastRead
aggColTemplate[4] = NA #column for LastTime
aggColTemplate[5] = 9 #column for Eac-1
aggColTemplate[6] = NA #column for Eac-2
aggColTemplate[7] = 21 #column for Yld-1
aggColTemplate[8] = NA #column for Yld-2
aggColTemplate[9] = NA #column for PR-1
aggColTemplate[10] = NA #column for PR-2
aggColTemplate[11] = 14 #column for Irr
aggColTemplate[12] = "Self" # IrrSrc Value
aggColTemplate[13] = NA #column for Tamb
aggColTemplate[14] = 10 #column for Tmod
aggColTemplate[15] = NA #column for Hamb

registerColumnList("VN-001X","I6",aggNameTemplate,aggColTemplate)
