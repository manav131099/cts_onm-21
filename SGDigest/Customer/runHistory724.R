source('/home/admin/CODE/common/math.R')
checkdir = function(x)
{
  if(!file.exists(x))
  {
    dir.create(x)
  }
}
rf = function(x)
{
  return(format(round(x,2),nsmall=2))
}
rf1 = function(x)
{
  return(format(round(x,1),nsmall=1))
}
rf3 = function(x)
{
  return(format(round(x,3),nsmall=3))
}

prepareSummFab35 = function(date,wait)
{
  yr = substr(date,1,4)
  mon = substr(date,1,7)
  pathMain = paste("/home/admin/Dropbox/Second Gen/[SG-725S]/",yr,"/",mon,"/[SG-725S] ",date,".txt",sep="")
  colsIntreset = c(4,5,6,7,9,10,11,12,13,14)
  vals = unlist(rep(0,length(colsIntreset)))
  vals[7] = ""
  {
    if(!file.exists(pathMain) && wait)
    {
      print(paste(pathMain,"Doesnt exist sleeping 2hrs"))
      Sys.sleep(7200) # Wait two hours for the file.. good odds rest of the files will land
      return(prepareSummFab35(date,0))
    }
    else if(file.exists(pathMain))
    {
      dataread = read.table(pathMain,header=T,sep="\t",stringsAsFactors=F)
      vals = dataread[1,colsIntreset]
    }
  }
  return(vals)
}

prepareSummFab7 = function(date,wait)
{
  yr = substr(date,1,4)
  mon = substr(date,1,7)
  pathMain = paste("/home/admin/Dropbox/Second Gen/[SG-726S]/",yr,"/",mon,"/[SG-726S] ",date,".txt",sep="")
  colsIntreset = c(4,5,6,7,9,10,11,12,14,15,16,17,18,19,20,22,23,24,25,26,27,28,13,21)
  vals = unlist(rep(0,length(colsIntreset)))
  vals[7] = ""
  vals[15] = ""
  {
    if(!file.exists(pathMain) && wait)
    {
      print(paste(pathMain,"Doesnt exist sleeping 2hrs"))
      Sys.sleep(7200) # Wait two hours for the file.. good odds rest of the files will land
      return(prepareSummFab7(date,0))
    }
    else if(file.exists(pathMain))
    {
      dataread = read.table(pathMain,header=T,sep="\t",stringsAsFactors=F)
      vals = dataread[1,colsIntreset]
    }
  }
  print("returning vals")
  print(vals)
  print("####################")
  return(vals)
}

prepareSummFab7G = function(date,wait)
{
  yr = substr(date,1,4)
  mon = substr(date,1,7)
  pathMain = paste("/home/admin/Dropbox/Second Gen/[SG-727S]/",yr,"/",mon,"/[SG-727S] ",date,".txt",sep="")
  colsIntreset = c(4,5,6,7,9,10,11,12,13,14)
  vals = unlist(rep(0,length(colsIntreset)))
  vals[7] = ""
  {
    if(!file.exists(pathMain) && wait)
    {
      print(paste(pathMain,"Doesnt exist sleeping 2hrs"))
      Sys.sleep(7200) # Wait two hours for the file.. good odds rest of the files will land
      return(prepareSummFab7G(date,0))
    }
    else if(file.exists(pathMain))
    {
      dataread = read.table(pathMain,header=T,sep="\t",stringsAsFactors=F)
      vals = dataread[1,colsIntreset]
    }
  }
  return(vals)
}

prepareSumm = function(dataread,wait)
{
  APPENDWARNING <<- 0
  da = nrow(dataread)
  thresh = 5/1000
  gsi1=gsi2=tambsh=tambmxsh=tambmnsh=tamb=tambmx=tambmn=hamb=hambmx=hambmn=NA
  hambsh=hambmxsh=hambmnsh=tsi=tsish=tsimx=tsimxsh=tsimn=tsimnsh=NA
  
  dataread2 = dataread[complete.cases(dataread[,3]),3]
  if(length(dataread2))
    gsi1 = sum(dataread[complete.cases(dataread[,3]),3])/60000
  dataread2 = dataread[complete.cases(dataread[,4]),4]
  if(length(dataread2))
    gsi2 = sum(dataread[complete.cases(dataread[,4]),4])/60000
  #  gsi2 = sum(dataread[complete.cases(dataread[,4]),4])/60000
  #  gsismp = sum(dataread[complete.cases(dataread[,5]),5])/60000
  subdata = dataread[complete.cases(dataread[,3]),]
  subdata = subdata[as.numeric(subdata[,3]) > thresh,]
  
  dataread2 = subdata[complete.cases(subdata[,6]),6]
  if(length(dataread2))
  {
    tambsh = mean(dataread2)
    tambmxsh = max(dataread2)
    tambmnsh = min(dataread2) 
  }
  dataread2 = subdata[complete.cases(subdata[,7]),7]
  if(length(dataread2))
  {
    hambsh = mean(dataread2)
    hambmxsh = max(dataread2)
    hambmnsh = min(dataread2)
  }
  dataread2 = subdata[complete.cases(subdata[,8]),8]
  if(length(dataread2))
  {
    tsish = mean(dataread2)
    tsimxsh = max(dataread2)
    tsimnsh = min(dataread2)
  }
  
  dataread2 = dataread[complete.cases(dataread[,6]),6]
  if(length(dataread2))
  {
    tamb = mean(dataread2)
    tambmx = max(dataread2)
    tambmn = min(dataread2) 
  }
  dataread2 = dataread[complete.cases(dataread[,7]),7]
  if(length(dataread2))
  {
    hamb = mean(dataread2)
    hambmx = max(dataread2)
    hambmn = min(dataread2)
  }
  dataread2 = dataread[complete.cases(dataread[,8]),8]
  if(length(dataread2))
  {
    tsi = mean(dataread2)
    tsimx = max(dataread2)
    tsimn = min(dataread2)
  }
  
  
  gsirat = gsi1 / gsi2
  #  smprat = gsismp / gsi2
  
  Eac11 = Eac21 =LastR1=LastT1=LastR2 = LastR3 = NA
  
  dataread2 = dataread[complete.cases(dataread[,24]),24]
  if(length(dataread2))
  {
    Eac11 = sum(dataread[complete.cases(dataread[,24]),24])/60
  }
  dataread2 = dataread[complete.cases(dataread[,42]),42]
  if(length(dataread2))
  {
    Eac21 = as.numeric(dataread2[length(dataread2)])  - as.numeric(dataread2[1])
    LastR1 = dataread2[length(dataread2)]
    LastT1 = as.character(dataread[complete.cases(as.numeric(dataread[,42])),1])
    print(paste("length of T1",length(LastT1)))
    LastT1 = LastT1[length(LastT1)]
  }
  dataread2 = dataread[complete.cases(dataread[,39]),39]
  if(length(dataread2))
    LastR2 = dataread2[length(dataread2)]
  dataread2 = dataread[complete.cases(dataread[,40]),40]
  if(length(dataread2))
    LastR3 = dataread2[length(dataread2)]
  PR11 = (Eac11 * 100) / (gsi1*794.97)
  PR21 = (Eac21 * 100) / (gsi1*794.97)
  PR11si = (Eac11 * 100) / (gsi2*794.97)
  PR21si = (Eac21 * 100) / (gsi2*794.97)
  YLD11 = 0.01*PR11 * gsi1
  YLD21 = 0.01*PR21 * gsi1
  dateAc = NA
  
  if(nrow(dataread))
    dateAc = substr(dataread[1,1],1,10)
  
  fab35Data = prepareSummFab35(dateAc,wait)
  fab7 = prepareSummFab7(dateAc,wait)
  fab7G = prepareSummFab7G(dateAc,wait)
  fab35 = fab35Data
  
  yrcur = as.numeric(substr(dateAc,1,4))
  moncur = as.numeric(substr(dateAc,6,7))
  daycurr = as.numeric(substr(dateAc,9,10))
  {
    if(yrcur > 2019 || (yrcur == 2019 && moncur > 10) || (yrcur == 2019 && moncur == 10 && daycurr > 21))
    {
      FullSiteProd = Eac21 + as.numeric(fab35[2]) + as.numeric(fab7[2]) + as.numeric(fab7[10]) + as.numeric(fab7G[2])
      denomUse = 3067.57
    }
    else
    {
      FullSiteProd = Eac21 + as.numeric(fab35[2])
      denomUse = 1837.77
    }
  }
  FullSiteYld=(FullSiteProd / denomUse)
  FullSitePR=(FullSiteProd *100/ denomUse/gsi1)
  FullSitePRsi=(FullSiteProd *100/ denomUse/gsi2)
  
  ylds = c(YLD21,as.numeric(fab35[4]))
  if(yrcur > 2019 || ((yrcur == 2019 && moncur > 10) || (yrcur == 2019 && moncur == 10 && daycurr > 21)))
  {
    ylds = c(YLD21,as.numeric(fab35[4]),as.numeric(fab7[4]),as.numeric(fab7[12]),as.numeric(fab7G[4]))
  }
  ylds = ylds[complete.cases(ylds)]
  
  sddev = sdp(ylds)
  cov = sddev * 100 / mean(ylds)
  
  datawrite = data.frame(Date = dateAc,
                         PtsRec = rf(da),
                         GPy = rf(gsi1), 
                         GSi = rf(gsi2), 
                         GRat = rf3(gsirat),
                         Tamb = rf(tamb),
                         TambMx = rf1(tambmx),
                         TambMn = rf1(tambmn),
                         TambSH = rf1(tambsh),
                         TambMxSH = rf1(tambmxsh),
                         TambMnSH = rf1(tambmnsh),
                         Hamb = rf1(hamb),
                         HambMx = rf1(hambmx),
                         HambMn = rf1(hambmn),
                         HambSH = rf1(hambsh),
                         HambMxSH = rf1(hambmxsh),
                         HambMnSH = rf1(hambmnsh),
                         TSi = rf1(tsi),
                         TSiMx = rf1(tsimx),
                         TSiMn = rf1(tsimn),
                         TSiSH = rf1(tsish),
                         TSiMxSH = rf1(tsimxsh),
                         TSiMnSH = rf1(tsimnsh),
                         SDYld = rf3(sddev),
                         CovYld = rf1(cov),
                         FullSite = rf(FullSiteProd),
                         FullSiteYld = rf(FullSiteYld),
                         FullSitePR = rf1(FullSitePR),
                         FullSitePRSi = rf1(FullSitePRsi),
                         Eac11 = rf(Eac11),
                         Eac21 = rf(Eac21),
                         Yld11 = rf(YLD11),
                         Yld21 = rf(YLD21),
                         PR11 = rf1(PR11),
                         PR21 = rf1(PR21),
                         PR11Si = rf1(PR11si),
                         PR21Si = rf1(PR21si),
                         LastR1 = LastR1,
                         LastT1 = LastT1,
                         LastR2 = LastR2,
                         LastR3 = LastR3,
                         Eac1Fab35 = as.character(fab35Data[1]),
                         Eac2Fab35 = as.character(fab35Data[2]),
                         Yld1Fab35 = as.character(fab35Data[3]),
                         Yld2Fab35 = as.character(fab35Data[4]),
                         PR1Fab35 = as.character(fab35Data[5]),
                         PR2Fab35 = as.character(fab35Data[6]),
                         LastTimeFab35 = as.character(fab35Data[7]),
                         LastReadFab35 = as.character(fab35Data[8]),
                         LastReadFab35Rec = as.character(fab35Data[9]),
                         LastReadFab35RecDel = as.character(fab35Data[10]),
                         Eac1Fab71 = as.numeric(fab7[1]),
                         Eac2Fab71 = as.numeric(fab7[2]),
                         Yld1Fab71 = as.numeric(fab7[3]),
                         Yld2Fab71 = as.numeric(fab7[4]),
                         PR1Fab71 = as.numeric(fab7[5]),
                         PR2Fab71 = as.numeric(fab7[6]),
                         LastTimeFab71 = as.character(fab7[7]),
                         LastReadFab71 = as.numeric(fab7[8]),
                         Eac1Fab72 = as.numeric(fab7[9]),
                         Eac2Fab72 = as.numeric(fab7[10]),
                         Yld1Fab72 = as.numeric(fab7[11]),
                         Yld2Fab72 = as.numeric(fab7[12]),
                         PR1Fab72 = as.numeric(fab7[13]),
                         PR2Fab72 = as.numeric(fab7[14]),
                         LastTimeFab72 = as.character(fab7[15]),
                         LastReadFab72 = as.numeric(fab7[16]),
                         Eac1Fab7Tot = as.numeric(fab7[17]),
                         Eac2Fab7Tot = as.numeric(fab7[18]),
                         Yld1Fab7Tot = as.numeric(fab7[19]),
                         Yld2Fab7Tot = as.numeric(fab7[20]),
                         PR1Fab7Tot = as.numeric(fab7[21]),
                         PR2Fab7Tot = as.numeric(fab7[22]),
                         somenewcol1 = as.numeric(fab7[23]),
                         somenewcol2 = as.numeric(fab7[24]),
                         Eac1Fab7G = as.character(fab7G[1]),
                         Eac2Fab7G = as.character(fab7G[2]),
                         Yld1Fab7G = as.character(fab7G[3]),
                         Yld2Fab7G = as.character(fab7G[4]),
                         PR1Fab7G = as.character(fab7G[5]),
                         PR2Fab7G = as.character(fab7G[6]),
                         LastTimeFab7G = as.character(fab7G[7]),
                         LastReadFab7G = as.character(fab7G[8]),
                         LastReadFab7GRec = as.character(fab7G[9]),
                         LastReadFab7GRecDel = as.character(fab7G[10]),
                         stringsAsFactors=F
  )
  print(paste('last time is', as.character(datawrite[1,39])))
  datawrite
}
rewriteSumm = function(datawrite)
{
  df = datawrite[,c(1,2,4,31,40,41,38,39,43,48,49,50,51,53,58,59,61,66,67,69,76,77,78,79,80,81,82,83)]
  print(paste('last time in rewrite is', as.character(datawrite[1,39])))
  df[1,2] = round(as.numeric(df[1,2])/14.4,1)
  df[1,8] = as.character(datawrite[1,39])
  print(paste('last time-2 in rewrite is', as.character(df[1,8])))
  colnames(df) = c("Date","Data-Availability [%]","Irradiation [kWh/m2]","Daily Energy Fab-2 [kWh]",
                   "Act_E-Del_Fab-2","Act_E-Rec_Fab-2","Act_E-Del-Rec_Fab-2","Time Stamp","Daily Energy Fab-3/5 [kWh]",
                   "Time Stamp","Act_E-Del_Fab-3/5","Act_E-Rec_Fab-3/5","Act_E-Del-Rec_Fab-3/5",
                   "Eac-Fab7_1","LastTime-Fab7_1","LastRead-Fab7_1","Eac-Fab7_2","LastTime-Fab7_2","LastRead-Fab7_2","EacTot-Fab7",
                   "Eac1-Fab7G","Eac2-Fab7G","Yld1-Fab7G","Yld2-Fab7G","PR1-Fab7G","PR2-Fab7G","LastTime-Fab7G")
  df
}

path = "/home/admin/Dropbox/Cleantechsolar/1min/[724]"
pathwrite = "/home/admin/Dropbox/Customer/[SG-9005S]"
checkdir(pathwrite)
years = dir(path)
x=y=z=1
for(x in 1 : length(years))
{
  pathyear = paste(path,years[x],sep="/")
  writeyear = paste(pathwrite,years[x],sep="/")
  checkdir(writeyear)
  months = dir(pathyear)
  for(y in  1: length(months))
  {
    pathmonth = paste(pathyear,months[y],sep="/")
    writemonth = paste(writeyear,months[y],sep="/")
    checkdir(writemonth)
    days = dir(pathmonth)
    sumfilename = paste("[SG-9005S] ",substr(months[y],3,4),substr(months[y],6,7),".txt",sep="")
    for(z in 1 : length(days))
    {
      dataread = read.table(paste(pathmonth,days[z],sep="/"),sep="\t",header = T)
      datawrite = prepareSumm(dataread,0)
      datasum = rewriteSumm(datawrite)
      currdayw = gsub("724","SG-9005S",days[z])
      write.table(datawrite,file = paste(writemonth,currdayw,sep="/"),append = F,sep="\t",row.names = F,col.names = T)
      {
        if(!file.exists(paste(writemonth,sumfilename,sep="/")) || (x == 1 && y == 1 && z==1))
        {
          write.table(datasum,file = paste(writemonth,sumfilename,sep="/"),append = F,sep="\t",row.names = F,col.names = T)
        }
        else 
        {
          write.table(datasum,file = paste(writemonth,sumfilename,sep="/"),append = T,sep="\t",row.names = F,col.names = F)
        }
      }
    }
  }
}
