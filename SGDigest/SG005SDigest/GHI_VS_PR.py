import pandas as pd
import os
import numpy as np
import pandas as pd
import datetime
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.patches as mpatch
import matplotlib.dates as mdates
from scipy import stats
import matplotlib.dates as mdates
from matplotlib.ticker import MaxNLocator
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_squared_error, r2_score
import matplotlib.patches as mpatches
import pyodbc
import sys
from bisect import bisect
from matplotlib.text import Annotation
from matplotlib.transforms import Affine2D

server = 'cleantechsolar.database.windows.net'
database = 'Cleantech Meter Readings'
username = 'RohanKN'
password = 'R@h@nKN1'
driver= '{ODBC Driver 13 for SQL Server}'

class LineAnnotation(Annotation):
    def __init__(
        self, text, line, x , line_type, xytext=(0, 5), textcoords="offset points", **kwargs
    ):
        assert textcoords.startswith(
            "offset "
        ), "*textcoords* must be 'offset points' or 'offset pixels'"

        self.line = line
        self.xytext = xytext

        # Determine points of line immediately to the left and right of x
        xs, ys = line.get_data()

        assert (
            np.diff(xs) >= 0
        ).all(), "*line* must be a graph with datapoints in increasing x order"

        i = np.clip(bisect(xs, x), 1, len(xs) + 1)
        self.neighbours = n1, n2 = np.asarray([(xs[i - 1], ys[i - 1]), (xs[i], ys[i])])

        # Calculate y by interpolating neighbouring points
        if(line_type == 'below'):
            y = n1[1] + ((x - n1[0]) * (n2[1] - n1[1]) / (n2[0] - n1[0]))-3
        else:
            y = n1[1] + ((x - n1[0]) * (n2[1] - n1[1]) / (n2[0] - n1[0]))

        kwargs = {
            "horizontalalignment": "center",
            "rotation_mode": "anchor",
            **kwargs,
        }
        super().__init__(text, (x, y), xytext=xytext, textcoords=textcoords, **kwargs)

    def get_rotation(self):

        transData = self.line.get_transform()
        dx, dy = np.diff(transData.transform(self.neighbours), axis=0).squeeze()
        return np.rad2deg(np.arctan2(dy, dx))

    def update_positions(self, renderer):
        xytext = Affine2D().rotate_deg(self.get_rotation()).transform(self.xytext)
        self.set_position(xytext)
        super().update_positions(renderer)


def line_annotate(text, line, x , line_type, *args, **kwargs):
    ax = line.axes
    a = LineAnnotation(text, line, x, line_type, *args, **kwargs)
    if "clip_on" in kwargs:
        a.set_clip_path(ax.patch)
    ax.add_artist(a)
    return a

def get_total_eac(date):
    connStr = pyodbc.connect('DRIVER='+driver+';PORT=1433;SERVER='+server+';PORT=1443;DATABASE='+database+';UID='+username+';PWD='+ password)
    SQL_Query = pd.read_sql_query("SELECT SUM([Eac-MFM]) AS SUM FROM [dbo].[Stations_Data] WHERE Station_Id=(SELECT Station_Id FROM [dbo].[Stations] WHERE Station_Name='SG-005') AND Date='"+date+"' ", connStr)
    df_sum = pd.DataFrame(SQL_Query, columns=['SUM'])
    return df_sum
    

def abline(slope, intercept):
    x_vals = np.array(ax.get_xlim())
    y_vals = intercept + slope * x_vals
    y_vals2 = (intercept+0.05*intercept) + slope * x_vals
    y_vals3 = (intercept-0.05*intercept) + slope * x_vals
    plt.plot(x_vals, y_vals,color='darkgreen')
    line_1 = plt.plot(x_vals, y_vals2,'--',color='#008000')
    line_2 = plt.plot(x_vals, y_vals3,'--',color='#008000')
    line_annotate('+5% variation from mean',line_1[0],4,'above',color='#008000')
    line_annotate('-5% variation from mean',line_2[0],3.8,'below',color='#008000')

    
font = {'size'   : 11}

matplotlib.rc('font', **font)


date_today = sys.argv[1]


path='/home/admin/Graphs/Graph_Extract/SG-005/[SG-005] Graph '+date_today+' - PR Evolution.txt'
df=pd.read_csv(path,sep='\t')

df=df[df['Date']>='2020-01-01']
df_temp = df.copy()

df_today = df[df['Date']==date_today]
df = df[(df['Date']!=date_today)]

fig3 = plt.figure(num=None, figsize=(13.2  , 8.8))

ax = fig3.add_subplot(111)

shape_dict = {1:'1',2:'2',3:'<',4:'4',5:'p',6:'h',7:'v',8:'>',9:'s',10:'^',11:'o',12:'d'}
temp_dict = {}
temp_dict_2 = {}

dates = df['Date'].tolist()
g_vals = df['GHI'].tolist()
pr_vals = df['PR'].tolist()


for index,i in enumerate(pr_vals):
    month = datetime.datetime.strptime(dates[index], '%Y-%m-%d').month
    temp_dict[month] = datetime.datetime.strptime(dates[index], '%Y-%m-%d').strftime('%b')
    if(g_vals[index]<2):
        c='darkblue'
    elif(g_vals[index]>=2 and g_vals[index]<=4):
        c='deepskyblue'
    elif(g_vals[index]>=4 and g_vals[index]<=6):
        c='orange'
    elif(g_vals[index]>6):
        c='chocolate'
    if(month == 1):
        ln14=ax.scatter(g_vals[index], i, marker=shape_dict[month],color=c,s=60, alpha=0.75, zorder=3)
        temp_dict_2[month] = ln14
    if(month == 2):
        ln13=ax.scatter(g_vals[index], i, marker=shape_dict[month],color=c,s=60, alpha=0.75, zorder=3)
        temp_dict_2[month] = ln13
    if(month == 3):
        ln12=ax.scatter(g_vals[index], i, marker=shape_dict[month],color=c,s=60, alpha=0.75, zorder=3)
        temp_dict_2[month] = ln12
    if(month == 4):
        ln11=ax.scatter(g_vals[index], i, marker=shape_dict[month],color=c,s=60, alpha=0.75, zorder=3)
        temp_dict_2[month] = ln11
    if(month == 5):
        ln10=ax.scatter(g_vals[index], i, marker=shape_dict[month],color=c,s=60, alpha=0.75, zorder=3)
        temp_dict_2[month] = ln10
    if(month == 6):
        ln9=ax.scatter(g_vals[index], i, marker=shape_dict[month],color=c,s=60, alpha=0.75, zorder=3)
        temp_dict_2[month] = ln9
    if(month == 7):
        ln8=ax.scatter(g_vals[index], i, marker=shape_dict[month],color=c,s=60, alpha=0.75, zorder=3)
        temp_dict_2[month] = ln8
    if(month == 8):
        ln7=ax.scatter(g_vals[index], i, marker=shape_dict[month],color=c,s=60, alpha=0.75, zorder=3)
        temp_dict_2[month] = ln7
    if(month == 9):
        ln1=ax.scatter(g_vals[index], i, marker=shape_dict[month],color=c,s=60, alpha=0.75, zorder=3)
        temp_dict_2[month] = ln1
    elif(month == 10):
        ln2=ax.scatter(g_vals[index], i, marker=shape_dict[month],color=c,s=60, alpha=0.75, zorder=3)
        temp_dict_2[month] = ln2
    elif(month == 11):
        ln3=ax.scatter(g_vals[index], i, marker=shape_dict[month],color=c,s=60, alpha=0.75, zorder=3)
        temp_dict_2[month] = ln3
    elif(month == 12):
        ln6=ax.scatter(g_vals[index], i, marker=shape_dict[month],color=c,s=60, alpha=0.75, zorder=3)
        temp_dict_2[month] = ln6
   

today_g = df_today['GHI'].values[0]
today_pr = df_today['PR'].values[0]
ln5 = ax.scatter(today_g, today_pr, marker='*',color='black',s=60, alpha=0.75, zorder=3)
temp_dict_2[14] = ln5

g_patch = mpatches.Patch(color='white', label='GHI [kWh/m2]')
kh_patch = mpatches.Patch(color='darkblue', label='< 2')
th_patch = mpatches.Patch(color='deepskyblue', label='2 ~ 4')
vn_patch = mpatches.Patch(color='orange', label='4 ~ 6')
ph_patch = mpatches.Patch(color='chocolate', label='> 6')

X = df['GHI'].values.reshape(-1, 1)
Y = df['PR'].values.reshape(-1, 1)
linear_regressor = LinearRegression()  # create object for the class
linear_regressor.fit(X, Y)  # perform linear regression
slope=linear_regressor.coef_[0][0]
intercept=linear_regressor.intercept_[0]
legend = plt.legend(list(temp_dict_2.values()),list(temp_dict.values())+[date_today],scatterpoints=1,fontsize=11,loc=(0.05,.05),frameon=False)
totlen=len(list(temp_dict_2.values()))
print(totlen)
for m in range(totlen):
    print(m)
    legend.legendHandles[m].set_color('black')

plt.gca().add_artist(legend)
plt.legend(handles=[g_patch,kh_patch,th_patch,vn_patch,ph_patch],loc=(0.38,.92),frameon=False,ncol=5,fontsize=11)
x=[today_g]
y=[today_pr]
plt.ylim(0,100)
plt.xlim(0,7.5)
plt.vlines(x, 0, y, linestyle="dashed",color='red')
plt.hlines(y, 0, x, linestyle="dashed",color='red' )
plt.text(x[0]-.15,10, str(x[0])+ ' [kWh/m2]', ha='left', va='center', rotation=90,color='red')
plt.text(0.05,y[0]-2, str(round(y[0],1))+' [%]', ha='left', va='center',color='red')
df_sum = get_total_eac(date_today)
total_eac = (df_sum['SUM'].values[0])/1000
plt.text(x[0]/2,y[0]/2, 'EAC = '+str(round(total_eac,1))+' MWh', ha='left', va='center',color='red')
abline(slope,intercept)

#ax3.plot(df['GHI'],Y_pred,color='black',lw=2,ls='-',label='Linear Fit')
ax.set_ylabel("PR [%]")
ax.set_xlabel("GHI [kWh/m2]")
ttl = ax.set_title('From '+str(df['Date'].head(1).values[0])[0:10]+' to '+str(df_temp['Date'].tail(1).values[0])[0:10], fontdict={'fontsize': 13, 'fontweight': 'medium'})
ttl.set_position([.495, 1.02])
ttl_main=fig3.suptitle('SG-005 GHI vs PR',fontsize=15,x=.512)

ax.annotate('Equation: y='+str(round(slope,2))+'x+'+str(round(intercept,2)), (.76, .1),xytext=(4, -4),xycoords='axes fraction',textcoords='offset points',fontweight='bold',color='black',ha='left', va='bottom',size=11)

fig3.savefig("/home/admin/Graphs/Graph_Output/SG-005/[SG-005] Graph - "+date_today+" - GHI vs PR.pdf", bbox_inches='tight')
df_temp.to_csv("/home/admin/Graphs/Graph_Extract/SG-005/[SG-005] Graph - "+date_today+" - GHI vs PR.txt",sep='\t')


