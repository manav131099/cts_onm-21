source('/home/admin/CODE/Send_mail/sendmail.R')
sendCustomerInfo = function(pathWMS,pathMFM1,pathMFM2,path4G,inst,daysAl,na.rm=TRUE)
{
	pathWrite = "/home/admin/Dropbox/Customer/[MY-9002C]"
	if(!file.exists(pathWrite))
		dir.create(pathWrite)
	data4G = read.table(path4G,header=T,sep="\t",stringsAsFactors=F)
	mons = substr(as.character(data4G[,1]),1,7)
	monsuniq = unique(mons)
	idxs = match(monsuniq,mons)
	idxs[length(idxs)+1]=length(mons)
	monSend = unlist(strsplit(pathWMS,"/"))
	monSend = unlist(strsplit(monSend[length(monSend)],"WMS-"))
	dateSend=substr(monSend[2],1,10)
	monSend = substr(monSend[2],1,7)
	pathSend = paste(pathWrite,"/[MY-9002C] ",monSend,".txt",sep="")
	fileNameSend = paste("[MY-9002C] ",monSend,".txt",sep="")
	pathall = c(pathWMS,pathMFM1,pathMFM2,pathSend)
	filenam = c()
	for(x in 1 : length(pathall))
	{
		tmp = unlist(strsplit(pathall[x],"/"))
		filenam[x] = tmp[length(tmp)]
	}
	filenam[length(filenam)] = fileNameSend
	print("========================")
	print(pathall)
	print(dateSend)
	print(monSend)
	print(filenam)
	print("========================")
  body = ""
	body = paste(body,"Site Name: Denis Group, Mafipro",sep="")
	body = paste(body,"\n\nLocation: Taiping, Perak, Malaysia - 34600")
	body = paste(body,"\n\nO&M Code: MY-002")
	body = paste(body,"\n\nSystem Size:",inst)
	body = paste(body,"\n\nNumber of Energy Meters: 2")
	body = paste(body,"\n\nModule Brand / Model / Nos: JINKO / 330 / 1275")
	body = paste(body,"\n\nModule Brand / Model / Nos: SMA / Solid-Q50 / 8")
	body = paste(body,"\n\nSite COD: 2018-08-02")
	body = paste(body,"\n\nSystem age [days]:",as.character((as.numeric(daysAl))))
	body = paste(body,"\n\nSystem age [years]:",as.character(round((as.numeric(daysAl))/365,2)))
	body = paste(body,"\n\n________________________________________________\n\n")
	body = paste(body,dateSend,"\n\n",sep="")
	body = paste(body,"________________________________________________\n\n",sep="")
	for(x in 1: (length(idxs)-1))
	{
		start = idxs[x]
		end = idxs[x+1] - 1 
		if(start > end)
			end = start
		pathWriteFinal = paste(pathWrite,"/[MY-9002C] ",monsuniq[x],".txt",sep="")
		Date = Irr = Eac1 = Eac2 = LastR1 = LastR2 = LastT1 = LastT2 = Tot= Yld1=Yld2=c()
		inneridx = 1
		for(y in start:end)
		{
			Date[inneridx] = as.character(data4G[y,1])
			Irr[inneridx] = as.character(data4G[y,3])
			Eac1[inneridx] = as.character(data4G[y,10])
			Eac2[inneridx] = as.character(data4G[y,19])
			LastR1[inneridx] = as.character(data4G[y,15])
			LastR2[inneridx] = as.character(data4G[y,24])
			LastT1[inneridx] = as.character(data4G[y,16])
			LastT2[inneridx] = as.character(data4G[y,25])
			Tot[inneridx] = as.numeric(Eac1[inneridx]) + as.numeric(Eac2[inneridx])
			Yld1[inneridx]=as.character(data4G[y,12])
			Yld2[inneridx]=as.character(data4G[y,21])
			if(dateSend == Date[inneridx])
			{
				body = paste(body,"Date: ",Date[inneridx],"\n\n",sep="")
				body = paste(body,"Irradiation [kWk/m2]: ",Irr[inneridx],"\n\n",sep="")
				body = paste(body,"Eac MFM-1 [kWh]: ",Eac1[inneridx],"\n\n",sep="")
				body = paste(body,"Eac MFM-2 [kWh]: ",Eac2[inneridx],"\n\n",sep="")
				body = paste(body,"Eac Total [kWh]: ",Tot[inneridx],"\n\n",sep="")
				body = paste(body,"Last recorded meter reading MFM-1 [kWh]: ",LastR1[inneridx],"\n\n",sep="")
				body = paste(body,"Last recorded meter reading MFM-2 [kWh]: ",LastR2[inneridx],"\n\n",sep="")
				body = paste(body,"Last recorded time MFM-1: ",LastT1[inneridx],"\n\n",sep="")
				body = paste(body,"Last recorded time MFM-2: ",LastT2[inneridx],"\n\n",sep="")
				body = paste(body,"Yield MFM-1 [kWh/kWp]: ",Yld1[inneridx],"\n\n",sep="")
				body = paste(body,"Yield MFM-2 [kWh/kWp]: ",Yld2[inneridx],"\n\n",sep="")
			}
			inneridx = inneridx + 1
		}
		df = data.frame(Date=Date,Irr=Irr,EacMFM1=Eac1,EacMFM2=Eac2,LastReadMFM1=LastR1,
		LastReadMFM2=LastR2,LastTimeMFM1=LastT1,LastTimeMFM2=LastT2,TotEac=Tot,YldMFM1=Yld1,YldMFM2=Yld2,stringsAsFactors=F)
		write.table(df,file=pathWriteFinal,sep="\t",row.names=F,col.names=T,append=F)
	}
	recipientsCust = getRecipients("MY-902C","m")
	sender = c('operations@cleantechsolar.com')
	uname = 'shravan.karthik@cleantechsolar.com'
	pwd = 'CTS&*(789'
  send.mail(from = sender,
            to = recipientsCust,
            subject = paste("Client [MY-9002C] Digest",dateSend),
            body = body,
            smtp = list(host.name = "smtp.office365.com", port = 587, user.name = uname, passwd = pwd, tls = TRUE),
            authenticate = TRUE,
            send = TRUE,
            attach.files = pathall,
            file.names = filenam, # optional paramete
            debug = F)
}
