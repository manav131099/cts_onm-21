require('httr')
source('/home/admin/CODE/MasterMail/timestamp.R')
cleansedata = function(pth)
{
	df = read.table(pth,header = T,sep="\t")
  c2 = as.numeric(df[,17])
  c2idx = which(c2 %in% 0)
	c1 = as.character(df[,19])
  c2 = as.character(df[,18])
  c3 = as.character(df[,17])
  if(length(c2idx) > 1)
	{
	c1 = as.character(df[-(c2idx),19])
  c2 = as.character(df[-(c2idx),18])
  c3 = as.character(df[-(c2idx),17])
  }
	df2 = data.frame(Tm=c1,Pac=c2,Eac=c3)
	recordTimeMaster("IN-015T","Gen1",as.character(df2[nrow(df2),1]))
  return(df2)
}
pathCentral = '/home/admin/Data/TORP Data/AllStations'

probePath = function(day)
{
	path = pathCentral
	DDMMYYYY = unlist(strsplit(as.character(day),"/"))
	pathYr = paste(path,DDMMYYYY[3],sep="/")
	#checkDir(pathYr)
	pathMon = paste(pathYr,paste(DDMMYYYY[3],"-",DDMMYYYY[2],sep=""),sep="/")
	#checkDir(pathYr)
	pathFinal = paste(pathMon,paste(paste(DDMMYYYY[3],DDMMYYYY[2],DDMMYYYY[1],sep="-"),".txt",sep=""),sep="/")
	{
		if(file.exists(pathFinal))
		{
			df = try(read.table(pathFinal,header = T,sep = "\t"),silent=T)
			if(class(df)=='try-error')
				return(NULL)
			return(df)
		}
		else
			return(NULL)
	}
}

fetchrawdata = function(day1,day2)
{
  	#fetch data from SERVER USING A POST-request
		masterData = probePath(day1)
		
		if(is.null(masterData))
		{
			return(masterData)
		}
		stnnames = as.character(masterData[,13])
		subst = which(stnnames %in% "ATL Chennai")
		if(length(subst))
		{
			vals2 = masterData[subst,] # extract only rows with MJ Logistics tag in column 13
			recordTimeMaster("IN-015T","FTPNewFiles",as.character(vals2[nrow(vals2),19]))
			return(vals2)
		}
		return(NULL)
}

