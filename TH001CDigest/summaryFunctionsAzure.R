require('compiler')

checkdirAzUnOp = function(path)
{
if(!file.exists(path))
{
	dir.create(path)
}
}

checkdirAz = cmpfun(checkdirAzUnOp)

LASTRECORDEDTIME = ""
INSTCAPAZ = 1404
NOMETERAZ = 3
NOINVSAZ = 18
bucketTimeUnOp = function(x)
{
	hr = as.numeric(substr(x,12,13))
	min = as.numeric(substr(x,15,16))
	bucket = hr * 60 + min
	bucket = floor(bucket/30)+1
	return(bucket)
}

bucketTime = cmpfun(bucketTimeUnOp)

getGTIDataAzureUnOp = function(date)
{
   yr = substr(date,1,4)
	 yrmon = substr(date,1,7)
	 path = "/home/admin/Dropbox/FlexiMC_Data/Azure_Second_Gen/[TH-001C]"
	 filename = paste("[TH-001C]-WMS-",date,".txt",sep="")
	 pathRead = paste(path,yr,yrmon,"WMS",filename,sep="/")
	 GTI = unlist(rep(NA,48))
	 if(file.exists(pathRead))
	 {
	 		dataread = read.table(pathRead,sep="\t",header = T)
			if(nrow(dataread) > 0)
			{
				GTI = as.numeric(dataread[,2])
			}
	 }
	 return(GTI)
}

getGTIDataAzure = cmpfun(getGTIDataAzureUnOp)

pruneDataUnOp = function(fourthGenData,path)
{
	timestamps = as.character(fourthGenData[,1])
	idxmtch = match(LASTRECORDEDTIME,timestamps)
	end = nrow(fourthGenData)
	if(is.finite(idxmtch))
	{
		print(paste("Match found for",LASTRECORDEDTIME,"at",idxmtch))
		end = idxmtch
	}
	start = end - 1000
	if(start > 0 && end > 0)
	{
		latestData = fourthGenData[start:end,]
		write.table(latestData,file=path,sep="\t",row.names =F,col.names =T,append=F)
	}
}

pruneData = cmpfun(pruneDataUnOp)

secondGenDataAzureUnOp = function(pathread, pathwrite,type)
{
	dataread = try(read.table(pathread,sep="\t",header = T),silent = T)
	if(class(dataread) == "try-error")
	{
		print(paste('pathread error',pathread))
		date=DA=Eac1=Eac3=Eac2=LT=LR=Yld1=Yld2=PR1=PR2=Tamb=Tmod=TModSH=TambSH=GTI=NA
		{
		if(type == 0)
			data = data.frame(Date = date,Eac1 = Eac1, Eac2 = Eac2, Yld1 = Yld1,
			Yld2 = Yld2, PR1= PR1, PR2=PR2, LastRead = LR, LastTime=LT)
		else if(type == 1)
			data = data.frame(Date = date,Eac1 = Eac1, EacAC = Eac2, EacDC = Eac3,
			Yld1 = Yld1, Yld2 = Yld2, LastRead = LR,LastTime = LT)
		else if(type == 2)
			data = data.frame(Date = date,GTI = GTI, Tamb = Tamb, Tmod = Tmod,TambSH=TambSH,TModSH=TModSH)
		}
		write.table(data,file=pathwrite,row.names =F,col.names = T,sep="\t",append=F)
		return()	
	}
	buckets = bucketTime(as.character(dataread[,1]))
	datareadOrig = dataread
	date = substr(dataread[1,1],1,10)
	if(is.na(date) || grepl("NA",date))
	{
		return()
	}
	{
		if(type == 0)
		{
			idxPower = 32
			idxEnergy = 65
			DATE = EAC1 = EAC2 = YLD1 = YLD2 = PR1 = PR2 = LT = LR  = c()
			for( x in 1 : 48)
			{
				Eac1 = Eac2  = 0
				lt = lr = NA
				hr = floor(x/2)
				if(x < 20)
				{
					hr = paste("0",hr,sep="")
				}
				min = "00"
				if(x %% 2)
					min = "30"
				if(hr == 24)
				{
					hr = 23
					min = 59
				}
				tmstmp = paste(hr,min,"00",sep=":")
				tmstmp = paste(date,tmstmp)

				dataInt = as.numeric(dataread[complete.cases(as.numeric(dataread[,idxEnergy])),idxEnergy])
				dataIntTr = round(dataInt/1000000,0)
				dataIntTr2 = unique(dataIntTr)
				mode = dataIntTr2[which.max(tabulate(match(dataIntTr, dataIntTr2)))]
				
				idxs = which(buckets %in% x)
				idxs = idxs[complete.cases(idxs)]
				datareadOrig2 = datareadOrig[complete.cases(as.numeric(datareadOrig[,idxEnergy])),idxEnergy]
				if(length(idxs))
				{
				LASTRECORDEDTIME <<- tmstmp
				dataread = datareadOrig[idxs,]
				dataInt2 = as.character(dataread[complete.cases(as.numeric(dataread[,idxEnergy])),1])
				
				dataInt = dataInt[abs(dataIntTr - mode) <= 100]
				dataInt2 = dataInt2[abs(dataIntTr - mode) <= 100]
				
				if(length(dataInt))
				{
					Eac2 = round((abs(dataInt[length(dataInt)] - datareadOrig2[1])/1000),2)
					lr = round(as.numeric(dataInt[length(dataInt)])/1000,2)
					lt = as.character(dataInt2[length(dataInt2)])
				}
				dataInt = as.numeric(dataread[complete.cases(as.numeric(dataread[,idxPower])),idxPower])
				dataInt = dataInt[abs(dataInt) < 1000000]
				if(length(dataInt))
					Eac1 = as.numeric(format(round((sum(dataInt)/12),2),nsmall=2))
				}
				DATE[x] = tmstmp
				EAC1[x] = Eac1
				EAC2[x] = Eac2
				YLD1[x] = round(as.numeric(Eac1)/INSTCAPAZ,2)
				YLD2[x] = round(as.numeric(Eac2)/INSTCAPAZ,2)
				LT[x] = lt
				LR[x] = lr
			}
				GTI = getGTIDataAzure(substr(DATE[1],1,10))
				PR1 = round(YLD1*100/GTI,1)
				PR2 = round(YLD2*100/GTI,1)
			data = data.frame(Date = DATE,Eac1 = EAC1, Eac2 = EAC2, Yld1= YLD1, Yld2 = YLD2, PR1 = PR1, PR2= PR2, LastRead = LR, LastTime=LT)
		}
		else if(type == 1)
		{
			idxEnergy = 33
			idxPowerAC = 15
			idxPowerDC = 21
			DATE = EAC1 = EAC2 = EAC3 = YLD1 = YLD2 = LR = LT = c()
			for( x in 1 : 48)
			{
				hr = floor(x/2)
				if(x < 20)
				{
					hr = paste("0",hr,sep="")
				}
				min = "00"
				if(x %% 2)
					min = "30"
				if(hr == 24)
				{
					hr = 23
					min = 59
				}
				tmstmp = paste(hr,min,"00",sep=":")
				tmstmp = paste(date,tmstmp)
				Eac1 = Eac2 = Eac3 = 0
				lt = lr = NA
			
				idxs = which(buckets %in% x)
				idxs = idxs[complete.cases(idxs)]
				datareadOrig2 = datareadOrig[complete.cases(as.numeric(datareadOrig[,idxEnergy])),idxEnergy]
				if(length(idxs))
				{
				dataread = datareadOrig[idxs,]
				dataInt = as.numeric(dataread[complete.cases(as.numeric(dataread[,idxEnergy])),idxEnergy])
				dataInt2 = as.character(dataread[complete.cases(as.numeric(dataread[,idxEnergy])),1])
				
				dataIntTr = round(dataInt/1000000,0)
				dataIntTr2 = unique(dataIntTr)
				mode = dataIntTr2[which.max(tabulate(match(dataIntTr, dataIntTr2)))]
				dataInt = dataInt[abs(dataIntTr - mode) <= 1]
				dataInt2 = dataInt2[abs(dataIntTr - mode) <= 1]
				
				if(length(dataInt))
				{
					Eac2 = round((abs(dataInt[length(dataInt)] - datareadOrig2[1])/1000),2)
					lr = round(dataInt[length(dataInt)]/1000,2)
					lt = dataInt2[length(dataInt2)]
				}
				dataInt = as.numeric(dataread[complete.cases(as.numeric(dataread[,idxPowerAC])),idxPowerAC])
				dataInt = dataInt[abs(dataInt) < 1000000]
				if(length(dataInt))
					Eac1 = as.numeric(format(round((sum(dataInt)/12000),2),nsmall=2))
			
				dataInt = as.numeric(dataread[complete.cases(as.numeric(dataread[,idxPowerDC])),idxPowerDC])
				dataInt = dataInt[abs(dataInt) < 1000000]
				if(length(dataInt))
					Eac3 = as.numeric(format(round((sum(dataInt)/12000),2),nsmall=2))
				}
				DATE[x] = tmstmp
				EAC1[x] = Eac1
				EAC2[x] = Eac2
				EAC3[x] = Eac3
				YLD1[x] = as.numeric(EAC1[x])*NOINVSAZ/INSTCAPAZ
				YLD2[x] = as.numeric(EAC2[x])*NOINVSAZ/INSTCAPAZ
				LR[x] = lr
				LT[x] = lt
			}
			data = data.frame(Date = DATE,Eac1 = EAC1, EacAC = EAC2, EacDC = EAC3,
			Yld1 = YLD1, Yld2 = YLD2,LastRead = LR,LastTime = LT)
		}
		else if(type == 2)
		{
			idxGTI = 21
			idxTMod = 10
			idxTamb = 13
			DATE = TAMB = TMOD = GTIG = noEntry = TMODSH = TAMBSH = c()
			for( x in 1 : 48)
			{
				hr = floor(x/2)
				if(x < 20)
				{
					hr = paste("0",hr,sep="")
				}
				min = "00"
				if(x %% 2)
					min = "30"
				if(hr == 24)
				{
					hr = 23
					min = 59
				}
				tmstmp = paste(hr,min,"00",sep=":")
				tmstmp = paste(date,tmstmp)
				Tamb = Tmod = GTI = TModSH = TambSH = 0
			
				idxs = which(buckets %in% x)
				idxs = idxs[complete.cases(idxs)]
				noEntry[x] = length(idxs)
				if(length(idxs))
				{
					dataread = datareadOrig[idxs,]
					dataInt = as.numeric(dataread[complete.cases(as.numeric(dataread[,idxTMod])),idxTMod])
					if(length(dataInt))
						Tmod = round(mean(dataInt),1)
					if(hr > 6 && hr < 20)
						TModSH = Tmod
			
					dataInt = as.numeric(dataread[complete.cases(as.numeric(dataread[,idxTamb])),idxTamb])
					if(length(dataInt))
						Tamb = round(mean(dataInt),1)
					if(hr > 6 && hr < 20)
						TambSH = Tamb

					dataInt = as.numeric(dataread[complete.cases(as.numeric(dataread[,idxGTI])),idxGTI])
					if(length(dataInt))
						GTI = as.numeric(format(round((sum(dataInt)/12000),2),nsmall=2))
					T
				}
				DATE[x] = tmstmp
				TMOD[x] = Tmod
				TAMB[x] = Tamb
				GTIG[x] = GTI
				TMODSH[x] = TModSH
				TAMBSH[x] = TambSH
				if(x > 1)
				{
					TMOD[x] = round(((Tmod * noEntry[x]) + (TMOD[x-1] * noEntry[x-1])) / (noEntry[x-1] + noEntry[x]),1)
					TAMB[x] = round(((Tamb * noEntry[x]) + (TAMB[x-1] * noEntry[x-1])) / (noEntry[x-1] + noEntry[x]),1)
					GTIG[x] = GTIG[x-1] + GTI
					TMODSH[x] = TMOD[x]
					TAMBSH[x] = TAMB[x]
					noEntry[x] = noEntry[x] + noEntry[x-1]
				}
			}
			data = data.frame(Date = DATE,GTI = GTIG, Tamb = TAMB, Tmod = TMOD,TambSH=TAMBSH,TmodSH=TMODSH)
		}
	}
	write.table(data,file=pathwrite,row.names =F,col.names = T,sep="\t",append=F)
	fourthGenDataAzure(pathwrite,data)
}

secondGenDataAzure = cmpfun(secondGenDataAzureUnOp)

fourthGenDataAzureUnOp = function(path2G,dataread)
{
	order = c("WMS","Solar_Invoice","Solar_Check","Load_MFM","Inverter_1",
	"Inverter_2","Inverter_3","Inverter_4","Inverter_5",
	"Inverter_6","Inverter_7","Inverter_8","Inverter_9","Inverter_10","Inverter_11",
	"Inverter_12","Inverter_13","Inverter_14","Inverter_15","Inverter_16","Inverter_17","Inverter_18")
	CONSTANTLEN = c(5,unlist(rep(8,NOMETERAZ)),unlist(rep(7,NOINVSAZ))) # No of columns for each file excluding date
	
	stnnames = unlist(strsplit(path2G,"/"))
	template = matrix(NA,nrow=48,ncol=(1+sum(CONSTANTLEN)))
	template = data.frame(template)
	path4G = paste("/home/admin/Dropbox/FlexiMC_Data/Azure_Fourth_Gen",stnnames[7],sep="/")
	checkdirAz(path4G)
	pathfinal = paste(path4G,paste(stnnames[7],"-lifetime.txt",sep=""),sep="/")
	pathprune = paste(path4G,paste(stnnames[7],"-latest.txt",sep=""),sep="/")
	idxmtch = match(stnnames[10],order)
	start = 2
	if(idxmtch > 1)
	{
		start = 2 + sum(CONSTANTLEN[1:(idxmtch-1)])
	}
	end = start -1 + CONSTANTLEN[idxmtch]

	idxuse = start : end
	colnames2G = colnames(dataread)
	colnames2G = colnames2G[-1]
	colnames2G = paste(stnnames[10],colnames2G,sep="-")
	{
		if(!file.exists(pathfinal))
		{
			colnames = rep("Date",ncol(template))
			colnames[idxuse] = colnames2G
			template[,c(1,idxuse)] = dataread
			dataExist = data.frame(template)
			colnames(dataExist) = colnames
			write.table(dataExist,file=pathfinal,row.names =F,col.names=T,sep="\t",append=F)
			data4G = dataExist
		}
		else
		{
			dataExist = read.table(pathfinal,sep="\t",header=T)
			data4G = dataExist
			colnames = colnames(dataExist)
			colnames[idxuse] = colnames2G
			dates = as.character(dataExist[,1])
			idxmtchdt = match(as.character(dataread[,1]),dates)
			if(is.finite(idxmtchdt[1]))
			{
				tmp = dataExist[idxmtchdt,]
				tmp[,idxuse] = dataread[,(2:ncol(dataread))]
				dataExist[idxmtchdt,] = tmp
				colnames(dataExist) = colnames
				write.table(dataExist,file=pathfinal,row.names =F,col.names=T,sep="\t",append=F)
			}
			else
			{
				template[,c(1,idxuse)] = dataread
				dataExist = data.frame(template)
				colnames(dataExist) = colnames
				write.table(dataExist,file=pathfinal,row.names =F,col.names=F,sep="\t",append=T)
			}
		}
	}
	pruneData(data4G,pathprune)
}

fourthGenDataAzure = cmpfun(fourthGenDataAzureUnOp)
