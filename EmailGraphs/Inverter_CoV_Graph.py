import requests, json
import requests.auth
import pandas as pd
import datetime
import os
import re 
import time
import shutil
import math
import pytz
import sys
import pyodbc
import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.patches as mpatch
import matplotlib.dates as mdates
from scipy import stats

def chkdir(path):
    a=os.path.isdir(path)
    if(a):
        return
    else:
        os.makedirs(path)

stn=sys.argv[1]
end_date=sys.argv[2]
threshold=float(sys.argv[3])
start=sys.argv[4]
excluded=sys.argv[5].split(',')
name=sys.argv[6]

if(stn[-1].isdigit()):
    print('in')
    path='/home/admin/Dropbox/Fourth_Gen/['+stn+'L]/['+stn+'L]-lifetime.txt'
else:
    path='/home/admin/Dropbox/Fourth_Gen/['+stn+']/['+stn+']-lifetime.txt'
    stn=stn[:-1]

path_write='/home/admin/Graphs/'

df=pd.read_csv(path,sep='\t')
temp=[]
cols=df.columns
flag=0

for i in cols:
    if(i[-4:]=='Yld2' and i[0:3]!='MFM'):
        if(str(i.split('_')[1][:-5]) in excluded):
            pass
        else:
            temp.append(i)


df_Yld=df[['Date']+temp].copy()
df_Yld['COV']=((df_Yld.std(axis=1,skipna=True,ddof=0)*100)/df_Yld.mean(axis=1,skipna=True))
df=df_Yld
df=df[df['Date']>start]
no_points=len(df)
df=df[df['Date']<=end_date]
df=df.replace(0, np.nan)
df = df.drop_duplicates(df.columns[1:])
last_7=df.tail(7)
last_30=df.tail(30)
last_60=df.tail(60).copy()
avg_std_7=last_7['COV'].mean()
avg_std_30=last_30['COV'].mean()
avg_std_60=last_60['COV'].mean()
avg_std_lifetime=df['COV'].mean()
plt.rcParams.update({'font.size': 28})
plt.rcParams['axes.facecolor'] = 'gainsboro'
fig = plt.figure(num=None, figsize=(55  , 30))
ax = fig.add_subplot(111)
last_date=last_60['Date'].tail(1).values[0]
first_date=last_60['Date'].head(1).values[0]
dates=last_60['Date'].values.tolist()
df_outlier=last_60[(np.abs(stats.zscore(last_60['COV'].fillna(0)))> 3)]
outlier_dates=df_outlier['Date'].values.tolist()
outlier_values=df_outlier['COV'].values.tolist()
last_60['Date']=pd.to_datetime(last_60['Date'])
last_60.index=last_60['Date']
upsampled=last_60.resample('2H')
try:
    interpolated = upsampled.interpolate(method='cubic')
    interpolated = upsampled.interpolate(method='polynomial',order=1)
    plt.plot(interpolated['COV'],color='black',linewidth=3)
except:
    plt.plot(last_60['COV'],color='black',linewidth=3)
cov_values=last_60['COV'].values.tolist()
#plt.plot(last_60['Date'].tolist(),last_60['COV'].tolist(),color='black',linewidth=3)
plt.xticks(last_60['Date'].tolist(),rotation=90)
plt.xlim([str(datetime.datetime.strptime(first_date,"%Y-%m-%d").date()+datetime.timedelta(days=-1)),last_date])

if(len(outlier_values)>0 and not math.isnan(outlier_values[0])):
    if(max(outlier_values)<100):
        plt.ylim([0,max(outlier_values)+5])
    else:
        plt.ylim([0,50])
else:
    if(max(cov_values)<100):
        if(abs(threshold-max(cov_values)<2)):
            plt.ylim([0,max(cov_values)+10])
        else:
            plt.ylim([0,max(cov_values)+5])
    else:
        plt.ylim([0,50])
myFmt = mdates.DateFormatter('%Y-%m-%d')
ax.xaxis.set_major_formatter(myFmt)
ttl = ax.set_title('From '+str(first_date)+' to '+str(last_date), fontdict={'fontsize': 45, 'fontweight': 'medium'})
ttl.set_position([.485, 1.02])
ttl_main=fig.suptitle(stn+' Daily '+name,fontsize=60)
ax.annotate('No. of Points: '+str(no_points)+' days\nLast 7-day CoV average [%]: '+str(round(avg_std_7,1))+'\nLast 30-day CoV average [%]: '+str(round(avg_std_30,1))+'\nLast 60-day CoV average [%]: '+str(round(avg_std_60,1))+'\nLifetime CoV average [%]: '+str(round(avg_std_lifetime,1)), (.01, .98),xytext=(4, -4),xycoords='axes fraction',textcoords='offset points',fontweight='bold',color='white',backgroundcolor='k',ha='left', va='top',size=25)
plt.axhline(y=threshold, color='green',linewidth=8)
if(stn=='VN-002'):
    plt.axhline(y=(threshold+2), color='orange',linewidth=8)
else:
    plt.axhline(y=(threshold+5), color='orange',linewidth=8)
ax.set_ylabel('CoV [%]', fontsize=32)
ln1=None
ln2=None
ln3=None
for index,i in enumerate(cov_values):
    if(i<=(threshold)):
        ln1=plt.scatter(dates[index], i, marker='D',color='green',s=300)
    elif(i>(threshold) and i<(threshold+5)):
        ln2=plt.scatter(dates[index], i, marker='s',color='orange',s=300)
    elif(i>=(threshold+5)):
        ln3=plt.scatter(dates[index], i, marker='^',color='red',s=300)
if(ln1!=None and ln2!=None and ln3!=None):
    plt.legend((ln1, ln2, ln3),
            ('Cov [%] <= '+str(threshold), 'Cov [%] > '+str(threshold)+' and CoV [%] < '+str(threshold+5), 'Cov [%] >= '+str(threshold+5)),
            scatterpoints=1,
            loc='upper right',
            ncol=3,
            fontsize=30)
elif(ln1!=None and ln2!=None):
    plt.legend((ln1, ln2),
            ('Cov [%] <= '+str(threshold), 'Cov [%] > '+str(threshold)+' and CoV [%] < '+str(threshold+5)),
            scatterpoints=1,
            loc='upper right',
            ncol=3,
            fontsize=30)
elif(ln1!=None and ln3!=None):
    plt.legend((ln1, ln3),
            ('Cov [%] <= '+str(threshold), 'Cov [%] >= '+str(threshold+5)),
            scatterpoints=1,
            loc='upper right',
            ncol=3,
            fontsize=30)
elif(ln1!=None):
    plt.legend(([ln1]),
            (['Cov [%] <= '+str(threshold)]),
            scatterpoints=1,
            loc='upper right',
            ncol=3,
            fontsize=30)
for i, label in enumerate(outlier_dates):
    if(outlier_values[i]>50):
        pass
    else:
        ax.annotate(label, (label, outlier_values[i]+.25),size=25,color='red')
chkdir(path_write+"Graph_Output/"+stn)
chkdir(path_write+"Graph_Extract/"+stn)
fig.savefig(path_write+"Graph_Output/"+stn+'/['+stn+"] Graph "+sys.argv[2]+" - "+name+".pdf",bbox_inches='tight')
last_60.to_csv(path_write+"Graph_Extract/"+stn+'/['+stn+"] Graph "+sys.argv[2]+" - "+name+".txt",sep='\t',index=False)