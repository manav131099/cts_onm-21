system('rm -R "/home/admin/Dropbox/Second Gen/[IN-010X]"')
system('rm -R "/home/admin/Dropbox/Third Gen/[IN-010X]"')

require('mailR')
require('Hmisc')

source('/home/admin/CODE/IN010Digest/DelhiMailDigestFunctions.R')
source('/home/admin/CODE/Misc/calcBackLog.R')
path = '/home/admin/Dropbox/Gen 1 Data/[IN-010X]'
writepath2G = '/home/admin/Dropbox/Second Gen/[IN-010X]'
checkdir(writepath2G)

writepath3G = '/home/admin/Dropbox/Third Gen/[IN-010X]'
checkdir(writepath3G)
DAYSALIVE = 0
LAST30DAYSEAC1 = vector('numeric',30)
LAST30DAYSEAC2 = vector('numeric',30)
MONTHTOTEAC1 = 0
MONTHTOTEAC2 = 0
DAYSTHISMONTH = 0
DAYSTHISMONTHAC = 0
EAC1GLOB = 0
EAC2GLOB = 0
idxvec = 1
years = dir(path)
stnnickName2 = "IN-010X"
lastdatemail = lastMailDate(paste('/home/admin/Start/MasterMail/',stnnickName2,'_Mail.txt',sep=""))
stopDate = paste("[",stnnickName2,"] ",lastdatemail,".txt",sep="")
ENDCALL=0
for(x in 1 : length(years))
{
  pathyrs = paste(path,years[x],sep="/")
  writepath2Gyr = paste(writepath2G,years[x],sep="/")
  checkdir(writepath2Gyr)
  writepath3Gyr = paste(writepath3G,years[x],sep="/")
  checkdir(writepath3Gyr)
  months = dir(pathyrs)
  for(y in 1 : length(months))
  {
    MONTHTOTEAC1 = 0
    MONTHTOTEAC2 = 0
    DAYSTHISMONTH = 0
    pathdays= paste(pathyrs,months[y],sep="/")
    writepath2Gdays = paste(writepath2Gyr,months[y],sep="/")
    checkdir(writepath2Gdays)
    writepath3Gfinal = paste(writepath3Gyr,"/[IN-010X] ",months[y],".txt",sep="")
    days = dir(pathdays)
    temp = unlist(strsplit(days[1]," "))
    temp = as.Date(substr(temp[2],1,10),"%Y-%m-%d")
    DAYSTHISMONTHAC = monthDays(temp)
		#printMontlyReport(years,x,months,y,"[IN-010]",path,"NoMeter",writepath3Gyr,2)
      for(t in 1 : length(days))
      {
       	if(ENDCALL==1)
					break
				{
          if(x == 1 && y == 1 && t == 1)
          {
            DOB = unlist(strsplit(days[t]," "))
            DOB = substr(DOB[2],1,10)
            DOB = as.Date(DOB,"%Y-%m-%d")
          }
          else if(x == length(years) && y == length(months) && t == length(days))
          {
            next
          }
        }
        DAYSALIVE = DAYSALIVE + 1
        writepath2Gfinal = paste(writepath2Gdays,"/",days[t],sep="")
        readpath = paste(pathdays,days[t],sep="/")
        df1 = secondGenData(readpath,writepath2Gfinal)
        tots = thirdGenData(writepath2Gfinal,writepath3Gfinal)
				if(!is.finite(tots[1])) 
				{
					tots[1]=0
				}
				if(!is.finite(tots[2]))
				{
					tots[2]=0
				}
        LAST30DAYSEAC1[[idxvec]] = tots[1]
        LAST30DAYSEAC2[[idxvec]] = tots[2]
        MONTHTOTEAC1 = MONTHTOTEAC1 + tots[1]
        MONTHTOTEAC2 = MONTHTOTEAC2 + tots[2]
        DAYSTHISMONTH = DAYSTHISMONTH + 1
        EAC1GLOB = EAC1GLOB + tots[1]
        EAC2GLOB = EAC2GLOB + tots[2]
        idxvec = (idxvec + 1) %% 30
        if(idxvec == 0)
        {
          idxvec = 1
        }
				if(days[t] == stopDate)
				{
					ENDCALL = 1
				}
      }
			if(ENDCALL==1)
			break
    }
			if(ENDCALL==1)
			break
}
print('Historical Analysis Done')
