rm(list=ls())
errHandle = file('/home/admin/Logs/LogsIN036CMail.txt',open='w',encoding='UTF-8')
sink(errHandle,type='message',append = T)
sink(errHandle,type='output',append = T)

source('/home/admin/CODE/IN036CDigest/summaryFunctions.R')
source('/home/admin/CODE/Send_mail/sendmail.R')
source('/home/admin/CODE/common/math.R')
RESETHISTORICAL=0
daysAlive = 0
reorderStnPaths = c(54,2,6,3,4,7,18,29,40,49,50,51,52,53,8,9,10,11,12,13,14,15,
16,17,19,20,21,22,23,24,25,26,27,28,30,31,32,33,34,35,36,37,38,39,41,42,43,44,
45,46,47,48)

source('/home/admin/CODE/MasterMail/timestamp.R')
require('mailR')
source('/home/admin/CODE/IN036CDigest/aggregateInfo.R')

METERNICKNAMES = c("WMS","C-MFM","S-MFM","C-INV-1","C-INV-2","S-INV-1",
"S-INV-2","S-INV-3","S-INV-4","S-INV-5","S-INV-6","S-INV-7","S-INV-8",
"S-INV-9","S-INV-10","S-INV-11","S-INV-12","S-INV-13","S-INV-14","S-INV-15",
"S-INV-16","S-INV-17","S-INV-18","S-INV-19","S-INV20",
"S-INV-21","S-INV-22","S-INV-23","S-INV-24","S-INV-25","S-INV-26",
"S-INV-27","S-INV-28","S-INV-29","S-INV-30","S-INV-31","S-INV-32","S-INV-33",
"S-INV-34","S-INV-35","S-INV-36","S-INV-37","S-INV-38","S-INV-39","S-INV-40",
"S-INV-41","S-INV-42","S-INV-43","S-INV-44","S-INV-45","S-INV-46","S-INV-47")

METERACNAMES = c("WMS","CI_MFM","SI_MFM","C_Inverter_1","C_Inverter_2","S_Inverter_1",
"S_Inverter_2","S_Inverter_3","S_Inverter_4","S_Inverter_5","S_Inverter_6",
"S_Inverter_7","S_Inverter_8","S_Inverter_9","S_Inverter_10","S_Inverter_11",
"S_Inverter_12","S_Inverter_13","S_Inverter_14","S_Inverter_15","S_Inverter_16",
"S_Inverter_17","S_Inverter_18","S_Inverter_19","S_Inverter_20","S_Inverter_21",
"S_Inverter_22","S_Inverter_23","S_Inverter_24","S_Inverter_25","S_Inverter_26",
"S_Inverter_27","S_Inverter_28","S_Inverter_29","S_Inverter_30","S_Inverter_31",
"S_Inverter_32","S_Inverter_33","S_Inverter_34","S_Inverter_35","S_Inverter_36",
"S_Inverter_37","S_Inverter_38","S_Inverter_39","S_Inverter_40","S_Inverter_41",
"S_Inverter_42","S_Inverter_43","S_Inverter_44","S_Inverter_45","S_Inverter_46","S_Inverter_47")

checkdir = function(x)
{
	if(!file.exists(x))
	{
		dir.create(x)
	}
}

DOB = "27-12-2017"
sender = c('operations@cleantechsolar.com')
uname = 'shravan.karthik@cleantechsolar.com'
recipients = getRecipients("IN-036C","m")
pwd = 'CTS&*(789'
referenceDays = NA
extractDaysOnly = function(days)
{
	if(length(days!=0))
	{
	seq = seq(from=1,to=length(days)*6,by=6)
	days = unlist(strsplit(days,"-"))
	days = paste(days[seq+3],days[seq+4],days[seq+5],sep="-")
	return(days)
	}
}
analyseDays = function(days,ref)
{
	if(length(days) == length(ref))
		return(days)
	daysret = unlist(rep(NA,length(ref)))
	if(length(days) == 0)
		return(daysret)
	days2 = extractDaysOnly(days)
	idxmtch = match(days2,ref)
	idxmtch2 = idxmtch[complete.cases(idxmtch)]
	if(length(idxmtch) != length(idxmtch2))
	{
		print(".................Missmatch..................")
		print(days2)
		print("#########")
		print(ref)
		print("............................................")
		return(days)
	}
	daysret[idxmtch] = as.character(days)
	if(!(is.na(daysret[length(daysret)])))
		return(daysret)
	else
		return(days)
}
performBackWalkCheck = function(day)
{
	path = "/home/admin/Dropbox/FlexiMC_Data/Gen1_Data/[IN-036C]"
	retval = 0
	yr = substr(day,1,4)
	yrmon = substr(day,1,7)
	pathprobe = paste(path,yr,yrmon,sep="/")
	stns = dir(pathprobe)
	print(stns)
	idxday = c()
	for(t in 1 : length(stns))
	{
		pathdays = paste(pathprobe,stns[t],sep="/")
		print(pathdays)
		days = dir(pathdays)
		dayMtch = days[grepl(day,days)]
		if(length(dayMtch))
		{
			idxday[t] = match(dayMtch,days)
		}
		print(paste("Match for",day,"is",idxday[t]))
	}
	idxmtch = match(NA,idxday[t])
	if(length(unique(idxday))==1 || length(idxmtch) < 5)
	{
		print('All days present ')
		retval = 1
	}
	return(retval)
}
sendMail= function(pathall)
{
  
  Cur=Sys.Date()
  filenams = c()
  body = ""
	body = paste(body,"Site Name: BESCOM",sep="")
	body = paste(body,"\n\nLocation: Hubli, Karnataka, India")
	body = paste(body,"\n\nO&M Code: IN-036")
	body = paste(body,"\n\nSystem Size:",sum(INSTCAPM))
	body = paste(body,"\n\nNumber of Energy Meters: 2")
	body = paste(body,"\n\nModule Brand / Model / Nos: JA Solar / 325Wp / 13880")
	body = paste(body,"\n\nInverter Brand / Model / Nos: Huawei / SUN2000_43KTL / 47")
	body = paste(body,"\n\nInverter Brand / Model / Nos: ABB / PVS800-57-1000KW-c / 2")
	body = paste(body,"\n\nSite COD: 2017-12-27")
	body = paste(body,"\n\nSystem age [days]:",as.character((as.numeric(daysAlive)+35)))
	body = paste(body,"\n\nSystem age [years]:",as.character(round((as.numeric(daysAlive)+35)/365,2)))
	bodyac = body
	body = ""
	TOTALGENCALC = 0
	yr = substr(Cur,1,4)
  yrmon = substr(Cur,1,7)
  
  path1 = "/home/admin/Dropbox/Second Gen/[IN-036S]"
  path2 = "/home/admin/Dropbox/FlexiMC_Data/Second_Gen/[IN-036C]"
  filename1 = paste("[IN-036S] ",Cur,".txt",sep="")
  filename2 = paste("[IN-036C]-SI_MFM-",Cur,".txt",sep="")
  
  pathRead2 = paste(path1,yr,yrmon,filename1,sep="/")
  pathRead3 = paste(path2,yr,yrmon,"SI_MFM",filename2,sep="/")
  print("Printing paths")
  print(pathRead2)
  print(pathRead3)
  GTIGreater20=NA
  GTI = DNI = NA
  if(file.exists(pathRead3))
  {
    dataread3 = read.table(pathRead3, sep = "\t", header = T)
    if(nrow(dataread3) > 0)
    {
      yield1 = as.numeric(dataread3[1,5])
      yield2 = as.numeric(dataread3[1,6])
      if(file.exists(pathRead2))
      {
        dataread2 = read.table(pathRead2, sep = "\t", header = T)
        GTI2 = as.numeric(dataread2[1,30])
        PR1 = round((yield1*100)/GTI2,1)
        PR2 = round((yield2*100)/GTI2,1)
      }else{
        GTI2 = 0
        PR1 = 0
        PR2 = 0
      }
      body = paste(body,"\n\n_____________________________________________\n",sep="")
      body = paste(body,"WMS")
      body  = paste(body,"\n_____________________________________________\n",sep="")
      body = paste(body,"\nIrradiation (From IN-036S): ",GTI2)
      body = paste(body,"\n\nPR-1 [%]: ",PR1)
      body = paste(body,"\n\nPR-2 [%]: ",PR2)
      
      
    }
  }
	MYLD = c()
	
	acname = METERNICKNAMES
	noInverters = NOINVS
	InvReadings = unlist(rep(NA,noInverters))
	InvAvail = unlist(rep(NA,noInverters))
	globMtYld = globMtVal = c()
	idxglobMtYld = 1
	GTI = NA
	for(t in 1 : length(pathall))
  {
	type = 0
	meteridx = t
	if(grepl("MFM",pathall[t]) || grepl("WMS",pathall[t]))
		type = 1
	if( type == 0)
	{
		splitpath = unlist(strsplit(pathall[t],"Inverter_"))
		offset = 0
		if(grepl("S_Inv",pathall[t]))
			offset = 2
		if(length(splitpath)>1)
		{
			meteridx = unlist(strsplit(splitpath[2],"/"))
			meteridx = as.numeric(meteridx[1]) + offset 
		}
	}
	path = pathall[t]
  dataread = read.table(path,header = T,sep="\t")
	currday = as.character(dataread[1,1])
	offsetnam = 0
	if(type == 0)
	  offsetnam = 1+NOMETERS
	filenams[t] = paste(currday,"-",METERNICKNAMES[meteridx + offsetnam],".txt",sep="")
	print("---------------------")
	print(path)
	print(filenams[t])
	print("---------------------")
	if(type == 1)
	{
	body = paste(body,"\n\n________________________________________________\n\n")
	body = paste(body,currday,acname[t])
	body = paste(body,"\n\n________________________________________________\n\n")
	body = paste(body,"DA [%]:",as.character(dataread[1,2]),"\n\n")
	{
	if(grepl("MFM",pathall[t]))
	{
	body = paste(body,"EAC method-1 (Pac) [kWh]:",as.character(dataread[1,3]),"\n\n")
	body = paste(body,"EAC method-2 (Eac) [kWh]:",as.character(dataread[1,4]),"\n\n")
	TOTALGENCALC = TOTALGENCALC + as.numeric(dataread[1,4])
	body = paste(body,"Yield-1 [kWh/kWp]:",as.character(dataread[1,5]),"\n\n")
	body = paste(body,"Yield-2 [kWh/kWp]:",as.character(dataread[1,6]),"\n\n")
	globMtYld[idxglobMtYld] = as.numeric(dataread[1,6])
	globMtVal[idxglobMtYld] = as.numeric(dataread[1,4])
	idxglobMtYld = idxglobMtYld + 1
	body = paste(body,"PR-1 (using IN-036C) [%]:",as.character(dataread[1,7]),"\n\n")
	body = paste(body,"PR-2 (using IN-036C) [%]:",as.character(dataread[1,8]),"\n\n")
	body = paste(body,"Last recorded value [kWh]:",as.character(dataread[1,9]),"\n\n")
	body = paste(body,"Last recorded time:",as.character(dataread[1,10]),"\n\n")
	body = paste(body,"Grid Availability [%]:",as.character(dataread[1,13]),"\n\n")
	body = paste(body,"Plant Availability [%]:",as.character(dataread[1,14]))
	}
	else
	{
		body = paste(body,"GTI (from IN-036C) [kWh/m^2]:",as.character(dataread[1,3]),"\n\n")
		GTI = as.numeric(dataread[1,3])
		body = paste(body,"Tamb [C]:",as.character(dataread[1,4]),"\n\n")
		body = paste(body,"Tmod [C]:",as.character(dataread[1,5]),"\n\n")
		body = paste(body,"Tamb solar hours [C]:",as.character(dataread[1,6]),"\n\n")
		body = paste(body,"Tmod solar hours[C]:",as.character(dataread[1,7]),"\n\n")
		body = paste(body,"Max wind speed [m/s]:",as.character(dataread[1,8]))
	}
	}
	next
  }
	InvReadings[meteridx] = as.numeric(dataread[1,7])
    InvAvail[meteridx]= as.numeric(dataread[1,10])
	MYLD[(meteridx-2)] = as.numeric(dataread[1,7])
	}
	body = paste(body,"\n\n________________________________________________\n\n")
	body = paste(body,currday,"Inverters")
	body = paste(body,"\n\n________________________________________________")
	MYLDCPY = MYLD
	MYLD = MYLD[complete.cases(MYLD)]
	addwarn = 0
	if(length(MYLD))
	{
		addwarn = 1
		sddev = round(sdp(MYLD),2)
		meanyld = mean(MYLD)
		covar = round((sdp(MYLD)*100/meanyld),1)
	}
	MYLD = MYLDCPY
	for(t in 1 : noInverters)
	{
		body = paste(body,"\n\n Yield ",acname[(t+1+NOMETERS)],": ",as.character(InvReadings[t]),sep="")
		if( addwarn && is.finite(covar) && covar > 5 &&
		(!is.finite(InvReadings[t]) || (abs(InvReadings[t] - meanyld) > 2*sddev)))
		{
			body = paste(body,">>> Inverter outside range")
		}
	}
	body = paste(body,"\n\nStdev/COV Yields: ",sddev," / ",covar,"%",sep="")
	for(t in 1 : noInverters)
	{
		body = paste(body,"\n\n Inverter Availability ",acname[(t+1+NOMETERS)]," [%]: ",as.character(InvAvail[t]),sep="")
	}
	bodyac = paste(bodyac,"\n\nSystem Full Generation [kWh]:",sum(globMtVal))
	bodyac = paste(bodyac,"\n\nSystem Full Yield [kWh]:",
		round(sum(globMtVal)/sum(INSTCAPM),2))
	if(GTI == 0)
	{
	bodyac = paste(bodyac,"\n\nGTI [kWh/m2]:",GTI)
	bodyac = paste(bodyac,"\n\nSystem Full PR [%]:",
		round(sum(globMtVal)*100/sum(INSTCAPM)/GTI,1),"\n\n")
	bodyac = paste(bodyac,"\n\nCalculating PR from IN-036C because data from IN-036S is not coming in","\n\n")
	}
	else
	{
	bodyac = paste(bodyac,"\n\nGTI [kWh/m2]:",GTI2)
	bodyac = paste(bodyac,"\n\nSystem Full PR [%]:",
		round(sum(globMtVal)*100/sum(INSTCAPM)/GTI2,1),"\n\n")
	}
	for(t in 1 : NOMETERS)
	{
		 bodyac = paste(bodyac,"Yield",acname[(t+1)],"[kWh/kWp]:",
			 as.character(globMtYld[t]),"\n\n")
	}
	sdm =round(sdp(globMtYld),3)
	covarm = round(sdm*100/mean(globMtYld),1)
	bodyac = paste(bodyac,"Stdev/COV Yields:",sdm,"/",covarm,"[%]")
	body = paste(bodyac,body,sep="")
	body = gsub("\n ","\n",body)
	graph_command=paste("sudo python3 /home/admin/CODE/IN036CDigest/Inverter_CoV_Graph.py",'IN-036',currday,sep=" ")
	print(graph_command)
	system(graph_command,wait=TRUE)
	print('Graph Done!')
	graph_path=paste('/home/admin/Graphs/Graph_Output/IN-036/[IN-036] Graph ',currday,' - Inverter CoV.pdf',sep="")
	print(graph_path)
	graph_extract_path=paste('/home/admin/Graphs/Graph_Extract/IN-036/[IN-036] Graph ',currday,' - Inverter CoV.txt',sep="")
	print(graph_extract_path)
	filestosendpath = c(graph_path,graph_extract_path,pathall)
  send.mail(from = sender,
            to = recipients,
            subject = paste("Station [IN-036C] Digest",currday),
            body = body,
            smtp = list(host.name = "smtp.office365.com", port = 587, user.name = uname, passwd = pwd, tls = TRUE),
            authenticate = TRUE,
            send = TRUE,
            attach.files = filestosendpath,
            #file.names = filenams, # optional paramete
            debug = F)
recordTimeMaster("IN-036C","Mail",currday)
}


path = "/home/admin/Dropbox/FlexiMC_Data/Gen1_Data/[IN-036C]"
path2G = '/home/admin/Dropbox/FlexiMC_Data/Second_Gen/[IN-036C]'
path3G = '/home/admin/Dropbox/FlexiMC_Data/Third_Gen/[IN-036C]'
path4G = '/home/admin/Dropbox/FlexiMC_Data/Fourth_Gen/[IN-036C]'

if(RESETHISTORICAL)
{
	command = paste("rm -rf",path2G)
	system(command)
	command = paste("rm -rf",path3G)
	system(command)
	command = paste("rm -rf",path4G)
	system(command)
}

checkdir(path2G)
checkdir(path3G)

years = dir(path)
stnnickName2 = "IN-036C"
lastdatemail = lastMailDate(paste('/home/admin/Start/MasterMail/',stnnickName2,'_Mail.txt',sep=""))
stopDate = paste("[",stnnickName2,"]-SI46-",lastdatemail,".txt",sep="")
print(paste('StopDate is',stopDate))
ENDCALL=0
for(x in 2 : length(years)) # 2017 data isn't valid so start from 2018
{
	path2Gyr = paste(path2G,years[x],sep = "/")
	pathyr = paste(path,years[x],sep="/")
	checkdir(path2Gyr)
	months = dir(pathyr)
	startmon = 1
	if(x == 2)
	{
		startmon = 2 # Jan data is also not valid for 2018
	}
	for(y in startmon : length(months))
	{
		path2Gmon = paste(path2Gyr,months[y],sep = "/")
		pathmon = paste(pathyr,months[y],sep="/")
		checkdir(path2Gmon)
		stns = dir(pathmon)
		stns = stns[reorderStnPaths]
		dunmun = 0
		for(t in 1 : length(stns))
		{
			type = 1
			if(grepl("MFM",stns[t]))
				type = 0
			if(stns[t] == "WMS")
				type = 2
			pathmon1 = paste(pathmon,stns[t],sep="/")
		  days = dir(pathmon1)
		  path2Gmon1 = paste(path2Gmon,stns[t],sep = "/")
			if(!file.exists(path2Gmon1))
			{	
				dir.create(path2Gmon1)
			}

		if(length(days)>0)
		{
		for(z in 1 : length(days))
		{
			if(ENDCALL == 1)
				break
			if((z==length(days)) && (y == length(months)) && (x ==length(years)))
				next
			print(days[z])
			pathfinal = paste(pathmon1,days[z],sep = "/")
			path2Gfinal = paste(path2Gmon1,days[z],sep="/")
			if(RESETHISTORICAL)
			{
				secondGenData(pathfinal,path2Gfinal,type)
			}
			if(!dunmun){
			daysAlive = daysAlive+1
			}
			if(days[z] == stopDate)
			{
				ENDCALL = 1
				print('Hit end call')
				next
			}
		}
		}
		dunmun = 1
		if(ENDCALL == 1)
			break
	}
	if(ENDCALL == 1)
	break
}
if(ENDCALL == 1)
break
}

print('Backlog done')

prevx = x
prevy = y
prevz = z
repeats = 0
SENDMAILTRIGGER = 0
newlen=1
while(1)
{
	recipients = getRecipients("IN-036C","m")
	recordTimeMaster("IN-036C","Bot")
	years = dir(path)
	noyrs = length(years)
	path2Gfinalall = vector('list')
	for(x in prevx : noyrs)
	{
		pathyr = paste(path,years[x],sep="/")
		path2Gyr = paste(path2G,years[x],sep="/")
		checkdir(path2Gyr)
		mons = dir(pathyr)
		nomons = length(mons)
		startmn = prevy
		endmn = nomons
		if(startmn>endmn)
		{
			startmn = 1
			prevx = x-1
			prevz = 1
		}
		for(y in startmn:endmn)
		{
			pathmon = paste(pathyr,mons[y],sep="/")
			path2Gmon = paste(path2Gyr,mons[y],sep="/")
			checkdir(path2Gmon)
			stns = dir(pathmon)
			if(length(stns) < 30) ## Need to make change according to no.of  substations
			{
				print('Station sync issue.. sleeping for an hour')
				Sys.sleep(3600) # Sync issue, meter data comes after MFM and WMS
				stns = dir(pathmon)
			}
			stns = stns[reorderStnPaths]
			for(t in 1 : length(stns))
			{
				newlen = (y-startmn+1) + (x-prevx)
				print(paste('Reset newlen to',newlen))
				type = 1
				if(grepl("MFM",stns[t]))
					type = 0
				if(stns[t] == "WMS")
					type = 2
			  pathmon1 = paste(pathmon,stns[t],sep="/")
			  days = dir(pathmon1)
			  path2Gmon1 = paste(path2Gmon,stns[t],sep = "/")
			chkcopydays = days[grepl('Copy',days)]
			if(!file.exists(path2Gmon1))
			{
			dir.create(path2Gmon1)
			}
			if(length(chkcopydays) > 0)
			{
				print('Copy file found they are')
				print(chkcopydays)
				idxflse = match(chkcopydays,days)
				print(paste('idx matches are'),idxflse)
				for(innerin in 1 : length(idxflse))
				{
					command = paste("rm '",pathmon,"/",days[idxflse[innerin]],"'",sep="")
					print(paste('Calling command',command))
					system(command)
				}
				days = days[-idxflse]
			}
			days = days[complete.cases(days)]
			if(t==1)
				referenceDays <<- extractDaysOnly(days)
			else
				days = analyseDays(days,referenceDays)
			nodays = length(days)
			if(y > startmn)
			{
				z = prevz = 1
			}
			if(nodays > 0)
			{
			startz = prevz
			if(startz > nodays)
				startz = nodays
			for(z in startz : nodays)
			{
				if(t == 1)
				{
					path2Gfinalall[[newlen]] = vector('list')
				}
				if(is.na(days[z]))
					next
				pathdays = paste(pathmon1,days[z],sep = "/")
				path2Gfinal = paste(path2Gmon1,days[z],sep="/")
				secondGenData(pathdays,path2Gfinal,type)
				if((z == nodays) && (y == endmn) && (x == noyrs))
				{
					if(!repeats)
					{
						print('No new data')
						repeats = 1
					}
					next
				}
				if(is.na(days[z]))
				{
					print('Day was NA, do next in loop')
					next
				}
			  SENDMAILTRIGGER <<- 1
				if(t == 1)
				{
					splitstr =unlist(strsplit(days[z],"-"))
					daysSend = paste(splitstr[4],splitstr[5],splitstr[6],sep="-")
					print(paste("Sending",daysSend))
					if(performBackWalkCheck(daysSend) == 0)
					{
						print("Sync issue, sleeping for an 1 hour")
						Sys.sleep(3600) # Sync issue -- meter data comes a little later than
					   	             # MFM and WMS
					}
				}
				repeats = 0
				print(paste('New data, calculating digests',days[z]))
				path2Gfinalall[[newlen]][t] = paste(path2Gmon1,days[z],sep="/")
				newlen = newlen + 1
				print(paste('Incremented newlen by 1 to',newlen))
			}
			}
			}
		}
	}
	if(SENDMAILTRIGGER)
	{
	print('Sending mail')
	print(paste('newlen is ',newlen))
	for(lenPaths in 1 : (newlen - 1))
	{
		if(lenPaths < 1)
			next
		print(unlist(path2Gfinalall[[lenPaths]]))
		pathsend = unlist(path2Gfinalall[[lenPaths]])
		daysAlive = daysAlive+1
		sendMail(pathsend)
	}
	SENDMAILTRIGGER <<- 0
	newlen = 1
	print(paste('Reset newlen to',newlen))

	}

	prevx = x
	prevy = y
	prevz = z
	Sys.sleep(3600)
}
print(paste('Exited for some reason x y z values are'),x,y,z)
sink()
