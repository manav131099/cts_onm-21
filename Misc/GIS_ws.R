rm(list = ls())

path = "/home/admin/Dropbox/GIS/Raw Files"

days = dir(path)

days = days[grepl("Coim",days)]
days = days[grepl("201805",days)]
days = days[-2]
print(length(days))

dateall = wsmean = wsmax = DA = c()
idxglobal = 1
for(x in 1 : length(days))
{
   pathdata = paste(path,days[x],sep="/")
	 print(pathdata)
	 dataread = read.table(pathdata,header =T,sep = ";")
	 date = as.character(dataread[,1])
	 dateparse = unique(date)
	 for(y in 1 : length(dateparse))
	 {
	 	idxuse = which(date %in% dateparse[y])
		dateall[idxglobal] = gsub("\\.","-",dateparse[y])
		temp = unlist(strsplit(dateall[idxglobal],"-"))
		dateall[idxglobal] = paste(temp[3],temp[2],temp[1],sep="-")
		wsmean[idxglobal] = wsmax[idxglobal] = DA[idxglobal] =NA
		if(length(idxuse))
		{
		   datasub = as.numeric(dataread[idxuse,10])
			 wsmean[idxglobal] = round(mean(datasub),1)
			 wsmax[idxglobal] = max(datasub)
			 DA[idxglobal] = round(length(idxuse)/.24,1)
		}
		print(dateall[idxglobal])
		idxglobal = idxglobal + 1
	 }
}

df = data.frame(Date=dateall,WSMean=wsmean,WSMax=wsmax,DA=DA)

write.table(df,file="/home/admin/ws_coim.txt",row.names =F,col.names =T,append=F,sep="\t")

