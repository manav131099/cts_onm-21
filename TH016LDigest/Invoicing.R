invoice = function(currday) {

rm(list=ls(all =TRUE))
date = as.character(Sys.Date()-1)
year = substr(date,1,4)
print(year)
yrmon = substr(date,1,7)
print(yrmon)
pathRead <- paste('/home/admin/Dropbox/Gen 1 Data/[TH-016L]/',year,'/',yrmon,'/MFM_1_PV Meter',sep="")     #MFM_1
setwd(pathRead)

writetxt <- paste('/tmp/[TH-016L] TH Invoicing summary ',date,'.txt',sep="")
filelist <- dir(pattern = ".txt", recursive= TRUE)

rf = function(x){
  return(format(round(x,2),nsmall=2))
}

coerce_num <- function(x)
{
  v1 <- as.numeric(as.character(x))
  return(v1)
}

#Peak condition 1 
#Condition 1; defining peak time
timemin <- format(as.POSIXct("2020-02-01 09:00:00"), format="%H:%M:%S")
timemax <- format(as.POSIXct("2020-02-01 22:00:00"), format="%H:%M:%S")

#Public holidays obs in 2018 APRIL [Thailand] 
#Condition 2; defining off peak days on week days)
##https://www.officeholidays.com/countries/thailand/index.php
PH = c(
  as.Date("2018-04-06"),    #Chakri Day
  as.Date("2018-04-12"),    #Songkran 4
  as.Date("2018-04-13"),    #Songkran
  as.Date("2018-04-14"),    #Songkran 2
  as.Date("2018-04-15"),    #Songkran 3
  as.Date("2018-04-16"),    #Songkran obs
  as.Date("2018-05-01"),    #Labour day
  as.Date("2018-05-14"),    #Royal ploughing ceremony
  as.Date("2018-05-29"),    #Visakha Bucha
  as.Date("2018-07-27"),   #Asaalha Bucha
  as.Date("2018-08-12"),   #Queen's bday
  as.Date("2018-08-13"),   #Queen's bday obs
  as.Date("2018-10-13"),   #Anniversary of Death of King Bhumibol
  as.Date("2018-10-15"),   #Anniversary of Death of King Bhumibol obs
  as.Date("2018-10-23"),   #Chulalongkorn Day
  as.Date("2018-12-05"),   #Father's day
  as.Date("2018-12-10"),   #Constitution Day
  as.Date("2018-12-31"),
  as.Date("2019-01-01"),
  as.Date("2019-02-19"),
  as.Date("2019-04-15"),
  as.Date("2019-05-01"),
  as.Date("2019-07-16"),
  as.Date("2019-07-17"),
  as.Date("2019-08-12"),
  as.Date("2019-10-23"),
  as.Date("2019-12-05"),
  as.Date("2019-12-10"),
  as.Date("2019-12-31"),
  as.Date("2020-01-01"),
  as.Date("2020-04-06"),
  as.Date("2020-05-01"),
  as.Date("2020-05-04"),
  as.Date("2020-05-06"),
  as.Date("2020-06-03"),
  as.Date("2020-07-06"),
  as.Date("2020-07-28"),
  as.Date("2020-08-12"),
  as.Date("2020-10-13"),
  as.Date("2020-10-23"),
  as.Date("2020-12-10"),
  as.Date("2020-12-31")
)   #New year's eve

#dates <- seq.Date(as.Date("2017-01-01"),as.Date("2017-12-31"),by="1 day")

i=0
df=dff=NULL
for(z in filelist[1:length(filelist)]){
 
  temp <- read.table(z, header = T, sep= '\t', stringsAsFactors = F)
  #data availability
  
  da <- (length(temp[,1])/288)*100
  condition0 <- format(as.POSIXct(temp[,1]), format="%H:%M:%S") > format(as.POSIXct("2017-01-07 05:59:00"), format="%H:%M:%S") & 
    format(as.POSIXct(temp[,1]), format="%H:%M:%S") < format(as.POSIXct("2017-01-07 20:00:00"), format="%H:%M:%S")  #for DA
  temp2 <- temp[condition0,]
  da_2 <- (length(temp2[,1])/168)*100
  condition1 <- format(as.POSIXct(temp[,1]), format="%H:%M:%S") > timemin & format(as.POSIXct(temp[,1]), format="%H:%M:%S") <timemax

  temp[,85][temp[,85] < 0] <- 0
  pac_TH <- coerce_num(temp[,85])
  total_eac <- as.numeric(sum(pac_TH, na.rm = T)/10000)          #Eac here appears as kWh
  pacTemp <- temp[condition1,]
  date=as.Date(temp[1,1])
  time_stamp <- as.character(as.POSIXct(temp[length(temp[,1]),1]))
  if(is.finite(match(date,PH)))
  {
    peak_eac = 0
    nonpeak_eac= total_eac
    holi <- "Y"
  } 
  else{
    pac_TH2 <- coerce_num(pacTemp[,85])
   # peak_eac <- as.numeric(rf(sum(pac_TH2, na.rm = T)/10000))
    peak_eac <- sum(pacTemp[,85],na.rm = T)/10000
    nonpeak_eac = total_eac - peak_eac
    holi <- "N"
  }
  d <- format(head(date), format="%w")
  day <- format(date, format = "%a")
  #Condition 3 ; weekends = Off Peak
  #defining non peak period at 0 (sunday) and 6 (saturday)
  if(d == 0 | d == 6) {
    peak_eac = 0
    nonpeak_eac = total_eac
  }
  #powertotal column BW ; 71
  #percentages
  pk_eac_per <- (peak_eac/total_eac)*100
  offpk_eac_per <- (nonpeak_eac/total_eac)*100
  start_read <- as.numeric(temp[1,59])
  last_read <- as.numeric(temp[length(temp[,1]),59])   # in Wh
  total_eac_m2 <-  last_read-start_read
  df <- cbind(as.character(date),
              day,
              holi,
              rf(da), 
              rf(da_2),
              last_read/1000,
              total_eac_m2/1000,
              rf(peak_eac), rf(nonpeak_eac), 
              rf(pk_eac_per), rf(offpk_eac_per), 
              total_eac,
              time_stamp)
  dff <- rbind(dff,df)
  print(paste(z, " done"))
}

dff=data.frame(dff)

colnames(dff) <- c("Date", "Day" ,"Holiday (Y/N)","DA (%)", "DA_6AM_TO_8PM (%)",
                   "Eac delivered (kWh)",  "Eac2 (kWh)","Peak_Eac (kWh)",
                   "Offpeak_Eac (kWh)", "Peak_Eac (%)", "Offpeak_Eac (%)", "Eac1 (kWh)" ,"Time")

write.table(dff,writetxt, row.names = FALSE, sep= "\t")

}