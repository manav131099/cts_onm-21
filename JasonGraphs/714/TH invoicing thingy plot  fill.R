################
################
rm(list=ls(all =TRUE))

library(ggplot2)
library(scales)

rf = function(x){
  return(format(round(x,2),nsmall=2))
}

lm_eqn <- function(x,y,data)
{
  m <- lm(y ~ x, data);
  eq <- substitute(italic(y) == a + b %.% italic(x)*","~~italic(r)^2~"="~r2, 
                   list(a = format(coef(m)[1], digits = 2), 
                        b = format(coef(m)[2], digits = 2), 
                        r2 = format(summary(m)$r.squared, digits = 3)))
  as.character(as.expression(eq));                 
}

themesettings1 <- theme(plot.title = element_text(hjust = 0.5), axis.title.x = element_text(size=19), axis.title.y = element_text(size=19), 
                        axis.text = element_text(size=11), 
                        panel.grid.minor = element_blank(), panel.border = element_blank(),axis.line = element_line(colour = "black"),
                        legend.justification = c(1, 1), legend.position = c(1, 1))

titlemonth = "714"
#allocating column number
date=1
gsi = 3
ac = 71
#

path = "~/intern/Data/[714]/2017/2017-03"

setwd(path)
filelist <- dir(pattern = ".txt", recursive= TRUE)
dffg=dff=NULL
for(z in filelist[1:length(filelist)]){
  temp <- read.table(z, header =T, sep= '\t', stringsAsFactors = F)
  
  pacTemp <- temp
  irrad <- as.numeric(paste(pacTemp[,gsi]))
  pacTemp1 <- pacTemp[complete.cases(as.numeric(paste(abs(pacTemp[,ac])))),]
  pacpoints <- abs(as.numeric(as.character(pacTemp[,ac])))
  gsipoints <- abs(irrad)
  timestamps <- cbind(pacTemp[,date],gsipoints)
  dffg <- rbind(dffg,data.frame(timestamps))
  dff <- rbind(dff,data.frame(pacpoints))
  print(paste(z, "done"))
}

dff=data.frame(dff)
dffg=data.frame(dffg)
dffg=cbind(dffg,dff)

#conversion
dffg$pacpoints <- (dffg$pacpoints/63)*1404

#melt
dffg$date <- as.Date(substr(dffg$V1,0,10))
#1 week
df <- with(dffg, dffg[(date > "2017-03-04" & date < "2017-03-12"), ])
df$V1 <- as.POSIXct(df$V1, format="%Y-%m-%d %H:%M")

#sort
timemin <- format(as.POSIXct("2017-01-10 08:59:59"), format="%H:%M:%S")
timemax <- format(as.POSIXct("2017-11-06 22:01:00"), format="%H:%M:%S")

df$sort = 0

df$sort[format(as.POSIXct(df$V1), format = "%H:%M:%S") > timemin] <- "peak"  #sort peak and non peak timings

df$sort[format(df$date, format="%w") == 0 | format(df$date, format="%w") == 6] <- "nonpeak"      #sort sunday and saturday
df$sort[df$sort == 0] <- "nonpeak"

df$group[df$sort == "peak"] <- "1"
df$group[df$sort == "nonpeak"] <- "2"

cols <- c("peak" = "red", "nonpeak" = "lightblue")

df2 <- with(df, df[(date > "2017-03-07" & date < "2017-03-09"), ])

#df2$group[df2[1:540,1]] <- "2"
#df2$group[df2[540:1440,1]] <- "1"
df2$time <- substr(df2$V1,12,19)
df2$seq <- seq(1,1440,1)

p4 <- ggplot(data = df2, aes(x=seq)) + theme_bw()
p4 <- p4 + geom_line(data = df2, aes(y = pacpoints, colour=group), size = 1) 
p4 <- p4 + ylab("Power AC [kW]") + xlab("Time")
p4 <- p4 + themesettings1
#p4 <- p4 + geom_ribbon(data = df2, aes(x= time, ymax=pacpoints, group=group, fill=group), ymin=0, alpha = 0.8)
p4 <- p4 + geom_ribbon(aes(ymax=pacpoints, fill = group), ymin=0, alpha=0.8)
#p4 <- p4 + scale_x_discrete(breaks = c(df2[1,7], df2[316,7], df2[721,7], df2[1081,7], df2[1440,7]), labels = c("00:00","06:00","12:00","18:00","23:59"))
p4 <- p4 + scale_x_continuous(breaks = c(1,361,721,1081,1440), labels = c("00:00","06:00","12:00","18:00","23:59"))
p4 <- p4 + scale_colour_manual(name='Period', values=c("1" = "red3", "2" = "blue3"))
p4 <- p4 + scale_fill_manual(name='Period', values=c("1" = "lightcoral", "2" = "lightblue"), labels=c("Peak", "Off-Peak")) + guides(colour=F)   #guide to remove certain aes legend
p4


ggsave(paste0('~/intern/Results/[714]/1. result/1 day pac plot fill (2017-03-08).pdf'), p4, width =11, height=6)

