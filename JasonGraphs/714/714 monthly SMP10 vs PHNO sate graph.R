rm(list=ls(all =TRUE))

library(ggplot2)
result <- read.table("C:/Users/talki/Desktop/cec intern/results/714/[714]_smp10_summary.txt",header = T)
monthlist <- unique(result[,5])
counter=1
for(i in monthlist){
  temp <- result[result[,5] == i & result[,2] >= 99,]
  #result <- result[result[,2] >= 99,]
  
  model <- lm(temp[,3]~temp[,4])
  rsq <- format(as.numeric(summary(model)$r.squared),digits=3,nsmall = 3)
  graph <- ggplot(temp, aes(x=measure,y=model,group=Month))
  p1 <- graph + geom_point(aes(shape = Month,colour = Month))
  p1 <- p1 + theme_bw()
  p1 <- p1 + scale_shape_manual(values=counter)
  p1 <- p1 + expand_limits(x=0,y=0)
  p1 <- p1 + coord_cartesian(ylim=c(0,8),xlim=c(0,8))
  p1 <- p1 + geom_abline(intercept = 0, slope = 1)
  p1 <- p1 + annotate("text",label = paste0("R-sq error: ",rsq),size=4, x = 4, y= 1)
  p1 <- p1 + xlab(expression(paste("Ground Global Horizontal Irradiation [",kwh/m^2,", daily]")))
  p1 <- p1 + ylab(expression(paste("Satellite Global Horizontal Irradiation [",kwh/m^2,", daily]")))
  name <- paste0("714_",i,"_filtered.pdf")
  
  ggsave(p1, filename = paste0("C:/Users/talki/Desktop/cec intern/results/714/GSI/",name), width = 7.92, height = 5)
  print(paste(i," done"))
  counter=counter+1
}
