rm(list=ls(all =TRUE))

#####
pathRead <- 
#####

setwd(pathRead)

filelist <- dir(pattern = ".txt", recursive= TRUE)
timemin <- format(as.POSIXct("2016-09-17 06:59:59"), format="%H:%M:%S")
timemax <- format(as.POSIXct("2016-09-17 17:00:00"), format="%H:%M:%S")

stationLimit <- 
nameofStation <- 

col0 <- c()
col1 <- c()
col2 <- c()
col3 <- c()
col4 <- c()
col5 <- c()
col6 <- c()
col0[10^6] = col1[10^6] = col2[10^6] = col3[10^6] = col4[10^6] = col5[10^6] = col6[10^6] = 0
index <- 1

for (i in filelist){
  data <- NULL
  temp <- read.table(i,header = T, sep = "\t")
  date <- substr(as.character(temp[1,1]),1,10)
  col0[index] <- nameofStation
  col1[index] <- date
  
  #data availability
  pts <- length(temp[,1])
  col3[index] <- pts
  
  #pac
  condition <- format(as.POSIXct(temp[,1]), format="%H:%M:%S") > timemin &
    format(as.POSIXct(temp[,1]), format="%H:%M:%S") <timemax
  pacTemp <- temp[condition,]
  pacTemp <- pacTemp[complete.cases(pacTemp[,2]),]
  
  ### change the index inside
  pac <- as.numeric(pacTemp[,2])
  #############################
  
  numer <- length(pac[pac>0.1])
  pacTemp2 <- pacTemp[pac<0.1,]
  ###change the index inside
  gsi <- as.numeric(paste(pacTemp2[,4]))
  ##########################
  
  pacTemp2 <- pacTemp2[gsi>20,]
  denom <- length(pacTemp2[,1])+numer
  flagRate <- numer/denom
  col4[index] <- flagRate*100
  
  #wpac
  
  #### change the index inside
  gsi <- paste(as.numeric(pacTemp[,4]))
  ############################
  
  
  flag <- as.numeric(pac<0.1 & gsi > 20)
  wgsi <- gsi/sum(gsi)
  downloss <- wgsi * flag * 100
  if (length(downloss)>0){
    col5[index] <- 100 - sum(downloss)
  }
  else{
    col5[index] <- NA
  }
  
  #pr
  gsi <- temp[,4]
  eac <- temp[,3][complete.cases(temp[,3])]
  diff <- eac[length(eac)]- eac[1]
  pr <- diff/stationLimit/sum(gsi)*12000
  if (length(pr)==0){
    col6[index] = NA
  }
  else{
    col6[index] <- pr*100
  }
  index <- index + 1
}

col0 <- col0[1:(index-1)]
col1 <- col1[1:(index-1)]
col2 <- col2[1:(index-1)]
col3 <- col3[1:(index-1)]
col4 <- col4[1:(index-1)]
col5 <- col5[1:(index-1)]
col6 <- col6[1:(index-1)]
col2 <- col3/max(col3)*100
col2[is.na(col2)] <- NA
col3[is.na(col3)] <- NA
col4[is.na(col4)] <- NA
col5[is.na(col5)] <- NA
col6[is.na(col6)] <- NA
