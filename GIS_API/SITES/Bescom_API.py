import datetime as DT
import requests
from lxml import etree
import pandas as pd
import csv

today = DT.date.today()
api_key = 'HesHutsorc8mnk95sc'
url = 'https://solargis.info/ws/rest/datadelivery/request?key=%s' % api_key
headers = {'Content-Type': 'application/xml'}
xmlfile = open('/home/admin/CODE/GIS_API/API_req_GTI.xml', 'r')
body = xmlfile.read()
today = DT.date.today()
lat = 14.80685
lng = 76.124664
tilt = 12
azimuth = 180
week_ago = today - DT.timedelta(days=7)
yesterday = today - DT.timedelta(days=1)
today = yesterday
response = requests.post(url, data=body.format(Start_Date= today, End_Date = today, latitude= lat , longitude = lng, tilt = tilt, azimuth = azimuth), headers=headers)

      
data = []
ghi_rounded = []
gti_rounded = []
ts_rounded = []
root = etree.fromstring(bytes(response.text, encoding='utf-8'))
for element in root.iter("*"):
    data.append(element.items())
tuple_list = [item for t in data for item in t] 
first_tuple_elements = []

for a_tuple in tuple_list:
    first_tuple_elements.append(a_tuple[1])
#print(first_tuple_elements)
ts = first_tuple_elements[1::2]
for i in ts:
  x = slice(0, 10)
  ts_rounded.append(i[x])

ghi = first_tuple_elements[2::2]
ghi.pop(0)
for i in ghi:
  i = i.split()

  i[0] = float(i[0])
  i[1] = float(i[1])
  i[0] = round(i[0],2)
  i[1] = round(i[1],2)
  ghi_rounded.append(i[0])
  gti_rounded.append(i[1])
list_of_tuples = list(zip(ts_rounded, ghi_rounded)) 
df = pd.DataFrame(list_of_tuples) 
ts_rounded.pop(0)

df = df.iloc[1:]

file = open("/home/admin/Dropbox/GIS_API/BESCOM/BESCOM_AGGREGATE.txt", "a")
for index in range(len(ts_rounded)):
    file.write(str(ts_rounded[index]) + "\t" + str(ghi_rounded[index]) +  "\t" + str(gti_rounded[index]) + "\n")
file.close()
    

