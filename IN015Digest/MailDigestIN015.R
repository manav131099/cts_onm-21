errHandle = file('/home/admin/Logs/LogsIN015Mail.txt',open='w',encoding='UTF-8')
sink(errHandle,type='message',append = T)
sink(errHandle,type='output',append = T)

system('rm -R "/home/admin/Dropbox/Second Gen/[IN-015S]"')
rm(list = ls())
require('mailR')
require('tidyr')
require('reshape2')
source('/home/admin/CODE/IN015Digest/runHistoryIN015.R')
source('/home/admin/CODE/Send_mail/sendmail.R')
print('History done')
source('/home/admin/CODE/IN015Digest/3rdGenDataIN015.R')
print('3rd gen data called')
source('/home/admin/CODE/MasterMail/timestamp.R')
source('/home/admin/CODE/IN015Digest/aggregateInfo.R')
timetomins = function(x)
{
  hr = unlist(strsplit(x,":"))
  min = as.numeric(hr[2])
  hr = as.numeric(hr[1])
  return((hr * 60 + min + 1))
}
rf = function(x)
{
  return(format(round(x,2),nsmall=2))
}
rf1 = function(x)
{
  return(format(round(x,1),nsmall=1))
}
rf3 = function(x)
{
  return(format(round(x,3),nsmall=3))
}
checkdir = function(x)
{
  if(!file.exists(x))
  {
    dir.create(x)
  }
}

preparebody = function(path)
{
  print("Enter Body Func")
  body = ""
  body = paste(body,"Site Name: Apollo Tyres\n",sep="")
  body = paste(body,"\nLocation: Chennai, India\n",sep="")
  body = paste(body,"\nO&M Code: IN-015\n",sep="")
  body = paste(body,"\nSystem Size: 1,814.4 kWp\n",sep="")
  body = paste(body,"\nSystem Size facing North: 875.70 kWp (48.2% of total)\n",sep="")
  body = paste(body,"\nSystem Size facing South: 938.70 kWp (51.8% of total)\n",sep="")
  body = paste(body,"\nNumber of Energy Meters: 1\n",sep="")
  body = paste(body,"\nModule Brand / Model / Nos: Canadian Solar / 315W / 5760\n",sep="")
  body = paste(body,"\nInverter Brand / Model / Nos: SMA / STP60 / 24\n",sep="")
  body = paste(body,"\nSite COD: 2016-07-14\n",sep="")
  body = paste(body,"\nSystem age [days]: ",(20+DAYSACTIVE),sep="")
  body = paste(body,"\n\nSystem age [years]: ",round((20+DAYSACTIVE)/365,2),sep="")
  for(x in 1 : length(path))
  {
    print(path[x])
    body = paste(body,"\n\n_____________________________________________\n",sep="")
    days = unlist(strsplit(path[x],"/"))
    days = days[length(days)]
    days = unlist(strsplit(days," "))
    days = unlist(strsplit(days[2],"\\."))
    body = paste(body,days[1],sep="")
    body  = paste(body,"\n_____________________________________________\n",sep="")
    dataread = read.table(path[x],header = T,sep="\t")
    body = paste(body,"\nPts Recorded: ",as.character(dataread[1,2])," (",round(as.numeric(dataread[1,2])/14.4,1),"%)",sep="")
    body = paste(body,"\n\nDaily Irradiation Silicon Sensor [kWh/m2]: ",as.character(dataread[1,3]),sep="")
    body = paste(body,"\n\nDaily Irradiation Silicon Sensor, North [kWh/m2]: ",as.character(dataread[1,4]),sep="")
    body = paste(body,"\n\nDaily Irradiation Silicon Sensor, South [kWh/m2]: ",as.character(dataread[1,5]),sep="")
    body = paste(body,"\n\nDaily Irradiation Silicon Sensor, East [kWh/m2]: ",as.character(dataread[1,44]),sep="")
    body = paste(body,"\n\nDaily Irradiation Silicon Sensor, West [kWh/m2]: ",as.character(dataread[1,45]),sep="")
    body = paste(body,"\n\nGmodN/GSi Ratio: ",as.character(dataread[1,20]),sep="")
    body = paste(body,"\n\nGmodS/GSi Ratio: ",as.character(dataread[1,21]),sep="")
    body = paste(body,"\n\nGmodE/GSi Ratio: ",as.character(dataread[1,46]),sep="")
    body = paste(body,"\n\nGmodW/GSi Ratio: ",as.character(dataread[1,47]),sep="")
    body = paste(body,"\n\nWeighted Gmod for Site [kWh/m2]: ",as.character(dataread[1,34]),sep="")
    hvyield = as.numeric(dataread[1,37])
    lvyield = as.numeric(dataread[1,38])
    body = paste(body,"\n\nDaily specific yield LV side [kWh/kWp]: ",as.character(lvyield),sep="")
    prlv = as.numeric(dataread[1,39])
    prlvmod = as.numeric(dataread[1,40])
    
    prhv = as.numeric(dataread[1,41])
    prhvmod =as.numeric(dataread[1,42])
    
    body = paste(body,"\n\nPR GHI LV side: ",as.character(prlv),sep="")
    
    body = paste(body,"\n\nPRmod LV side: ",as.character(prlvmod),sep="")
    body = paste(body,"\n\nDaily specific yield HV side [kWh/kWp]: ",as.character(hvyield),sep="")
    body = paste(body,"\n\nPR GHI HV side: ",as.character(prhv),sep="")
    body = paste(body,"\n\nPRmod HV side: ",as.character(prhvmod),sep="")
    body = paste(body,"\n\nGrid Availability LV side [%]: ",as.character(dataread[,35]),"%",sep="")
    body = paste(body,"\n\nGrid Availability HV side [%]: ",as.character(dataread[,36]),"%",sep="")
    
    body = paste(body,"\n\nSolar Energy Delivered LV Side [kWh]: ",as.character(dataread[1,22]),sep="")
    body = paste(body,"\n\nSolar Energy Delivered HV Side [kWh]: ",as.character(dataread[1,23]),sep="")
    body = paste(body,"\n\nTrue solar generation HV side [kWh]: ",as.character(dataread[1,29]),sep="")
    body = paste(body,"\n\nSite consumption HV side [kWh]: ",as.character(dataread[1,30]),sep="")
    body = paste(body,"\n\nTransformer Losses [%]: ",as.character(dataread[1,24]),sep="")
    body = paste(body,"\n\nLosses due to site consumption [%]: ",as.character(dataread[1,31]),sep="")
    body = paste(body,"\n\nMean Ambient Temperature [C]: ",as.character(dataread[1,6]),sep="")
    body = paste(body,"\n\nMean Ambient Temperature, sun hours [C]: ",as.character(dataread[1,7]),sep="")
    body = paste(body,"\n\nMean Humidity [%]: ",as.character(dataread[1,12]),sep="")
    body = paste(body,"\n\nMean Humidity, sun hours [%]: ",as.character(dataread[1,13]),sep="")
    body = paste(body,"\n\nMean Module Temperature North, sun hours [C]: ",as.character(dataread[1,32]),sep="")
    body = paste(body,"\n\nMean Module Temperature South, sun hours [C]: ",as.character(dataread[1,33]),sep="")
    body = paste(body,"\n\nLast recorded time: ",as.character(dataread[1,26]),sep="")
    body = paste(body,"\n\nLast recorded reading HV side [kWh]: ",as.character(dataread[1,25]),sep="")
    body = paste(body,"\n\nLast recorded reading true solar generation HV side [kWh]: ",as.character(dataread[1,27]),sep="")
    body = paste(body,"\n\nLast recorded reading site consumption HV side [kWh]: ",as.character(dataread[1,28]),sep="")
  }
  print('Daily Summary for Body Done')
  body = paste(body,"\n\n_____________________________________________\n\n",sep="")
  
  body = paste(body,"Station History",sep="")
  body = paste(body,"\n\n_____________________________________________\n\n",sep="")
  
  body = paste(body,"Station Birth Date: ",dobstation)
  body = paste(body,"\n\n# Days station active: ",DAYSACTIVE)
  body = paste(body,"\n\n# Years station active: ",rf1(DAYSACTIVE/365))
  body = paste(body,"\n\nTotal irradiation from Si sensor this month [kWh/m2]: ",MONTHLYIRR1,sep="")
  body = paste(body,"\n\nTotal irradiation from 10N sensor this month [kWh/m2]: ",MONTHLYIRR2,sep="")
  body = paste(body,"\n\nTotal irradiation from 10S sensor this month [kWh/m2]: ",MONTHLYIRR3,sep="")
  body = paste(body,"\n\nAverage irradiation from Si sensor this month [kWh/m2]: ",MONTHAVG1,sep="")
  body = paste(body,"\n\nAverage irradiation from Si sensor 10N this month [kWh/m2]: ",MONTHAVG2,sep="")
  body = paste(body,"\n\nAverage irradiation from Si sensor 10S this month [kWh/m2]: ",MONTHAVG3,sep="")
  body = paste(body,"\n\nForecasted irradiation total for Si sensor this month [kWh/m2]: ",FORECASTIRR1,sep="");
  body = paste(body,"\n\nForecasted irradiance total for 10N Si sensor this month [kWh/m2]: ",FORECASTIRR2,sep="");
  body = paste(body,"\n\nForecasted irradiance total for 10S Si sensor this month [kWh/m2]: ",FORECASTIRR3,sep="");
  body = paste(body,"\n\nIrradiation total last 30 days from Si sensor [kWh/m2]: ",LAST30DAYSTOT,sep="")
  body = paste(body,"\n\nIrradiation average last 30 days from Si sensor [kWh/m2]: ",LAST30DAYSMEAN,sep="")
  body = paste(body,"\n\nGmodN/GSi loss (%): ",SOILINGDEC,sep="")
  body = paste(body,"\n\nGmodN/GSi loss per day (%): ",SOILINGDECPD,sep="")
  body = paste(body,"\n\nGmodS/GSi loss (%): ",SOILINGDEC2,sep="")
  body = paste(body,"\n\nGmodS/GSi loss per day (%)",SOILINGDECPD2)
  print('3G Data Done')	
  return(body)
}
path = "/home/admin/Dropbox/Cleantechsolar/1min/[712]"
pathwrite = "/home/admin/Dropbox/Second Gen/[IN-015S]"
checkdir(pathwrite)
date = format(Sys.time(),tz = "Singapore")
years = dir(path)
prevpathyear = paste(path,years[length(years)],sep="/")
months = dir(prevpathyear)
prevsumname = paste("[IN-015S] ",substr(months[length(months)],3,4),substr(months[length(months)],6,7),".txt",sep="")
prevpathmonth = paste(prevpathyear,months[length(months)],sep="/")
currday = prevday = dir(prevpathmonth)[length(dir(prevpathmonth))]
if(!grepl("txt",currday))
{
  currday = prevday = dir(prevpathmonth)[(length(dir(prevpathmonth))-1)] 
}
prevwriteyears = paste(pathwrite,years[length(years)],sep="/")
prevwritemonths = paste(prevwriteyears,months[length(months)],sep="/")


stnnickName = "712"
stnnickName2 = "IN-015S"
lastdatemail = lastMailDate(paste('/home/admin/Start/MasterMail/',stnnickName2,'_Mail.txt',sep=""))
if(!is.null(lastdatemail))
{
  yr = substr(lastdatemail,1,4)
  monyr = substr(lastdatemail,1,7)
  prevpathyear = paste(path,yr,sep="/")
  prevsumname = paste("[",stnnickName2,"] ",substr(lastdatemail,3,4),substr(lastdatemail,6,7),".txt",sep="")
  prevpathmonth = paste(prevpathyear,monyr,sep="/")
  currday = prevday = paste("[",stnnickName,"] ",lastdatemail,".txt",sep="")
  prevwriteyears = paste(pathwrite,yr,sep="/")
  prevwritemonths = paste(prevwriteyears,monyr,sep="/")
}

check = 0
sender <- "operations@cleantechsolar.com" 
uname <- 'shravan.karthik@cleantechsolar.com'
pwd = 'CTS&*(789'
recipients <- c('operationsSouthIN@cleantechsolar.com','om-it-digest@cleantechsolar.com','oms@avisolar.com')

wt =1
while(1)
{
  recipients <- c('operationsSouthIN@cleantechsolar.com','om-it-digest@cleantechsolar.com','oms@avisolar.com')
  recordTimeMaster("IN-015S","Bot")
  years = dir(path)
  writeyears = paste(pathwrite,years[length(years)],sep="/")
  checkdir(writeyears)
  pathyear = paste(path,years[length(years)],sep="/")
  months = dir(pathyear)
  writemonths = paste(writeyears,months[length(months)],sep="/")
  sumname = paste("[IN-015S] ",substr(months[length(months)],3,4),substr(months[length(months)],6,7),".txt",sep="")
  checkdir(writemonths)
  pathmonth = paste(pathyear,months[length(months)],sep="/")
  tm = timetomins(substr(format(Sys.time(),tz = "Singapore"),12,16))
  currday = dir(pathmonth)[length(dir(pathmonth))]
  if(!grepl("txt",currday))
  {
    currday = dir(prevpathmonth)[(length(dir(prevpathmonth))-1)] 
  }
  if(prevday == currday)
  {
    if(wt == 1)
    {
      print("")
      print("")
      print("__________________________________________________________")
      print(substr(currday,7,16))
      print("__________________________________________________________")
      print("")
      print("")
      print('Waiting')
      wt = 0
    }
    {
      if(tm > 1319 || tm < 30)
      {
        Sys.sleep(120)
      }        
      else
      {
        Sys.sleep(3600)
      }
    }
    next
  }
  print('New Data found.. sleeping to ensure sync complete')
  Sys.sleep(20)
  print('Sleep Done')
  if(prevpathmonth != pathmonth)
  {
    monthlyirr1 = monthlyirr2 = monthlyirr3 = 0
    strng = unlist(strsplit(months[length(months)],"-"))
    daystruemonth = nodays(strng[1],strng[2])
    dayssofarmonth = 0
  }
  
  dataread = read.table(paste(pathmonth,currday,sep='/'),header = T,sep = "\t")
  datawrite = prepareSumm(dataread)
  datasum = rewriteSumm(datawrite)
  currdayw = gsub("712","IN-015S",currday)
  write.table(datawrite,file = paste(writemonths,currdayw,sep="/"),append = F,sep="\t",row.names = F,col.names = T)
  
  
  
  monthlyirr1 = monthlyirr1 + as.numeric(datawrite[1,3])
  monthlyirr2 = monthlyirr2 + as.numeric(datawrite[1,4])
  monthlyirr3 = monthlyirr3 + as.numeric(datawrite[1,5])
  
  globirr1 = globirr1 + as.numeric(datawrite[1,3])
  globirr2 = globirr2 + as.numeric(datawrite[1,4])
  globirr3 = globirr3 + as.numeric(datawrite[1,5])
  
  dayssofarmonth = dayssofarmonth + 1    
  daysactive = daysactive + 1
  last30days[[last30daysidx]] = as.numeric(datawrite[1,3])
  
  
  
  print('Digest Written');
  {
    if(!file.exists(paste(writemonths,sumname,sep="/")))
    {
      write.table(datasum,file = paste(writemonths,sumname,sep="/"),append = F,sep="\t",row.names = F,col.names = T)
      print('Summary file created')
    }
    else 
    {
      write.table(datasum,file = paste(writemonths,sumname,sep="/"),append = T,sep="\t",row.names = F,col.names = F)
      print('Summary file updated')  
    }
  }
  filetosendpath = c(paste(writemonths,currdayw,sep="/"))
  filename = c(currday)
  filedesc = c("Daily Digest")
  dataread = read.table(paste(prevpathmonth,prevday,sep='/'),header = T,sep = "\t")
  datawrite = prepareSumm(dataread)
  prevdayw = gsub('712','IN-015S',prevday)
  dataprev = read.table(paste(prevwritemonths,prevdayw,sep="/"),header = T,sep = "\t")
  print('Summary read')
  if(as.numeric(datawrite[,2]) != as.numeric(dataprev[,2]))
  {
    print('Difference in old file noted')
    
    
    
    previdx = (last30daysidx - 1) %% 31
    
    if(previdx == 0) {previdx = 1}
    datasum = rewriteSumm(datawrite)
    prevdayw = gsub("712","IN-015S",prevday)
    write.table(datawrite,file = paste(prevwritemonths,prevdayw,sep="/"),append = F,sep="\t",row.names = F,col.names = T)
    
    
    if(prevpathmonth == pathmonth)
    {
      monthlyirr1 = monthlyirr1 + as.numeric(datawrite[1,3]) - as.numeric(dataprev[1,3])
      monthlyirr2 = monthlyirr2 + as.numeric(datawrite[1,4]) - as.numeric(dataprev[1,4])
      monthlyirr3 = monthlyirr3 + as.numeric(datawrite[1,5]) - as.numeric(dataprev[1,5])
    }
    globirr1 = globirr1 + as.numeric(datawrite[1,3]) - as.numeric(dataprev[1,3])
    globirr2 = globirr2 + as.numeric(datawrite[1,4]) - as.numeric(dataprev[1,4])
    globirr3 = globirr3 + as.numeric(datawrite[1,5]) - as.numeric(dataprev[1,5])
    
    last30days[[previdx]] = as.numeric(datawrite[1,3])
    
    datarw = read.table(paste(prevwritemonths,prevsumname,sep="/"),header = T,sep = "\t")
    rn = match(as.character(datawrite[,1]),as.character(datarw[,1]))
    print(paste('Match found at row no',rn))
    datarw[rn,] = datasum[1,]
    write.table(datarw,file = paste(prevwritemonths,prevsumname,sep="/"),append = F,sep="\t",row.names = F,col.names = T)
    filetosendpath[2] = paste(prevwritemonths,prevdayw,sep="/")
    filename[2] = prevdayw
    filedesc[2] = c("Updated Digest")
    print('Updated old files.. both digest and summary file')
  }
  
  last30daysidx = (last30daysidx + 1 ) %% 31
  if(last30daysidx == 0) {last30daysidx = 1}
  
  LAST30DAYSTOT = rf(sum(last30days))
  LAST30DAYSMEAN = rf(mean(last30days))
  DAYSACTIVE = daysactive
  MONTHLYIRR1 = monthlyirr1
  MONTHLYIRR2 = monthlyirr2
  MONTHLYIRR3 = monthlyirr3
  MONTHAVG1 = rf(monthlyirr1 / dayssofarmonth)
  MONTHAVG2 = rf(monthlyirr2 / dayssofarmonth)
  MONTHAVG3 = rf(monthlyirr3 / dayssofarmonth)
  
  FORECASTIRR1 = rf(as.numeric(MONTHAVG1) * daystruemonth)
  FORECASTIRR2 = rf(as.numeric(MONTHAVG2) * daystruemonth)
  FORECASTIRR3 = rf(as.numeric(MONTHAVG3) * daystruemonth)
  
  SOILINGDEC = rf3(((globirr2 / globirr1) - 1) * 100)
  SOILINGDEC2 = rf3(((globirr3 / globirr1) - 1) * 100)
  
  SOILINGDECPD = rf3(as.numeric(SOILINGDEC) / DAYSACTIVE)
  SOILINGDECPD2 = rf3(as.numeric(SOILINGDEC2) / DAYSACTIVE)
  
  
  print('Sending mail')
  body = ""
  body = preparebody(filetosendpath)

  graph_command=paste("Rscript /home/admin/CODE/EmailGraphs/PR_Graph_Azure.R","IN-015",75.2,0.01,"2016-07-14",substr(currday, 7, 16),sep=" ")
  system(graph_command,wait=TRUE)	
  graph_path=paste("/home/admin/Graphs/Graph_Output/IN-015/[IN-015] Graph ", substr(currday, 7, 16), " - PR Evolution.pdf", sep="")	
  print(graph_path)
  graph_extract_path=paste("/home/admin/Graphs/Graph_Extract/IN-015/[IN-015] Graph ", substr(currday, 7, 16), " - PR Evolution.txt", sep="")
  print(graph_extract_path)

  filetosendpath = c(graph_path,graph_extract_path,filetosendpath)

  send.mail(from = sender,
            to = recipients,
            subject = paste("Station [IN-015S] Digest",substr(currday,7,16)),
            body = body,
            smtp = list(host.name = "smtp.office365.com", port = 587, user.name = uname, passwd = pwd, tls = TRUE),
            authenticate = TRUE,
            send = TRUE,
            attach.files = filetosendpath,
            debug = F)
  print('Mail Sent');
  recordTimeMaster("IN-015S","Mail",substr(currday,7,16))
  prevday = currday
  prevpathyear = pathyear
  prevpathmonth = pathmonth
  prevwriteyears = writeyears
  prevwritemonths = writemonths
  prevsumname = sumname
  wt = 1
  gc()
}
sink()
