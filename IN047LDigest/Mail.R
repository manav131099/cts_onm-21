rm(list=ls())
require('compiler')
enableJIT(3)
errHandle = file('/home/admin/Logs/LogsIN047LMail.txt',open='w',encoding='UTF-8')
sink(errHandle,type='message',append = T)
sink(errHandle,type='output',append = T)

source('/home/admin/CODE/IN047LDigest/summaryFunctions.R')
source('/home/admin/CODE/common/math.R')
source('/home/admin/CODE/Send_mail/sendmail.R')
RESETHISTORICAL=0
daysAlive = 0
reorderStnPaths = c(6,5,1,2,3,4)

source('/home/admin/CODE/MasterMail/timestamp.R')

require('mailR')
source('/home/admin/CODE/IN047LDigest/aggregateInfo.R')
#source("/home/admin/CODE/EmailGraphs/IN047L/PR_plot.R")

METERNICKNAMES = c("WMS","MFM1","INV-1","INV-2","INV-3","INV-4")

METERACNAMES = c("WMS_1","MFM_1","INVERTER_1","INVERTER_2","INVERTER_3","INVERTER_4")

checkdir = function(x)
{
  if(!file.exists(x)){
    dir.create(x)
  }
}

DOB = "08-09-2018"
DOB2="2018-09-08"
DOB2=as.Date(DOB2)
sender = c('operations@cleantechsolar.com')
uname = 'shravan.karthik@cleantechsolar.com'
# recipients =getRecipients("IN-047L","m")
recipients = c('operationsCentralIN@cleantechsolar.com','om-it-digest@cleantechsolar.com')
pwd = 'CTS&*(789'
referenceDays = NA
extractDaysOnly = function(days)
{
	if(length(days!=0)){
	seq = seq(from=1,to=length(days)*6,by=6)
	days = unlist(strsplit(days,"-"))
	days = paste(days[seq+3],days[seq+4],days[seq+5],sep="-")
	return(days)
	}
}
analyseDays = function(days,ref)
{
  if(length(days) == length(ref))
    return(days)
  daysret = unlist(rep(NA,length(ref)))
  if(length(days) == 0)
    return(daysret)
  days2 = extractDaysOnly(days)
  idxmtch = match(days2,ref)
  idxmtch2 = idxmtch[complete.cases(idxmtch)]
  if(length(idxmtch) != length(idxmtch2)){
    print(".................Missmatch..................")
    print(days2)
    print("#########")
    print(ref)
    print("............................................")
    return(days)
  }
  if(is.finite(idxmtch)){
    daysret[idxmtch] = as.character(days)
    if(!(is.na(daysret[length(daysret)])))
      return(daysret)
  }	
  return(days)
}
performBackWalkCheck = function(day)
{
  path = "/home/admin/Dropbox/Gen 1 Data/[IN-047L]"
  retval = 0
  yr = substr(day,1,4)
  yrmon = substr(day,1,7)
  pathprobe = paste(path,yr,yrmon,sep="/")
  stns = dir(pathprobe)
  print(stns)
  idxday = c()
  for(t in 1 : length(stns)){
    pathdays = paste(pathprobe,stns[t],sep="/")
    print(pathdays)
    days = dir(pathdays)
    dayMtch = days[grepl(day,days)]
    if(length(dayMtch)){
      idxday[t] = match(dayMtch,days)
    }
    print(paste("Match for",day,"is",idxday[t]))
  }
  idxmtch = match(NA,idxday[t])
  if(length(unique(idxday))==1 || length(idxmtch) < 5){
    print('All days present ')
    retval = 1
  }
  return(retval)
}
sendMail= function(pathall)
{
  path = pathall[t]
  dataread = read.table(path,header = T,sep="\t")
  currday = as.character(dataread[1,1])
  Cur=as.Date(currday)
  INSTCAP = 180.01
  filenams = c()
  body = ""
  body = paste(body,"Site Name: Fortuna D-16",sep="")
  body = paste(body,"\n\nLocation: Nashik, India")
  body = paste(body,"\n\nO&M Code: IN-047")
  body = paste(body,"\n\nSystem Size [kWp]:",INSTCAP)
  body = paste(body,"\n\nNumber of Energy Meters: 1")
  body = paste(body,"\n\nModule Brand / Model / Nos: REC / 310 & 315 W / 580")
  body = paste(body,"\n\nInverter Brand / Model / Nos: Huawei / 36 KTL / 4")
  body = paste(body,"\n\nSite COD: 2018-09-08")
  body = paste(body,"\n\nSystem age [days]:",as.character((as.numeric(Cur-DOB2))))
  body = paste(body,"\n\nSystem age [years]:",as.character(round((as.numeric(Cur-DOB2))/365,1)))
  bodyac = body
  body = ""
  TOTALGENCALC = 0
  # Reporting PR functionality start
  yr = substr(Cur,1,4)
  yrmon = substr(Cur,1,7)
  filename = paste("[IN-068L]-WMS1-",Cur,".txt",sep="")
  filename2= paste("[IN-047L]-MFM1-",Cur,".txt",sep="")
  path = "/home/admin/Dropbox/Second Gen/[IN-068L]"
  path2 = "/home/admin/Dropbox/Gen 1 Data/[IN-047L]"
  path3 = "/home/admin/Dropbox/Second Gen/[IN-047L]"
  pathRead = paste(path,yr,yrmon,"WMS_1",filename,sep="/")
  pathRead2 = paste(path3,yr,yrmon,"MFM_1",filename2,sep="/")
  print("Printing Pathread")
  print(pathRead)
  GTIGreater20=NA
  GTI = DNI = NA
  if(file.exists(pathRead))
  {
    dataread = read.table(pathRead,sep="\t",header = T)
    dataread2 = read.table(pathRead2,sep="\t",header = T)
    print(dataread2)
    if(nrow(dataread) > 0){
      GTI1 = as.numeric(dataread[1,3])
      RPR = as.numeric(dataread2[1,15])
      
      body = paste(body,"\n\n_____________________________________________\n",sep="")
      body = paste(body,"WMS")
      body  = paste(body,"\n_____________________________________________\n",sep="")
      body = paste(body,"\nIrradiation (From IN-068L, 7 km from IN-047L): ",GTI1)
      if(is.na(RPR)){
        body = paste(body,"\n\nReporting PR (From IN-068L):",'NA')
      }else if(RPR==85){
        body = paste(body,"\n\nReporting PR (From IN-068L):",RPR,'>>> PR Ceiling Activated!')
      }else if(RPR==70){
        body = paste(body,"\n\nReporting PR (From IN-068L):",RPR,">>> PR Floor Activated!")
      }else{
        body = paste(body,"\n\nReporting PR (From IN-068L):",RPR)
      }
      
    }
  }
  # Reporting PR functionality end
  MYLD = c()
  
  acname = METERNICKNAMES
  noInverters = NOINVS
  InvReadings = unlist(rep(NA,noInverters))
  InvAvail = unlist(rep(NA,noInverters))
  for(t in 1 : length(pathall)){
    type = 0
    meteridx = t
    if(grepl("WMS",pathall[t])) 
      type = 1
    else if(grepl("MFM",pathall[t]))
      type=1
    else
      type = 0
    if( t > (1+NOMETERS)){
      splitpath = unlist(strsplit(pathall[t],"INVERTER_"))
      if(length(splitpath)>1)
      {
        meteridx = unlist(strsplit(splitpath[2],"/"))
        meteridx = as.numeric(meteridx[1])
      }
    }
    path = pathall[t]
    dataread = read.table(path,header = T,sep="\t")
    currday = as.character(dataread[1,1])
    if(type==0){
      filenams[t] = paste(currday,"-",METERNICKNAMES[meteridx+2],".txt",sep="")
    }
    else if(type==1 && grepl("WMS",pathall[t])){
      filenams[t] = paste(currday,"-",'WMS',".txt",sep="")
    }else{
      filenams[t] = paste(currday,"-",'MFM',".txt",sep="") 
    }
    print("---------------------")
    print(path)
    print(filenams[t])
    print("---------------------")
    if(type == 1){
      body = paste(body,"\n\n________________________________________________\n\n")
      body = paste(body,currday,acname[t])
      body = paste(body,"\n________________________________________________\n\n")
      body = paste(body,"DA [%]:",as.character(dataread[1,2]),"\n\n")
      {
        if(grepl("MFM",pathall[t])){
          body = paste(body,"EAC method-1 (Pac) [kWh]:",as.character(format(round(dataread[1,3],1), nsmall = 1)),"\n\n")
          body = paste(body,"EAC method-2 (Eac) [kWh]:",as.character(format(round(dataread[1,4],1), nsmall = 1)),"\n\n")
          TOTALGENCALC = TOTALGENCALC + as.numeric(dataread[1,4])
          body = paste(body,"Yield-1 [kWh/kWp]:",as.character(format(round(dataread[1,5],2), nsmall = 2)),"\n\n")
          body = paste(body,"Yield-2 [kWh/kWp]:",as.character(format(round(dataread[1,6],2), nsmall = 2)),"\n\n")
          body = paste(body,"PR-1 (GHI) [%]:",as.character(format(round(dataread[1,7],1), nsmall = 1)),"\n\n")
          body = paste(body,"PR-2 (GHI) [%]:",as.character(format(round(dataread[1,8],1), nsmall = 1)),"\n\n")
          body = paste(body,"Last recorded value [kWh]:",as.character(format(round(dataread[1,9],1), nsmall = 1)),"\n\n")
          body = paste(body,"Last recorded time:",as.character(dataread[1,10]),"\n\n")
	        body = paste(body,"Grid Availability [%]:",as.character(dataread[1,13]),"\n\n")
        	body = paste(body,"Plant Availability [%]:",as.character(dataread[1,14]))
        }
        else
        {
	      	body = paste(body,"GHI [kWh/m^2]:",as.character(format(round(dataread[1,3],2), nsmall = 2)),"\n\n")
		      body = paste(body,"Avg Tmod [C]:",as.character(format(round(dataread[1,5],1), nsmall = 1)),"\n\n")
	      	body = paste(body,"Avg Tmod solar hours [C]:",as.character(format(round(dataread[1,7],1), nsmall = 1)))
        }
      }
      next
    }
    InvReadings[meteridx] = as.numeric(dataread[1,7])
    InvAvail[meteridx]= as.numeric(dataread[1,10])
    MYLD[(meteridx)] = as.numeric(dataread[1,7])
  }
  body = paste(body,"\n\n________________________________________________\n\n")
  body = paste(body,currday,"Inverters")
  body = paste(body,"\n________________________________________________")
  MYLDCPY = MYLD
  if(length(MYLD)){
    MYLD = MYLD[complete.cases(MYLD)]
  }
  addwarn = 0
  sddev = covar = NA
  if(length(MYLD)){
    addwarn = 1
    sddev = round(sdp(MYLD),2)
    meanyld = mean(MYLD)
    covar = round((sdp(MYLD)*100/meanyld),1)
  }
  MYLD = MYLDCPY
  for(t in 1 : noInverters){
    body = paste(body,"\n\n Yield ",acname[(t+1+NOMETERS)],": ",as.character(InvReadings[t]),sep="")
    if( addwarn && is.finite(covar) && covar > 5 &&
        (!is.finite(InvReadings[t]) || (abs(InvReadings[t] - meanyld) > 2*sddev))){
      body = paste(body,">>> Inverter outside range")
    }
  }
  body = paste(body,"\n\nStdev/COV Yields: ",sddev," / ",covar,"%",sep="")
  for(t in 1 : noInverters){
		body = paste(body,"\n\n Inverter Availability ",acname[(t+1+NOMETERS)]," [%]: ",as.character(InvAvail[t]),sep="")
	}
  body = gsub("\n ","\n",body)
  body = paste(bodyac,body,sep="")
  
    #Generating Graphs
  graph_command1 = paste('sudo python3 /home/admin/CODE/EmailGraphs/Inverter_CoV_Graph.py "IN-047"', currday, 2, '2018-09-08', '" "', '"Inverter CoV"', sep=" ")
  system(graph_command1)
  graph_command2 = paste('python3 /home/admin/CODE/EmailGraphs/Grid_Availability_Graph_Azure_COD.py "IN-047"', currday, '2018-09-08', sep=" ")
  system(graph_command2)
  graph_command3 = paste("Rscript /home/admin/CODE/EmailGraphs/PR_Graph_Azure.R", "IN-047", 72.3, 0.008, "2018-09-08", currday, sep=" ")
  system(graph_command3)
  
  #Graph Paths
  filestosendpath=character()
  graphs = c('PR Evolution', 'Inverter CoV', 'Grid Availability', 'Plant Availability', 'Data Availability', 'System Availability')
  for(g in 1 : 6){
    graph_path = paste('/home/admin/Graphs/Graph_Output/IN-047/[IN-047] Graph ', currday, ' - ', graphs[g], '.pdf', sep="")
    filestosendpath = c(filestosendpath, graph_path)
  }

  #Graph Extract Paths
  graph_extracts = c('PR Evolution', 'Inverter CoV', 'Master Extract Daily', 'Master Extract Monthly')
  for(g in 1 : 4){
    if(g<3){
      graph_extract_path = paste('/home/admin/Graphs/Graph_Extract/IN-047/[IN-047] Graph ', currday, ' - ', graph_extracts[g], '.txt', sep="")
    }else{
      graph_extract_path = paste('/home/admin/Graphs/Graph_Extract/IN-047/[IN-047]', ' - ', graph_extracts[g], '.txt', sep="")
    }
    filestosendpath = c(filestosendpath, graph_extract_path)
  }
  
  filestosendpath = c(filestosendpath, pathall)
  
  send.mail(from = sender,
            to = recipients,
            subject = paste("Station [IN-047L] Digest",currday),
            body = body,
            smtp = list(host.name = "smtp.office365.com", port = 587, user.name = uname, passwd = pwd, tls = TRUE),
            authenticate = TRUE,
            send = TRUE,
            attach.files = filestosendpath,
            debug = F)
  recordTimeMaster("IN-047L","Mail",currday)
}


path = "/home/admin/Dropbox/Gen 1 Data/[IN-047L]"
path2G = '/home/admin/Dropbox/Second Gen/[IN-047L]'
path3G = '/home/admin/Dropbox/Third Gen/[IN-047L]'
path4G = '/home/admin/Dropbox/Fourth_Gen/[IN-047L]'


if(RESETHISTORICAL){
  command = paste("rm -rf",path2G)
  system(command)
  command = paste("rm -rf",path3G)
  system(command)
  command = paste("rm -rf",path4G)
  system(command)
}

checkdir(path2G)
checkdir(path3G)

years = dir(path)
stnnickName2 = "IN-047L"
lastdatemail = lastMailDate(paste('/home/admin/Start/MasterMail/',stnnickName2,'_Mail.txt',sep=""))
stopDate = paste("[",stnnickName2,"]-I4-",lastdatemail,".txt",sep="")
print(paste('StopDate is',stopDate))
ENDCALL=0
for(x in 1 : length(years)){
  path2Gyr = paste(path2G,years[x],sep = "/")
  pathyr = paste(path,years[x],sep="/")
  checkdir(path2Gyr)
  months = dir(pathyr)
  for(y in 1 : length(months)){
    path2Gmon = paste(path2Gyr,months[y],sep = "/")
    pathmon = paste(pathyr,months[y],sep="/")
    checkdir(path2Gmon)
    stns = dir(pathmon)
    stns = stns[reorderStnPaths]
    dunmun = 0
    for(t in 1 : length(stns)){
      type = 1
      if(grepl("MFM_1",stns[t]))
        type = 0
      if(grepl("WMS_1",stns[t]))
        type = 2
      pathmon1 = paste(pathmon,stns[t],sep="/")
      days = dir(pathmon1)
      path2Gmon1 = paste(path2Gmon,stns[t],sep = "/")
      if(!file.exists(path2Gmon1)){	
        dir.create(path2Gmon1)
      }
      if(length(days)>0){
        for(z in 1 : length(days))
        {
          if(ENDCALL == 1)
            break
          if((z==length(days)) && (y == length(months)) && (x ==length(years)))
            next
          print(days[z])
          pathfinal = paste(pathmon1,days[z],sep = "/")
          path2Gfinal = paste(path2Gmon1,days[z],sep="/")
          if(RESETHISTORICAL){
            secondGenData(pathfinal,path2Gfinal,type)
          }
          if(!dunmun){
            daysAlive = daysAlive+1
          }
          if(days[z] == stopDate){
            ENDCALL = 1
            print('Hit end call')
            next
          }
        }
      }
      dunmun = 1
      if(ENDCALL == 1)
        break
    }
    if(ENDCALL == 1)
      break
  }
  if(ENDCALL == 1)
    break
}

print('Backlog done')

prevx = x
prevy = y
prevz = z
repeats = 0
SENDMAILTRIGGER = 0
newlen=1
while(1){
  # recipients = getRecipients("IN-047L","m")
  recipients = c('operationsCentralIN@cleantechsolar.com','om-it-digest@cleantechsolar.com')
  recordTimeMaster("IN-047L","Bot")
  years = dir(path)
  noyrs = length(years)
  path2Gfinalall = vector('list')
  for(x in prevx : noyrs){
    pathyr = paste(path,years[x],sep="/")
    path2Gyr = paste(path2G,years[x],sep="/")
    checkdir(path2Gyr)
    mons = dir(pathyr)
    nomons = length(mons)
    startmn = prevy
    endmn = nomons
    if(startmn>endmn){
      startmn = 1
      prevx = x-1
      prevz = 1
    }
    for(y in startmn:endmn){
      pathmon = paste(pathyr,mons[y],sep="/")
      path2Gmon = paste(path2Gyr,mons[y],sep="/")
      checkdir(path2Gmon)
      stns = dir(pathmon)
      if(length(stns) < 6){
        print('Station sync issue.. sleeping for an hour')
        Sys.sleep(3600) # Sync issue, meter data comes after MFM and WMS
        stns = dir(pathmon)
      }
      stns = stns[reorderStnPaths]
      for(t in 1 : length(stns)){
        newlen = (y-startmn+1) + (x-prevx)
        print(paste('Reset newlen to',newlen))
        type = 1
        if(stns[t] == "MFM_1")
          type = 0
        if(stns[t] == "WMS_1")
          type = 2
        pathmon1 = paste(pathmon,stns[t],sep="/")
        days = dir(pathmon1)
        path2Gmon1 = paste(path2Gmon,stns[t],sep = "/")
        chkcopydays = days[grepl('Copy',days)]
        if(!file.exists(path2Gmon1)){
          dir.create(path2Gmon1)
        }
        if(length(chkcopydays) > 0){
          print('Copy file found they are')
          print(chkcopydays)
          idxflse = match(chkcopydays,days)
          print(paste('idx matches are'),idxflse)
          for(innerin in 1 : length(idxflse)){
            command = paste("rm '",pathmon,"/",days[idxflse[innerin]],"'",sep="")
            print(paste('Calling command',command))
            system(command)
          }
          days = days[-idxflse]
        }
        days = days[complete.cases(days)]
        if(length(days)>0){
          if(t==1){
            referenceDays <<- extractDaysOnly(days)
        }else{
          days = analyseDays(days,referenceDays)}
        }
        nodays = length(days)
        if(y > startmn){
          z = prevz = 1
        }
        if(nodays > 0){
          startz = prevz
          if(startz > nodays)
            startz = nodays
          for(z in startz : nodays){
            if(t == 1){
              path2Gfinalall[[newlen]] = vector('list')
            }
            if(is.na(days[z]))
              next
            pathdays = paste(pathmon1,days[z],sep = "/")
            path2Gfinal = paste(path2Gmon1,days[z],sep="/")
            secondGenData(pathdays,path2Gfinal,type)
            if((z == nodays) && (y == endmn) && (x == noyrs)){
              if(!repeats){
                print('No new data')
                repeats = 1
              }
              next
            }
            if(is.na(days[z])){
              print('Day was NA, do next in loop')
              next
            }
            SENDMAILTRIGGER <<- 1
            if(t == 1){
              splitstr =unlist(strsplit(days[z],"-"))
              daysSend = paste(splitstr[4],splitstr[5],splitstr[6],sep="-")
              print(paste("Sending",daysSend))
              if(performBackWalkCheck(daysSend) == 0){
                print("Sync issue, sleeping for an 1 hour")
                Sys.sleep(3600) # Sync issue -- meter data comes a little later than
                # MFM and WMS
              }
            }
            repeats = 0
            print(paste('New data, calculating digests',days[z]))
            path2Gfinalall[[newlen]][t] = paste(path2Gmon1,days[z],sep="/")
            newlen = newlen + 1
            print(paste('Incremented newlen by 1 to',newlen))
          }
        }
      }
    }
  }
  if(SENDMAILTRIGGER){
    print('Sending mail')
    print(paste('newlen is ',newlen))
    for(lenPaths in 1 : (newlen - 1)){
      if(lenPaths < 1)
        next
      print(unlist(path2Gfinalall[[lenPaths]]))
      pathsend = unlist(path2Gfinalall[[lenPaths]])
      daysAlive = daysAlive+1
      sendMail(pathsend)
    }
    SENDMAILTRIGGER <<- 0
    newlen = 1
    print(paste('Reset newlen to',newlen))
    
  }
  
  prevx = x
  prevy = y
  prevz = z
  Sys.sleep(300)
}
print(paste('Exited for some reason x y z values are'),x,y,z)
sink()

