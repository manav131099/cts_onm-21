rm(list = ls())
TIMESTAMPSALARM = NULL
TIMESTAMPSALARM2 = NULL
TIMESTAMPSALARM3 = NULL
METERCALLED = 0
ltcutoff = .001
CABLOSSTOPRINT = 0
instcaps = c(236.94,307.56,62.7)
path4G = "/home/admin/Dropbox/Fourth_Gen/[MY-003X]/[MY-003X]-lifetime.txt"
RESET4G = 1
timetomins = function(x){
	hr = unlist(strsplit(x,":"))
	seq1 = seq(from = 1,to = length(hr),by =2)
	seq2 = seq(from = 2,to = length(hr),by =2)
  	min = as.numeric(hr[seq2]) 
  	hr = as.numeric(hr[seq1]) * 60
  	return((hr + min + 1))
}
checkdir = function(x){
  if(!file.exists(x)){
    dir.create(x)
  }
}
fetchGSIData = function(date){
	pathMain = '/home/admin/Dropbox/Second Gen/[MY-002L]'
	yr = substr(date,1,4)
	mon = substr(date,1,7)
	txtFileName = paste('[MY-002L]-WMS1-',date,".txt",sep="")
	pathyr = paste(pathMain,yr,sep="/")
	gsiVal = NA
	if(file.exists(pathyr)){
		pathmon = paste(pathyr,mon,sep="/")
		if(file.exists(pathmon)){
			pathfile = paste(pathmon,"WMS_1_Reference Cell",txtFileName,sep="/")
			if(file.exists(pathfile)){
				dataread = read.table(pathfile,sep="\t",header = T,fill=TRUE)
				gsiVal = as.numeric(dataread[,3])
			}
		}
	}
	return(gsiVal)
}
secondGenData = function(filepath,writefilepath){
  	if(METERCALLED == 1){
		TIMESTAMPSALARM <<- NULL}
	else if(METERCALLED == 2){
		TIMESTAMPSALARM2 <<- NULL}
	else if(METERCALLED == 3)
	{
		TIMESTAMPALARM3 <<- NULL
	}
	bypass=0
	print(paste('IN 2G',filepath))
  	dataread = try(read.table(filepath,header = T,sep = "\t",stringsAsFactors=F),silent=T)
	if(class(dataread)=="try-error")
		bypass=1
	date = DA=Eac1=Eac2=gtiNI=PR1=PR2=lastread=lasttime=DSPY1=totrowsmissing=DSPY2=RATIO=NA
	dataread2 = dataread
	if(!bypass){
		lasttime=lastread=Eac2=NA
		coleac1 = 19
		coleac2 = 46
		#	if(METERCALLED == 3)
		#		coleac2 = 12
		if(ncol(dataread) < 50){
			coleac1 = 15
			coleac2 = 39
		}
		dataread = dataread2[complete.cases(as.numeric(dataread2[,coleac2])),]
		if(nrow(dataread)>0){
			lasttime = as.character(dataread[nrow(dataread),1])
			lastread = as.character(round(as.numeric(dataread[nrow(dataread),coleac2])/1000,3))
			Eac2 = format(round((as.numeric(dataread[nrow(dataread),coleac2]) - as.numeric(dataread[1,coleac2]))/1000,2),nsmall=1)
		}
		dataread = dataread2[complete.cases(as.numeric(dataread2[,coleac1])),]
		if(METERCALLED!=1)
			dataread = dataread[as.numeric(dataread[,coleac1]) > 0,]
		Eac1 = RATIO=NA
		if(nrow(dataread) > 1){
		Eac1 = format(abs(round(sum(as.numeric(dataread[,coleac1]))/60000,1)),nsmall=2)
			RATIO = round(as.numeric(Eac1)*100/as.numeric(Eac2),1)
		}
		dataread = dataread2
		DA = format(round(nrow(dataread)/14.4,1),nsmall=1)
		tdx = timetomins(substr(dataread[,1],12,16))
		dataread2 = dataread[tdx > 540,]
		tdx = tdx[tdx > 540]
		dataread2 = dataread2[tdx < 1020,]
		dataread2 = dataread2[complete.cases(as.numeric(dataread2[,coleac1])),]
		missingfactor = 480 - nrow(dataread2)
		{
			if(METERCALLED==1)
			dataread2 = dataread2[abs(as.numeric(dataread2[,coleac1])) < ltcutoff,]
			else
			dataread2 = dataread2[abs(as.numeric(dataread2[,coleac1])) < ltcutoff,]
		}
		if(length(dataread2[,1]) > 0){
			{
				if(METERCALLED == 1){
					TIMESTAMPSALARM <<- as.character(dataread2[,1])}
				else if(METERCALLED == 2){
				TIMESTAMPSALARM2 <<- as.character(dataread2[,1])}
				else if(METERCALLED == 3){
				TIMESTAMPSALARM3 <<- as.character(dataread2[,1])}
			}
		}
		totrowsmissing = format(round((missingfactor + nrow(dataread2))/4.8,1),nsmall=1)
		date = NA
		if(nrow(dataread)>0)
			date = substr(as.character(dataread[1,1]),1,10)
		gtiNI = fetchGSIData(date)
		print(paste('Date',date))
		print(paste('EAC1',Eac1))
		print(paste('Eac2',Eac2))
		print(paste('RATIO',RATIO))
		print(paste('DA',DA))
		print(paste('Downtime',totrowsmissing))
		print(paste('Lasttime',lasttime))
		print(paste('LastRead',lastread))
		DSPY1 = round((as.numeric(Eac1)/instcaps[METERCALLED]),2)
		DSPY2 = round((as.numeric(Eac2)/instcaps[METERCALLED]),2)
		PR1 = round(100*DSPY1/gtiNI,1)
		PR2 = round(100*DSPY2/gtiNI,1)
		RPR=PR2
		print('######')
		print(PR2)
		if(!is.na(PR2)){
			if(PR2>85){
				RPR=85
			}else if(PR2<70){
				RPR=70
		}
		}else{
			RPR='NA'
		}
	}
	if(METERCALLED == 1){
      	df = data.frame(Date = date, SolarPowerMeth1 = as.numeric(Eac1),SolarPowerMeth2 = as.numeric(Eac2),Ratio=RATIO,DA = DA,Downtime = totrowsmissing,LastTime = lasttime, LastRead = lastread,Yield1 = DSPY1, Yield2 = DSPY2,GTIMY002S=gtiNI,PR1=PR1,PR2=PR2,RPR=RPR,stringsAsFactors = F)
    }else if(METERCALLED == 2){
      	df = data.frame(Date = date, SolarPowerMeth1 = as.numeric(Eac1),SolarPowerMeth2= as.numeric(Eac2),Ratio=RATIO,DA = DA,Downtime = totrowsmissing,LastTime = lasttime, LastRead = lastread,Yield1 = DSPY1,Yield2 = DSPY2,GTIMY002S=gtiNI,PR1=PR1,PR2=PR2,RPR=RPR,stringsAsFactors = F)
	}else if(METERCALLED == 3){
			#DailyCons = dataread[,ncol(dataread)]
			#DailyCons = DailyCons[complete.cases(DailyCons)]
			#DailyCons = round(sum(DailyCons)/60000,2)
      	df = data.frame(Date = date, SolaPowerMeth1 = as.numeric(Eac1),CokePowerMeth2= as.numeric(Eac2),
                  Ratio=RATIO,DA = DA,Downtime = totrowsmissing,
									LastTime = lasttime, LastRead = lastread,Yield1 = DSPY1, Yield2 = DSPY2,
									GTIMY002S=gtiNI,PR1=PR1,PR2=PR2,RPR=RPR,
									#DailyCons = DailyCons,
									stringsAsFactors = F)
	}
	write.table(df,file = writefilepath,row.names = F,col.names = T,sep="\t",append = F)
  	return(df)
}

thirdGenData = function(filepathm1, filepathm2,filepathm3,writefilepath,tmPush){
  	dataread2 =read.table(filepathm1,header = T,sep="\t",stringsAsFactors=F,fill=TRUE) #its correct dont change
  	dataread1 = read.table(filepathm2,header = T,sep = "\t",stringsAsFactors=F,fill=TRUE) #its correct dont change
  	if(!(is.null(filepathm3)))
		dataread3 = read.table(filepathm3,header = T,sep = "\t",stringsAsFactors=F,fill=TRUE) #its correct dont change
	dt = as.character(dataread1[,6])
	da = as.character(dataread1[,5])
	EacCokeMeth1 = as.numeric(dataread2[,2])
	EacCokeMeth2 = as.numeric(dataread2[,3])
	CokeLoadMeth1=ArtificialLoad=CokeLoadMeth2=PercentageArtSolar=RatioCokeLoad=NA
	ltCokeLoad=lrCokeLoad=DailyCons=NA
	Irr=NA
  	if(!(is.null(filepathm3))){
		CokeLoadMeth1 = as.numeric(dataread3[,2])
		CokeLoadMeth2 = as.numeric(dataread3[,3])
		Irr=as.numeric(dataread3[,11])
		#ArtificialLoad = CokeLoadMeth1 + EacCokeMeth1
		#PercentageArtSolar = round(((EacCokeMeth1*100) / (ArtificialLoad)),2)
	}
	SolarEnergyMeth1 = as.numeric(dataread1[,2])
	SolarEnergyMeth2 = as.numeric(dataread1[,3])
  	RatioCoke = as.numeric(dataread2[,4])
	if(!is.finite(RatioCoke))
		RatioCoke = NA
  	RatioSolar = as.numeric(dataread1[,4])
  	if(!(is.null(filepathm3)))
		RatioCokeLoad = as.numeric(dataread3[,4])
	CABLOSS= round((EacCokeMeth2/SolarEnergyMeth2),3)
	CABLOSS = abs(1 - CABLOSS)*100
	CABLOSSTOPRINT <<- format(CABLOSS,nsmall=2)
	ltSol = as.character(dataread1[,7])
	lrSol = as.character(dataread1[,8])
	ltCoke = as.character(dataread2[,7])
	lrCoke = as.character(dataread2[,8])
  	if(!(is.null(filepathm3))){
		ltCokeLoad = as.character(dataread3[,7])
		lrCokeLoad = as.character(dataread3[,8])
		#DailyCons = as.character(dataread3[,9])
  	}
	dateac = substr(as.character(dataread1[1,1]),1,10)
	df = data.frame(Date = dateac,
	DA= da,
	Downtime = dt,
	Eac1GF1 = EacCokeMeth1,
	Eac2GF2 = EacCokeMeth2,
	Eac1GF2=SolarEnergyMeth1,
	Eac2GF2=SolarEnergyMeth2,
	EacRatioGF1=RatioCoke,
	EacRatioGF2=RatioSolar,
	CableLoss=CABLOSS,
	LastTimeGF2=ltSol,
	LastReadGF2=lrSol,
	LastTimeGF1=ltCoke,
	LastReadGF1=lrCoke,
	Eac1GF3=CokeLoadMeth1,
	Eac2GF3=CokeLoadMeth2,
	EacRatioGF3=RatioCokeLoad,
	LastTimeGF3=ltCokeLoad,
	LastReadGF3=lrCokeLoad,
	Irr=Irr,
	#ArtificialLoad=ArtificialLoad,
	#PercentageSolar=PercentageArtSolar,
	#DailyCons = DailyCons,
	stringsAsFactors=F)
  
    if(!file.exists(writefilepath)){
      write.table(df,file = writefilepath,row.names = F,col.names = T,sep = "\t",append = T)
    }else{
  		dataread2 = read.table(writefilepath,header = T,sep = "\t",stringsAsFactors=F,fill=TRUE)
		dateall = as.character(dataread2[,1])
		idxmtch = match(dateac,dateall)
			
		if(!is.finite(idxmtch)){
			write.table(df,file = writefilepath,row.names = F,col.names = F,sep = "\t",append = T)
		}else{
			dataread2[idxmtch,] = df[1,]
			write.table(dataread2,file = writefilepath,row.names = F,col.names = T,sep = "\t",append = F)
		}
    }
  
	fourthGenData(writefilepath,tmPush)
}

fourthGenData = function(path,tmPush){
	data = read.table(path,header=T,sep="\t",stringsAsFactors=F,fill=TRUE)
	data = data[nrow(data),]
	dateac = as.character(data[,1])
	colnamesuse = c(colnames(data),"TimeUpdated","StnID")
	if(is.na(tmPush))
		tmPush = as.character(format(Sys.time(),tz = "Singapore"))
	data[1,(ncol(data)+1)] = tmPush
	data[1,(ncol(data)+1)] = "MY-003X"
	colnames(data) = colnamesuse
	
	if(RESET4G){
		write.table(data,file=path4G,sep="\t",row.names=F,col.names=T,append=F)
		RESET4G <<- 0
	}else{
		dataread2 = read.table(path4G,header = T,sep = "\t",stringsAsFactors=F,fill=TRUE)
		dateall = as.character(dataread2[,1])
		idxmtch = match(dateac,dateall)
		print(paste(idxmtch,"is idxmtch"))
		if(!is.finite(idxmtch)){
			write.table(data,file = path4G,row.names = F,col.names = F,sep = "\t",append = T)
		}else{
			dataread2[idxmtch,] = data[1,]
			write.table(dataread2,file = path4G,row.names = F,col.names = T,sep = "\t",append = F)
		}
		
	}
	
}
