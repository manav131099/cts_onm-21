system('rm -R "/home/admin/Dropbox/Second Gen/[KH-002X]"')
system('rm -R "/home/admin/Dropbox/Third Gen/[KH-002X]"')

require('mailR')
require('Hmisc')

source('/home/admin/CODE/KH002Digest/KH002MailDigestFunctions.R')
source('/home/admin/CODE/Misc/calcBackLog.R')
path = '/home/admin/Dropbox/Gen 1 Data/[KH-002X]'
writepath2G = '/home/admin/Dropbox/Second Gen/[KH-002X]'
irrpath = '/home/admin/Dropbox/Cleantechsolar/1min/[714]'
checkdir(writepath2G)

writepath3G = '/home/admin/Dropbox/Third Gen/[KH-002X]'
checkdir(writepath3G)
DAYSALIVE = 0
years = dir(path)
stnnickName2 = "KH-002X"
lastdatemail = lastMailDate(paste('/home/admin/Start/MasterMail/',stnnickName2,'_Mail.txt',sep=""))
stopDate = paste("[",stnnickName2,"-M1] ",lastdatemail,".txt",sep="")
ENDCALL=0
for(x in 1 : length(years))
{
  pathyrs = paste(path,years[x],sep="/")
	irrpathyears = paste(irrpath,years[x],sep="/")
  writepath2Gyr = paste(writepath2G,years[x],sep="/")
  checkdir(writepath2Gyr)
  writepath3Gyr = paste(writepath3G,years[x],sep="/")
  checkdir(writepath3Gyr)
  months = dir(pathyrs)
  for(y in 1 : length(months))
  {
    pathmonths = paste(pathyrs,months[y],sep="/")
    writepath2Gmon = paste(writepath2Gyr,months[y],sep="/")
		irrpathmonth = paste(irrpathyears,months[y],sep="/")
		daysirr = dir(irrpathmonth)
    checkdir(writepath2Gmon)
    writepath3Gfinal = paste(writepath3Gyr,"/[KH-002X] ",months[y],".txt",sep="")
    stations = dir(pathmonths)
    pathdays = paste(pathmonths,stations[1],sep="/")
    pathdays2 = paste(pathmonths,stations[2],sep="/")
    writepath2Gdays = paste(writepath2Gmon,stations[1],sep="/")
    writepath2Gdays2 = paste(writepath2Gmon,stations[2],sep="/")
    checkdir(writepath2Gdays)
    checkdir(writepath2Gdays2)
    days = dir(pathdays)
    temp = unlist(strsplit(days[1]," "))
    temp = as.Date(substr(temp[2],1,10),"%Y-%m-%d")
    days2 = dir(pathdays2)
      for(t in 1 : length(days2))
      {
        if(ENDCALL==1)
					break
				{
          if(x == 1 && y == 1 && t == 1)
          {
            DOB = unlist(strsplit(days[t]," "))
            DOB = substr(DOB[2],1,10)
            DOB = as.Date(DOB,"%Y-%m-%d")
          }
          else if(x == length(years) && y == length(months) && t == length(days))
          {
            next
          }
        }
        DAYSALIVE = DAYSALIVE + 1
				t2 = t
				if(t > length(days))
					t2 = length(days)
        writepath2Gfinal = paste(writepath2Gdays,"/",days[t2],sep="")
        writepath2Gfinal2 = paste(writepath2Gdays2,"/",days2[t],sep="")
        readpath = paste(pathdays,days[t2],sep="/")
        readpath2 = paste(pathdays2,days2[t],sep="/")
				METERCALLED <<- 1 # correct dont change
        df2 = secondGenData(readpath2,writepath2Gfinal2)
				METERCALLED <<- 2 #correct dont change
        df1 = secondGenData(readpath,writepath2Gfinal)
        thirdGenData(writepath2Gfinal,writepath2Gfinal2,writepath3Gfinal)
				if(days2[t] == stopDate)
				{
					ENDCALL = 1
				}
      }
			if(ENDCALL==1)
				break
    }
		if(ENDCALL==1)
			break
}
print('Historical Analysis Done')
