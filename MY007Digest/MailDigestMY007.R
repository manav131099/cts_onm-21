errHandle = file('/home/admin/Logs/LogsMY007Mail.txt',open='w',encoding='UTF-8')
sink(errHandle,type='message',append = T)
sink(errHandle,type='output',append = T)

source('/home/admin/CODE/MY007Digest/HistoricalAnalysis2G3GMY007.R')
source('/home/admin/CODE/MasterMail/timestamp.R')
source('/home/admin/CODE/Send_mail/sendmail.R')
source('/home/admin/CODE/MY007Digest/aggregateInfo.R')
initDigest = function(df,extra)
{
  body = "\n\n______________________________________________\n\n"
  body = paste(body,as.character(df[,1]))
  body = paste(body,"\n\n______________________________________________\n\n")
	body = paste(body,"Eac","-1 [kWh]: ",as.character(df[,2]),sep="")
  body = paste(body,"\n\nEac","-2 [kWh]: ",as.character(df[,3]),sep="")
  body = paste(body,"\n\nTotal daily irradiation [from MY-006, kWh/m2]:",as.character(df[,12]))
	body = paste(body,"\n\nDaily specific yield-1 [kWh/kWp]: ",df[,6],sep="")
	body = paste(body,"\n\nDaily specific yield-2 [kWh/kWp]: ",df[,9],sep="")
	body = paste(body,"\n\nPR-1 [%]: ",df[,10],sep="")
	body = paste(body,"\n\nPR-2 [%]: ",df[,11],sep="")
	body = paste(body,"\n\nRatio EAC","-2/EAC","-1: ",round((as.numeric(df[,3])/as.numeric(df[,2]))*100,2),sep="")
  acpts = round(as.numeric(df[,4]) * 14.4)
  body = paste(body,"\n\nPoints recorded: ",acpts," (",as.character(df[,4]),"%)",sep="")
  body = paste(body,"\n\nDowntime (%):",as.character(df[,5]))
  body = paste(body,"\n\nLast recorded timestamp:",as.character(df[,7]))
  body = paste(body,"\n\nLast recorded energy meter reading [kWh]:",as.character(df[,8]))
  return(body)
}
printtsfaults = function(TS,body)
{
	if(length(TS) > 1)
	{
		body = paste(body,"\n\n______________________________________________\n")
		body = paste(body,paste("\nTimestamps","where abs(Pac) < 1 between 8am - 6pm\n"))
		for(lm in  1 : length(TS))
		{
			body = paste(body,TS[lm],sep="\n")
		}
	}
	return(body)
}
sendMail = function(df1,pth1,pth3,extra)
{
  filetosendpath = pth3
  if(file.exists(pth3))
	{
  filetosendpath = c(pth1,pth3)
  }
	filenams = c()
  for(l in 1 : length(filetosendpath))
  {
    temp = unlist(strsplit(filetosendpath[l],"/"))
    filenams[l] = temp[length(temp)]
		if(l == 1)
			currday = temp[length(temp)]
  }
	print('Filenames Processed')
	body = "Site Name: Safran Nilai"
	body = paste(body,"\n\n Location: Safran Landing System Malaysia Sdn. Bhd")
	body = paste(body,"\n\n O&M Code: MY-007")
	body = paste(body,"\n\n System Size: 345.8 kWp")
	body = paste(body,"\n\n Number of Energy Meters: 1")
	body = paste(body,"\n\n Module Brand / Model / Nos: JA Solar / JAM72S01-380-PR / 910")
	body = paste(body,"\n\n Inverter Brand / Model / Nos: SMA / Solid Q-50 / 6")
	body = paste(body,"\n\n Site COD: 2019-06-01")
    body = paste(body,"\n\nSystem age [days]: ",as.character((as.numeric(DAYSALIVE))),sep="")
	body = paste(body,"\n\nSystem age [years]: ",as.character(round((as.numeric(DAYSALIVE))/365,2)),sep="")
  body = paste(body,initDigest(df1,extra))
  body = printtsfaults(TIMESTAMPSALARM,body)
	print('2G data processed')
  body = paste(body,"\n\n______________________________________________\n\n")
  body = paste(body,"Station History")
  body = paste(body,"\n\n______________________________________________\n\n")
  body = paste(body,"Station DOB:",as.character(DOB))
  body = paste(body,"\n\n# Days alive:",as.character(DAYSALIVE))
  yrsalive = format(round((DAYSALIVE/365),2),nsmall=2)
  body = paste(body,"\n\n# Years alive:",yrsalive)
  body = paste(body,"\n\n# Eac1 total this month [kWh]:",MONTHTOTEAC1)
  body = paste(body,"\n\n# Eac2 total this month [kWh]:",MONTHTOTEAC2)
  proje1 = format(round((MONTHTOTEAC1 * DAYSTHISMONTHAC / DAYSTHISMONTH),1),nsmall=1)
  proje2 = format(round((MONTHTOTEAC2 * DAYSTHISMONTHAC / DAYSTHISMONTH),1),nsmall=1)
  body = paste(body,"\n\n # Eac1 projected total for the month [kWh]:",proje1)
  body = paste(body,"\n\n # Eac2 projected total for the month [kWh]:",proje2)
	if(file.exists(pth3))
	{
	df = read.table(pth3,header =T,sep="\t")
	avge1 = format(round(mean(as.numeric(df[,2])),1),nsmall=1)
  avge2 = format(round(mean(as.numeric(df[,3])),1),nsmall=1)
	body = paste(body,"\n\n # Eac1 average for the month [kWh]:",avge1)
  body = paste(body,"\n\n # Eac2 average for the month [kWh]:",avge2)
  }
	body = paste(body,"\n\n # EAC1 sum last 30 days [kWh]:",sum(LAST30DAYSEAC1))
  body = paste(body,"\n\n # EAC2 sum last 30 days [kWh]:",sum(LAST30DAYSEAC2))
  body = paste(body,"\n\n # EAC1 average last 30 days [kWh]:",round(mean(LAST30DAYSEAC1),1))
  body = paste(body,"\n\n # EAC2 average last 30 days [kWh]:",round(mean(LAST30DAYSEAC2),1))
  body = paste(body,"\n\n EAC1/EAC2 lifetime ratio :",round((EAC1GLOB/EAC2GLOB),4))
	print('3G data processed')
	body = gsub("\n ","\n",body)
  send.mail(from = sender,
            to = recipients,
            subject = paste("Station [MY-007X] Digest",substr(currday,11,20)),
            body = body,
            smtp = list(host.name = "smtp.office365.com", port = 587, user.name = uname, passwd = pwd, tls = TRUE),
            authenticate = TRUE,
            send = TRUE,
            attach.files = filetosendpath,
            file.names = filenams, # optional parameter
            debug = F)
	recordTimeMaster("MY-007X","Mail",substr(currday,11,20))
}
sender = c('operations@cleantechsolar.com')
uname = 'shravan.karthik@cleantechsolar.com'
# recipients = getRecipients("MY-007X","m")
recipients = c('operationsMY@cleantechsolar.com','om-it-digest@cleantechsolar.com','om-interns@cleantechsolar.com','solarmy@cutechgroup.com')

pwd = 'CTS&*(789'
todisp = 1
needsleep = 0
while(1)
{
	# recipients = getRecipients("MY-007X","m")
  recipients = c('operationsMY@cleantechsolar.com','om-it-digest@cleantechsolar.com','om-interns@cleantechsolar.com','solarmy@cutechgroup.com')
  recordTimeMaster("MY-007X","Bot")
  sendmail = 0
  prevx = x
  prevy = y
  prevt = t
  years = dir(path)
  for(x in prevx : length(years))
  {
    pathyear = paste(path,years[x],sep="/")
    writepath2Gyr = paste(writepath2G,years[x],sep="/")
    writepath3Gyr = paste(writepath3G,years[x],sep="/")
    checkdir(writepath2Gyr)
    checkdir(writepath3Gyr)
    months = dir(pathyear)
    startmnth = 1
    if(prevx == x)
    {
      startmnth = prevy
    }
    for(y in startmnth : length(months))
    {
      pathdays = paste(pathyear,months[y],sep="/")
      writepath2Gdays = paste(writepath2Gyr,months[y],sep="/")
      writepath3Gfinal = paste(writepath3Gyr,"/[MY-007X] ",months[y],".txt",sep="")
      checkdir(writepath2Gdays)
      days = dir(pathdays)
      startdays = 1
      if(y == prevy)
      {
        startdays = prevt
      }
      if(startdays == 1)
      {
        MONTHTOTEAC1 = 0
        MONTHTOTEAC2 = 0
        DAYSTHISMONTH = 0
        temp = unlist(strsplit(days[1]," "))
				print(substr(temp[2],1,10))
        temp = as.Date(substr(temp[2],1,10),"%Y-%m-%d")
        DAYSTHISMONTHAC = monthDays(temp)
      }
      for(t in startdays : length(days))
      {
         condn1  = (t != length(days))
         condn2 = ((t == length(days)) && ((y != length(months)) || (x != length(years))))
         if(!(condn1 || condn2))
         {
				 	 if(todisp)
					 {
					 	print('No new files')
						todisp = 0
					 }
           Sys.sleep(3600)
           next
         }
				 print(paste('Processing',days[t]))
				 if(grepl("Copy",days[t]))
				 {
				 	print('Copy file found so removing it')
					command = paste('rm "',pathdays,'/',days[t],'"',sep="")
					print(command)
					system(command)
					print('Command complete.. sleeping')
				 	Sys.sleep(3600)
					next
				 }
				 todisp = 1
         sendmail = 1
         DAYSALIVE = DAYSALIVE + 1
         writepath2Gfinal = paste(writepath2Gdays,"/",days[t],sep="")
         readpath = paste(pathdays,days[t],sep="/")
				 lastdata = read.table(readpath,header = T,sep = "\t")
				 lastdata = c(as.character(lastdata[nrow(lastdata),1]),as.character(as.numeric(lastdata[nrow(lastdata),46])/1000))
         Sys.sleep(60)
				 df1 = secondGenData(readpath,writepath2Gfinal,needsleep)
         tots = thirdGenData(writepath2Gfinal,writepath3Gfinal)
				 if(!is.finite(tots[1])) 
				 {
					 tots[1]=0
				 }
				 if(!is.finite(tots[2]))
				 {
			 		 tots[2]=0
			 	 }
        LAST30DAYSEAC1[[idxvec]] = tots[1]
        LAST30DAYSEAC2[[idxvec]] = tots[2]

         LAST30DAYSEAC1[[idxvec]] = tots[1]
         LAST30DAYSEAC2[[idxvec]] = tots[2]
         MONTHTOTEAC1 = MONTHTOTEAC1 + tots[1]
         MONTHTOTEAC2 = MONTHTOTEAC2 + tots[2]
         DAYSTHISMONTH = DAYSTHISMONTH + 1
         EAC1GLOB = EAC1GLOB + tots[1]
         EAC2GLOB = EAC2GLOB + tots[2]
         idxvec = (idxvec + 1) %% 30
         if(idxvec == 0)
         {
           idxvec = 1
         }
  if(sendmail ==0)
  {
    next
  }
	print('Sending Mail')
  sendMail(df1,writepath2Gfinal,writepath3Gfinal,lastdata)
	print('Mail Sent')

      }
    }
  }
	needsleep = 1
	gc()
}
sink()
