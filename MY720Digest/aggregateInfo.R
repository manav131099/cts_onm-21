source('/home/admin/CODE/common/aggregate.R')

registerMeterList("MY-720S",c(""))
aggNameTemplate = getNameTemplate()
aggColTemplate = getColumnTemplate()

aggColTemplate[1] = 1 #Column no for date
aggColTemplate[2] = 31 #Column no for DA
aggColTemplate[3] = NA #column for LastRead
aggColTemplate[4] = NA #column for LastTime
aggColTemplate[5] = NA #column for Eac-1
aggColTemplate[6] = NA #column for Eac-2
aggColTemplate[7] = NA #column for Yld-1
aggColTemplate[8] = NA #column for Yld-2
aggColTemplate[9] = NA #column for PR-1
aggColTemplate[10] = NA #column for PR-2
aggColTemplate[11] = 3 #column for Irr
aggColTemplate[12] = "Self" # IrrSrc Value
aggColTemplate[13] = 6 #column for Tamb
aggColTemplate[14] = 18 #column for Tmod
aggColTemplate[15] = 12 #column for Hamb

registerColumnList("MY-720S","",aggNameTemplate,aggColTemplate)
