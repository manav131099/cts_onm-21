import urllib.request, json
import csv,time
import datetime
import collections
import os
import pytz 
import smtplib
import ssl

ssl._create_default_https_context = ssl._create_unverified_context

#global declarations
ihead = ['i1','i2','i3','i4','i5','i6','i7','i8','i9','i10','i11','i12','i13','i14','i15','i16','i17','i18','i19',
'i20','i21','i22','i23','i24','i25','i26','i27','i28','i29','i30','i31','i32','i33','i34','i35','i36','i37',
'i38','i39','i40','i41','i42','i43','i44','i45','i46','i47','i48','i49','i50','i51','i52','i53','i54']

mhead = ['m1','m2','m3','m4','m5','m6','m7','m8','m9','m10','m11','m12','m13','m14','m15','m16','m17','m18','m19','m20','m21','m22','m23',
'm24','m25','m26','m27','m28','m29','m30','m31','m32','m33','m34','m35','m36','m37','m38','m39','m40','m41','m42','m43','m44','m45',
'm46','m47','m48','m49','m50','m51','m52','m53','m54','m55','m56','m57','m58','m59','m60','m61','m62','m63','m64','m65','m66','m67']

whead = ['w1','w2','w3','w4','w5','w6','w7','w8','w9','w10','w11','w12','w13','w14','w15','w16','w17','w18','w19','w20','w21','w22','w23',
'w24','w25','w26','w27','w28','w29','w30','w31','w32','w33','w34','w35']


#Function to read start date
def read_date():
##    file = open('F:/Flexi_final/[IN-050C]/Raw/IN050C2.txt','r') ###local
    file = open('/home/admin/Start/IN050C2.txt','r') ###server
    date_read = file.read(10)
    return date_read

#Function to set start date
def set_date():
    date_read = read_date()
    global date
    date = datetime.datetime.strptime(date_read, '%Y-%m-%d').date()
    date += datetime.timedelta(days=-1) #To reduce date by a day as it gets added in current_date()

#Function to determine the next day
def determine_date():
    global date
    date += datetime.timedelta(days=1)
    return date


#Function to determine URL for the next day
def determine_url(date,url_head):
    replace_date = date.strftime("%Y%m%d")
    url = 'https://fbapi.fleximc.com/API/JSON/CleanTech/Arvind_Mills/Data/'+replace_date+'/'+url_head+replace_date+'.json'
    return url

#Function to send mail
def sendmail():
    fromaddr = 'operations@cleantechsolar.com'
    toaddr = 'kn.rohan@gmail.com','andre.nobre@cleantechsolar.com','shravan1994@gmail.com','rupesh.baker@cleantechsolar.com'
    uname = 'shravan.karthik@cleantechsolar.com'

    subject = 'Bot IN-050C2 Raw Down - Portal Error'
    text = ' '
    message = 'Subject: {}\n\n{}'.format(subject,text)
    password = 'CTS&*(789'
    try:
        print('send try')
        server = smtplib.SMTP('smtp-mail.outlook.com',587)
        server.ehlo()
        server.starttls()
        server.login(uname,password)
        server.sendmail(fromaddr,toaddr,message)
        print('sending')
        server.quit()
    except Exception as e:
        print('mail send exception - ',str(e))


#Function to get header names
def get_header(json_head):
    if json_head == 'inverter':
        return ihead
    elif json_head == 'MFM':
        return mhead
    else:
        return whead
    
    


#Function to determine path and create directory 
def determine_path(curr_date,device,fd):
    year = curr_date.strftime("%Y")
    month = curr_date.strftime("%Y-%m")
    day = curr_date.strftime("%Y-%m-%d")
##    master_path = 'F:/del/FlexiMC_Data/Raw_Data/[IN-050C]/' ###local
    master_path = '/home/admin/Data/Flexi_Raw_Data/[IN-050C]/' ###server
    year_path = master_path+year+'/'
    month_path = year_path+month+'/'
    device_path = month_path+device+'/'
    final_path = device_path+'[IN-050C]-'+fd+'-'+day+'.txt'
    
    if not os.path.exists(year_path):
        os.makedirs(year_path)
    if not os.path.exists(month_path):
        os.makedirs(month_path)
    if not os.path.exists(device_path):
        os.makedirs(device_path)
    if os.path.exists(final_path):
        os.remove(final_path)
        print('[IN-050C]-'+fd+'-'+day+'.txt'+' removed')
    
    return final_path


#Function to re-try connection
def connection_try(url):
    counter = 0
    while True:
        counter += 1
        try:
            print('second attempt try - ',counter)
            with urllib.request.urlopen(url) as url:
                try_data = json.loads(url.read().decode(), object_pairs_hook=collections.OrderedDict)
                return
        except Exception as e:
            if 'IncompleteRead' in str(e) or 'Errno 111' in str(e) or 'Remote end closed' in str(e) or 'urlopen error' in str(e):
                if counter == 4:
                    sendmail()
                    exit('Exiting due to persistent connection error')
                    
                print('sleeping for 15')
                time.sleep(900)
            else:
                return

#Function to determine path and create directory 
def meter(url_head,json_head,folder_name,file_field):
    tz = pytz.timezone('Asia/Kolkata')
    while True:
        today_date = datetime.datetime.now(tz).date()
        curr_date = determine_date()
        url = determine_url(curr_date,url_head)
        path = determine_path(curr_date,folder_name,file_field)
        try:
            with urllib.request.urlopen(url) as url:
                load_data = json.loads(url.read().decode(), object_pairs_hook=collections.OrderedDict)
                
        except Exception as e:
            if 'IncompleteRead' in str(e) or 'Errno 111' in str(e) or 'Remote end closed' in str(e) or 'urlopen error' in str(e):
                print('Incomplete read/connection refuse error - 1  Slepping for 60 min - ',str(e))
                time.sleep(3600)
                connection_try(url)
                try:
                    print('Last attempt try')
                    with urllib.request.urlopen(url) as url:
                        load_data = json.loads(url.read().decode(), object_pairs_hook=collections.OrderedDict)
                except Exception as e:
                    if 'IncompleteRead' in str(e) or 'Errno 111' in str(e) or 'Remote end closed' in str(e) or 'urlopen error' in str(e):
                        print('Incomplete read/connection refuse error - 2  Killing bot and sending mail - ',str(e))
                        sendmail()
                        exit('Exiting due to persistent connection error')
                        
                    else:
                        if curr_date > today_date:
                            print('Last file -2 - Exiting while loop',str(curr_date), str(e))
                            break
                        else:
                            print('No data found -2 - ',str(curr_date), str(e))
                            continue                 
                    


            else:
                if curr_date > today_date:
                    print('Last file - Exiting while loop',str(curr_date), str(e))
                    break
                else:
                    print('No data found - ',str(curr_date), str(e))
                    continue
            
        new_file = 1
        i=0
        if load_data[json_head] == None:
            print('NULL Returned by Flexi')
            continue
        length = len(load_data[json_head])
        while i < length:
            data = load_data[json_head][i]
            with open(path, 'a',newline='') as csv_file:
                csvwriter = csv.writer(csv_file, delimiter='\t')
                line = []
                header = []

                header = get_header(json_head)
                line = [data.get(k,'NA') for k in header]
                line = ['NULL' if w is None else w for w in line]

                if new_file == 1:
                    csvwriter.writerow(header)
                    new_file = 0
                csvwriter.writerow(line)
                csv_file.close()
            i = i+1
        print(str(length)+' rows have been written to - '+path)



set_date()
meter('32_Inverter_34_','inverter','ACH_Inverter_1','ACH_I1')
set_date()
meter('32_Inverter_35_','inverter','ACH_Inverter_2','ACH_I2')
set_date()
meter('32_Inverter_36_','inverter','ACH_Inverter_3','ACH_I3')
set_date()
meter('32_Inverter_37_','inverter','ACH_Inverter_4','ACH_I4')
set_date()
meter('32_Inverter_38_','inverter','ACH_Inverter_5','ACH_I5')
set_date() 
meter('32_Inverter_39_','inverter','ACH_Inverter_6','ACH_I6')
set_date()
meter('32_Inverter_40_','inverter','ACH_Inverter_7','ACH_I7')
set_date()
meter('32_Inverter_41_','inverter','ACH_Inverter_8','ACH_I8')
set_date()
meter('32_Inverter_42_','inverter','ACH_Inverter_9','ACH_I9')

set_date()
meter('32_Inverter_13_','inverter','ANM_Inverter_1','ANM_I1')
set_date()
meter('32_Inverter_14_','inverter','ANM_Inverter_2','ANM_I2')
set_date()
meter('32_Inverter_15_','inverter','ANM_Inverter_3','ANM_I3')
set_date()
meter('32_Inverter_16_','inverter','ANM_Inverter_4','ANM_I4')
set_date()
meter('32_Inverter_17_','inverter','ANM_Inverter_5','ANM_I5')
set_date()
meter('32_Inverter_18_','inverter','ANM_Inverter_6','ANM_I6')
set_date()
meter('32_Inverter_19_','inverter','ANM_Inverter_7','ANM_I7')
set_date()
meter('32_Inverter_20_','inverter','ANM_Inverter_8','ANM_I8')
set_date()
meter('32_Inverter_21_','inverter','ANM_Inverter_9','ANM_I9')

set_date()
meter('32_Inverter_43_','inverter','ACH_Inverter_10','ACH_I10')
set_date()
meter('32_Inverter_44_','inverter','ACH_Inverter_11','ACH_I11')
set_date()
meter('32_Inverter_45_','inverter','ACH_Inverter_12','ACH_I12')
set_date()
meter('32_Inverter_46_','inverter','ACH_Inverter_13','ACH_I13')
set_date()
meter('32_Inverter_47_','inverter','ACH_Inverter_14','ACH_I14')
set_date() 
meter('32_Inverter_48_','inverter','ACH_Inverter_15','ACH_I15')
set_date()
meter('32_Inverter_49_','inverter','ACH_Inverter_16','ACH_I16')
set_date()
meter('32_Inverter_50_','inverter','ACH_Inverter_17','ACH_I17')
set_date()
meter('32_Inverter_51_','inverter','ACH_Inverter_18','ACH_I18')
set_date()
meter('32_Inverter_52_','inverter','ACH_Inverter_19','ACH_I19')
set_date()
meter('32_Inverter_53_','inverter','ACH_Inverter_20','ACH_I20')
set_date()
meter('32_Inverter_54_','inverter','ACH_Inverter_21','ACH_I21')

set_date()
meter('32_Inverter_22_','inverter','ANM_Inverter_10','ANM_I10')
set_date()
meter('32_Inverter_23_','inverter','ANM_Inverter_11','ANM_I11')
set_date()
meter('32_Inverter_24_','inverter','ANM_Inverter_12','ANM_I12')
set_date()
meter('32_Inverter_25_','inverter','ANM_Inverter_13','ANM_I13')
set_date()
meter('32_Inverter_26_','inverter','ANM_Inverter_14','ANM_I14')
set_date()
meter('32_Inverter_27_','inverter','ANM_Inverter_15','ANM_I15')
set_date()
meter('32_Inverter_28_','inverter','ANM_Inverter_16','ANM_I16')
set_date()
meter('32_Inverter_29_','inverter','ANM_Inverter_17','ANM_I17')
set_date()
meter('32_Inverter_30_','inverter','ANM_Inverter_18','ANM_I18')
set_date()
meter('32_Inverter_31_','inverter','ANM_Inverter_19','ANM_I19')
set_date()
meter('32_Inverter_32_','inverter','ANM_Inverter_20','ANM_I20')
set_date()
meter('32_Inverter_33_','inverter','ANM_Inverter_21','ANM_21')

set_date()
meter('32_Inverter_1_','inverter','ANK_Inverter_1','ANK_I1')
set_date()
meter('32_Inverter_2_','inverter','ANK_Inverter_2','ANK_I2')
set_date()
meter('32_Inverter_3_','inverter','ANK_Inverter_3','ANK_I3')
set_date()
meter('32_Inverter_4_','inverter','ANK_Inverter_4','ANK_I4')
set_date()
meter('32_Inverter_5_','inverter','ANK_Inverter_5','ANK_I5')
set_date()
meter('32_Inverter_6_','inverter','ANK_Inverter_6','ANK_I6')
set_date()
meter('32_Inverter_7_','inverter','ANK_Inverter_7','ANK_I7')
set_date()
meter('32_Inverter_8_','inverter','ANK_Inverter_8','ANK_I8')
set_date()
meter('32_Inverter_9_','inverter','ANK_Inverter_9','ANK_I9')
set_date()
meter('32_Inverter_10_','inverter','ANK_Inverter_10','ANK_I10')
set_date()
meter('32_Inverter_11_','inverter','ANK_Inverter_11','ANK_I11')
set_date()
meter('32_Inverter_12_','inverter','ANK_Inverter_12','ANK_I12')

set_date()
meter('32_Inverter_55_','inverter','TP1_Inverter_11','TP1_I1')
set_date()
meter('32_Inverter_56_','inverter','TP1_Inverter_12','TP1_I2')
set_date()
meter('32_Inverter_57_','inverter','TP1_Inverter_13','TP1_I3')
set_date() 
meter('32_Inverter_58_','inverter','TP1_Inverter_14','TP1_I4')
set_date()
meter('32_Inverter_59_','inverter','TP2_Inverter_11','TP2_I1')
set_date()
meter('32_Inverter_60_','inverter','TP2_Inverter_12','TP2_I12')
set_date()
meter('32_Inverter_61_','inverter','TP2_Inverter_13','TP2_I13')
set_date()
meter('32_Inverter_62_','inverter','TP2_Inverter_14','TP2_I14')


set_date()
meter('32_MFM_6_','MFM','TP1_MFM1','TP1_MFM1')
set_date()
meter('32_MFM_7_','MFM','TP2_MFM1','TP2_MFM1')

set_date()
meter('32_MFM_4_','MFM','ACH_MFM1','ACH_MFM1')
set_date()
meter('32_MFM_5_','MFM','ACH_MFM2','ACH_MFM2')

set_date()
meter('32_MFM_1_','MFM','ANK_MFM1','ANK_MFM1')

set_date()
meter('32_MFM_2_','MFM','ANM_MFM1','ANM_MFM1')
set_date()
meter('32_MFM_3_','MFM','ANM_MFM2','ANM_MFM2')

set_date()
meter('32_WMS_1_','WMS','ARV_WMS','ARV_WMS')

print('DONE!')





        

    
