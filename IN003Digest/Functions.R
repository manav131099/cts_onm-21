# Code to define all functions used in Live.R

require('httr')

# No use of this variable for now will be ammended for FTP warning though
FIREMAILBACKOFF = 1 
source('/home/admin/CODE/MasterMail/timestamp.R')

###############################################################################
#Function to cleanse raw data and output a table which is eventually stored as
#Gen-1 data format. 
#The input for the function(pth) is the path of the raw-file, which is generated
#using the fetchrawdata function defined later.
###############################################################################
cleansedata = function(pth) 
{
	df = try(read.table(pth,header = T,sep="\t"),silent=T)
	if(class(df)=='try-error')
	{
		c1=c2=c3=c4=c5=NA
		df2 = data.frame(Tm=c1,Pac=c2,Eac=c3,GHI=c4,Tmod=c5)
  	return(df2)
	}

################################################################################
#The gen-1 data only needs the value Time, Pac, Eac, GHI and Tmod which are rows
#19,18,17,16 and 15 respectively of the raw-data. 
#The raw data has a bug such that the alternate rows record the correct values
#for certain parameters, i.e alternate rows have correct values of Pac,Eac
#while the other rows have correct values for GHI and Tmod. Incorrect values are
#identified by noting the value -1000 for Tmod, i.e if Tmod is -1000 for a 
#particular row, then that row has the correct values for Eac and Pac. However
#if the value for Tmod is not -1000 then that row has the correct values of Tmod
#and GHI. Notice for both these rows the time-stamp is the same.
#For example the 15-19th columns of the raw data-frame look something like this:
#
#Tmod		GHI		Eac		Pac		Time
#-1000	-1000	44.6	102.1	11:39
#38.5		1002	-1000	-1000	11:39
#-1000	-1000	50.4	103.2	11:40
#38.2		1001	-1000	-1000	11:40
#
#This function will cleanse the above data to give the below output
#
#Tm			Pac		Eac		GHI		Tmod
#11:39	102.1	44.6	1002	38.5
#11:40	103.2	50.4	1001	38.2
#
# Now this data-frame which is a cleansed version of the raw data is returned
###############################################################################

  c2 = as.numeric(df[,15])
  c2idx = which(c2 %in% -1000)
  c1 = as.character(df[c2idx,19]) # Time is same for both so can be either index
 # Rows which match -1000 for Tmod represent true value of Eac and Pac
  c2 = as.numeric(df[c2idx,18])
  c3 = as.numeric(df[c2idx,17]) 
 # Rows which dont match -1000 for Tmod represent true value of GHI and Tmod
	c4 = as.numeric(df[-c2idx,16])  
  c5 = as.numeric(df[-c2idx,15])
	len1 = length(c1)
	len2 = length(c4)
	len1 = len2 = min(len1,len2) #minimum length of two sets of metrics
	{
	if(len1 < 1)
	{
	#handle cases where no data is present, set all to NA
	c1 = c2 = c3 = c4 = c5 = NA
	}
	else
	{
  #trims columns to same length to accommodate them within a data-frame
	c1 = c1[1:len1]
	c2 = c2[1:len1]
	c3 = c3[1:len1]
	c4 = c4[1:len1]
	c5 = c5[1:len1]
	c1idx = unique(c1)
	c1idx = match(c1idx,c1)
	c1 = c1[c1idx]
	c2 = c2[c1idx]
	c3 = c3[c1idx]
	c4 = c4[c1idx]
	c5 = c5[c1idx]
  }
	}
	df2 = data.frame(Tm=c1,Pac=c2,Eac=c3,GHI=c4,Tmod=c5)
		recordTimeMaster("IN-003T","Gen1",as.character(df2[nrow(df2),1]))
  return(df2)
}


###############################################################################
#Function to ping the TORP server for new-data and return the raw-data frame.
#The function takes as inputs day1 and day2 which are the start and end-date.
#Usually both these are the same, i.e day1 = day2.
#The format for day1, day2 is DD/MM/YYYY
###############################################################################
pathCentral = '/home/admin/Data/TORP Data/AllStations'

probePath = function(day)
{
	path = pathCentral
	DDMMYYYY = unlist(strsplit(as.character(day),"/"))
	pathYr = paste(path,DDMMYYYY[3],sep="/")
	#checkDir(pathYr)
	pathMon = paste(pathYr,paste(DDMMYYYY[3],"-",DDMMYYYY[2],sep=""),sep="/")
	#checkDir(pathYr)
	pathFinal = paste(pathMon,paste(paste(DDMMYYYY[3],DDMMYYYY[2],DDMMYYYY[1],sep="-"),".txt",sep=""),sep="/")
	{
		if(file.exists(pathFinal))
		{
			df = read.table(pathFinal,header = T,sep = "\t")
			return(df)
		}
		else
			return(NULL)
	}
}

fetchrawdata = function(day1,day2)
{
  	#fetch data from SERVER USING A POST-request
		masterData = probePath(day1)
		
		if(is.null(masterData))
		{
			return(masterData)
		}
		stnnames = as.character(masterData[,13])
		subst = which(stnnames %in% "Amtek")
		if(length(subst))
		{
			vals2 = masterData[subst,] # extract only rows with MJ Logistics tag in column 13
			recordTimeMaster("IN-003T","FTPNewFiles",as.character(vals2[nrow(vals2),19]))
			return(vals2)
		}
		return(NULL)
}

