args<-commandArgs(TRUE)
code = paste("[", args[1], "]", sep="")
prbudget = as.numeric(args[2])
rate = as.numeric(args[3])
coddate =as.Date(args[4])
date = args[5]
checkdir = function(x)
{
  if(!file.exists(x))
  {
    dir.create(x)
  }
}
library(ggplot2)
library(zoo)
library(grid)
library(lubridate)
library(RODBC)
checkdir(paste("/home/admin/Graphs/Graph_Extract", substr(code, 2, 7), sep="/"))
checkdir(paste("/home/admin/Graphs/Graph_Output", substr(code, 2, 7), sep="/"))
pathwritetxt <- paste("/home/admin/Graphs/Graph_Extract", substr(code, 2, 7), paste("[", substr(code, 2, 7), "] Graph ", date, " - PR Evolution.txt", sep=""), sep="/")
graph <- paste("/home/admin/Graphs/Graph_Output", substr(code, 2, 7), paste("[", substr(code, 2, 7), "] Graph ", date, " - PR Evolution.pdf", sep=""), sep="/")
station_name = substr(code, 2, 7)
RODBC_connection <- odbcDriverConnect('driver={ODBC Driver 13 for SQL Server};server=cleantechsolar.database.windows.net;database=Cleantech Meter Readings;uid=RohanKN;pwd=R@h@nKN1')
query = sprintf("SELECT Station_Id FROM Stations WHERE Station_Name ='%s'",station_name)
dt <- sqlQuery(channel=RODBC_connection, query = query)
sid = dt[,1]
query = paste("SELECT Meter_id,Capacity FROM Meters WHERE Station_Id = ",sid,sep="")
dm<- sqlQuery(channel=RODBC_connection, query = query)
query = paste("SELECT Date,GHI,PR,Meter_Id FROM Stations_Data WHERE Station_Id = ",sid,sep="")
dff <- sqlQuery(channel=RODBC_connection, query = query)
odbcClose(RODBC_connection)
if(length(unique(dff[["Meter_Id"]]))>1){
for(x in 1 :nrow(dm))
{
  dff[dff$Meter_Id == dm[x,1],3]=(dff[dff$Meter_Id == dm[x,1],3]*dm[x,2])
}
prs=setNames(aggregate(dff$PR, by=list(Date=dff$Date), FUN=sum),c("Date", "PR"))
prs[,2]=prs[,2]/sum(dm[,2])
ghi=setNames(aggregate(dff$GHI, by=list(Date=dff$Date), FUN=mean),c("Date", "GHI"))
dff<- merge(ghi,prs,by="Date")
}

dff[dff$Date<'2019-10-18','PR']=NA
dff = dff[(dff[,3] <= 95),]
dff = dff[(dff[,3] >= 0),]

dff = dff[(as.Date(dff$Date)<= date),]

dff = unique(dff)
print(dff)
#moving average 7d/30d
zoo.PR <- zoo(dff[,3], as.Date(dff$Date))
if(nrow(dff)<120){
ma1 <- rollapplyr(zoo.PR, list(-(6:1)), mean, fill = NA, na.rm = T)
}else{
ma1 <- rollapplyr(zoo.PR, list(-(29:1)), mean, fill = NA, na.rm = T)
}
dff$ambPR.av = coredata(ma1)
row.names(dff) <- NULL

firstdate <- as.Date(dff[1,1])
lastdate <- as.Date(dff[length(dff[,1]),1])
PRUse = as.numeric(dff$PR)
PRUse = PRUse[complete.cases(PRUse)]
last7 = round(mean(tail(PRUse,7)),1)
last30 = round(mean(tail(PRUse,30)),1)
last60 = round(mean(tail(PRUse,60)),1)
last90 = round(mean(tail(PRUse,90)),1)
last365 = round(mean(tail(PRUse,365)),1)
lastlt = round(mean(PRUse),1)
rightIdx = nrow(dff) * 0.7
x2idx = round(nrow(dff) * 0.30, 0)

#data sorting for visualisation
dff$colour[dff[,2] < 2] <- '< 2'
dff$colour[dff[,2] >= 2 & dff[,2] <= 4] <- '2 ~ 4'
dff$colour[dff[,2] >= 4 & dff[,2] <= 6] <- '4 ~ 6'
dff$colour[dff[,2] > 6] <- '> 6'
dff$colour = factor(dff$colour, levels = c("< 2", "2 ~ 4", "4 ~ 6", "> 6"), labels =c('< 2', '2 ~ 4', '4 ~ 6', '> 6')) #to fix legend arrangement
titlesettings <- theme(plot.title = element_text(face = "bold", size = 12, lineheight = 0.7, hjust = 0.5, margin = margin(0,0,7,0)), plot.subtitle = element_text(face = "bold", size = 12, lineheight = 0.9, hjust = 0.5))

p <- ggplot() + theme_bw()

#30-d / 7-d average line

p <- p + geom_point(data=dff, aes(x=as.Date(Date), y = PR, shape = as.factor(18), colour = colour), size = 2) + guides(shape = FALSE)
p <- p + ylab("Performance Ratio [%]") + xlab("") + coord_cartesian(ylim = c(0,100))
if(nrow(dff)<120){
    p <- p + scale_x_date(expand=c(0,0),date_breaks = "7 days", date_labels = "%d-%b", limits = c(firstdate, NA)) + scale_y_continuous(breaks=seq(0, 100, 10))
    p <- p + annotate("segment", x = as.Date(dff[nrow(dff)* 0.25,1]), xend = as.Date(dff[nrow(dff)* 0.28,1]), y=45, yend=45, colour="red", size=1.5)
    p <- p + annotate('text', label = "7-d moving average of PR", y = 45, x = as.Date(dff[x2idx,1]), colour = "red",fontface =2,hjust = 0)
}else{
    p <- p + scale_x_date(expand=c(0,0),date_breaks = "3 months", date_labels = "%b/%y", limits = c(firstdate, NA)) + scale_y_continuous(breaks=seq(0, 100, 10))
    p <- p + annotate("segment", x = as.Date(dff[nrow(dff)* 0.25,1]), xend = as.Date(dff[nrow(dff)* 0.28,1]), y=45, yend=45, colour="red", size=1.5)
    p <- p + annotate('text', label = "30-d moving average of PR", y = 45, x = as.Date(dff[x2idx,1]), colour = "red",fontface =2,hjust = 0)
}
p <- p + theme(plot.margin = margin(5, 15, 0, 5),axis.title.x = element_text(size=13), axis.title.y = element_text(size=13), axis.text = element_text(size=13), panel.grid.minor = element_blank(),                 panel.border = element_blank(), axis.line = element_line(colour = "black"), legend.justification = c(0, 0.97), legend.position = c(0.1, 0.97))                     #LEGNED AT RHS
p <- p + titlesettings + ggtitle(paste0('Performance Ratio Evolution - ', substr(code, 2, 7), sep=''), subtitle = paste0('From ', '2019-07-19', ' to ', lastdate))
p <- p + theme(legend.text=element_text(size=9), legend.title=element_text(size=11))
p <- p + scale_colour_manual('Daily Irradiation [kWh/m2]', values = c("blue", "deepskyblue1", "orange","darkorange3"), labels = c('< 2','2 ~ 4','4 ~ 6','> 6',''),                                 guide = guide_legend(title.position = "left", ncol = 4, nrow=1)) 
p <- p + scale_shape_manual(values = 18)
p <- p + theme(legend.box = "horizontal", legend.direction="horizontal") #legend.position = "bottom"


#Creating steps using budget PR
coddate_og=coddate
prthresh = c(prbudget,prbudget)
dates = c(as.Date('2019-07-19'),coddate)
temp=prbudget
while(coddate<as.Date(date) && (as.Date(date)-coddate)>365){#
     coddate= coddate %m+% months(12)
     temp=temp-(temp*rate)
     prthresh=c(prthresh,temp)
     dates=c(dates,coddate)
}

prthresh=c(prthresh,temp)
dates=c(dates,lastdate)
print(dates)
p <- p +geom_step(mapping=aes(x=dates,y=prthresh),size=1,color='darkgreen')
dff$Date=as.Date(dff$Date, format = "%Y-%m-%d")
total=0
text="Target Budget Yield Performance Ratio ["
for(i in 2:(length(dates)-1)){#n-1 because we are taking number of years between. 
    if(i==(length(prthresh)-1)){
    text=paste(text,i-1,'Y-',format(round(prthresh[i],1),nsmall=1), "%]", sep="")
    }else{
    text=paste(text,i-1,'Y-',format(round(prthresh[i],1),nsmall=1), "%,", sep="")
    }
}

for(i in 1:(length(dates)-1)){
    if(i==(length(dates)-1)){
      d=dff[dff$Date >= dates[i] & dff$Date < dates[i+1],'PR']>prthresh[i]
      d =sum(d[!is.na(d)])
      total=total+d
    }else{
      d=dff[dff$Date >= dates[i] & dff$Date < dates[i+1],'PR']>prthresh[i]
      d =sum(d[!is.na(d)])
      total=total+d
    }
}


#Adding Legend + Stats
p <- p + guides(colour = guide_legend(override.aes = list(shape = 18)))
p <- p + geom_line(data=dff, aes(x=as.Date(Date), y=ambPR.av), color="red", size=1.5)
p <- p + annotate("segment", x = as.Date(dff[nrow(dff)* 0.25,1]), xend = as.Date(dff[nrow(dff)* 0.28,1]), y=50, yend=50, colour="darkgreen", size=1.5)
p <- p + annotate('text', label = text, y = 50, x = as.Date(dff[x2idx,1]), colour = "darkgreen",fontface =2,hjust = 0)
p <- p + annotate('text', label = paste("Points above Target Budget PR = ",total,'/',nrow(dff),' = ',round((total/nrow(dff))*100,1),'%',sep=''), y = 40, x = as.Date(dff[x2idx,1]), colour = "black",fontface =2,hjust = 0)
p <- p + annotate('text', label = paste('Commissioning Date: ',strftime(coddate_og,format="%Y-%m-%d"),sep=" "), y = 35, x = as.Date(dff[x2idx,1]), colour = "black",fontface =2,hjust = 0)
p <- p + annotate('text', label = paste("Average PR last 7-d:", last7, "%"), y=30, x=as.Date(dff[rightIdx,1]), colour="black", hjust = 0)
p <- p + annotate('text', label = paste("Average PR last 30-d:", last30, "%"), y=25, x=as.Date(dff[rightIdx,1]), colour="black", hjust = 0)
p <- p + annotate('text', label = paste("Average PR last 60-d:", last60, "%"), y=20, x=as.Date(dff[rightIdx,1]), colour="black", hjust = 0)
p <- p + annotate('text', label = paste("Average PR last 90-d:", last90, "%"), y=15, x=as.Date(dff[rightIdx,1]), colour="black", hjust = 0)
p <- p + annotate('text', label = paste("Average PR last 365-d:", last365,"%"), y=10, x=as.Date(dff[rightIdx,1]), colour="black", hjust = 0)
p <- p + annotate('text', label = paste("Average PR Lifetime:", lastlt,"%"), y=5, x=as.Date(dff[rightIdx,1]), colour="black", hjust = 0,fontface=2)
p <- p + annotate("segment", x = as.Date(dff[nrow(dff),1]), xend = as.Date(dff[nrow(dff),1]), y=0, yend=100, colour="deeppink4", size=1.5, alpha=0.5)

ggsave(paste0(graph), p, width = 11, height=8)
write.table(dff, na = "", pathwritetxt, row.names = FALSE, sep ="\t")
